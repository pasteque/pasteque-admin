# Pastèque admin
> https://www.pasteque.org

Presentation
============
Pasteque Admin is a web management client for Pastèque. It provides all the tools to manage the cash registers. As it does not create tickets and never will, it is not subject to the LF2016 and can be install and used without legal issues.

Pasteque Admin can manage only one Pasteque acount at a time.

It is made in [PHP] with [Sqlite].

Installation
============

The installation procedure is not complete yet and requires a lot of manual tweaks.

Pasteque Admin requires php7 (it is not working with php5.6) with sqlite3 driver, xml and curl.

First install dependencies with composer (it may take a while).

Then copy `/config/prod_example.php` to `config/prod.php` and edit it to your liking. Set `api_*`, these are relative to the Pasteque general acount.

To the same with `/config/dev_example.php` and set `debug` to false for a production environment.

Create `/tmp` and initialize the database with Doctrine by running `php vendor/bin/doctrine orm:schema-tool:create`.

Install with Apache
-------------------

Point Apache to `web/` and make sure `AllowOverride All` is set for this directory.

Create `web/.htaccess` and put the following content

```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^ index.php [QSA,L]
```

Make sure `tmp` is writeable by the apache user as well as `tmp/sqlite.db`.

User acounts
============

The login page refers to the end-users that are defined in the database of Pasteque. For a user to be able to connect, it must have a password set. This password is stored without encryption for now.
