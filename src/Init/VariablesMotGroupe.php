<?php

namespace App\Init;

use Declic3000\Pelican\Init\VariableInterface;

class VariablesMotGroupe extends VariableInterface
{
    function getVariables()
    {
        return [
            'systeme' => [
                'nom' => 'Système',
                'objetEnLien' => 'individu;membre',
                'importance' => 1,
            ],
            'npai' => [
                'nom' => 'Changement coordonnées',
                'objetEnLien' => 'individu',
                'importance' => 1,
                'parent' => 'systeme',
            ]

        ];
    }
}


