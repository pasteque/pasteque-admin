<?php


namespace App\Init;

use Declic3000\Pelican\Init\ChargeurSacInterface;

class ChargeurSac extends ChargeurSacInterface
{


    // /////////////////////////////////////////////////////////////////////////////////
    // // Fichier qui regroupe les fonctions pour travailler sur la super variable "table"
    function chargement_table(): array
    {
        $tab_a_charger = [
            'cashregister',
            'category',
            'paymentmode',
            'role',
            'tax',
            'place',
            'floor',
            'currency',
            'supplier'
        ];

        foreach ($tab_a_charger as $table) {
            $tab[$table] = $this->charger_table($table);
        }
        $tab_cat = $this->charger_table('category');
        $tab_cat = table_trier_par($tab_cat, 'disp_order');
        $tab['category_tree'] = table_hierarchise($tab_cat);

        $tab['pays'] = (include(__DIR__ . '/../..' . '/vendor/symfony/intl/Resources/data/regions/fr.php'))['Names'];
        uasort($tab['pays'], 'natksort');

        $tab['modele'] = $this->charger_modele();


        $tab_mot = $this->charger_table('mot');
        $tab_motgroupe = $this->charger_table('motgroupe');
        $tab_mot_arbre = [];
        $tab_mot_arbre_tmp = [];
        $tab_mot_filtre = [];

        foreach ($tab_mot as $k => &$mot) {
            if (isset($tab_motgroupe[$mot['id_motgroupe']])) {
                $mot['objet_en_lien'] = explode(';', (string) $tab_motgroupe[$mot['id_motgroupe']]['objets_en_lien']);
                $mot['systeme'] = $tab_motgroupe[$mot['id_motgroupe']]['systeme'];
            } else {
                $mot['id_motgroupe'] = 0;
            }
            $tab_mot_arbre_tmp[$mot['id_motgroupe']][] = $mot['id_mot'];
        }

        foreach ($tab_motgroupe as $id_motgroupe => &$motgroupe) {
            $tab_objet = explode(';', (string) $motgroupe['objets_en_lien']);
            $tab_tmp = $tab_mot_arbre_tmp[$id_motgroupe] ?? [];
            $motgroupe['mots'] = $tab_tmp;
            $motgroupe['enfant'] = table_simplifier(table_filtrer_valeur($tab_motgroupe, 'parent_id', $motgroupe['id_motgroupe']));
            $motgroupe['options'] = json_decode((string) $motgroupe['options'], true);
            $motgroupe['objets_en_lien'] = explode(';', (string) $motgroupe['objets_en_lien']);
            if (isset($motgroupe['options']) && is_array($motgroupe['options'])) {
                foreach ($motgroupe['options'] as $ol => $tab_filtre) {
                    $tab_mot_filtre[$ol][$tab_filtre['nom']] = $tab_filtre;
                }
            }
            foreach ($tab_objet as $objet) {
                $tab_mot_arbre[$objet][$motgroupe['nom']] = array_flip(table_simplifier(table_filtrer_valeur($tab_mot, 'id_motgroupe', $id_motgroupe)));
            }
        }
        $tab['mot'] = $tab_mot;
        $tab['mot_arbre'] = $tab_mot_arbre;
        $tab['mot_filtre'] = $tab_mot_filtre;
        $tab['motgroupe'] = $tab_motgroupe;
        return $tab;
    }


    function charger_modele()
    {
        $tab = [];
        foreach (glob(__DIR__ . '/../..' . '/src/importation/modele/*.php') as $modele) {
            $tab[basename($modele, ".php")] = [
                'nom' => basename($modele, ".php"),
                'chemin' => $modele
            ];
        }

        return $tab;
    }


}