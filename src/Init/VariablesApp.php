<?php

namespace App\Init;

use Declic3000\Pelican\Init\VariableInterface;

class VariablesApp extends VariableInterface
{
    function getVariables()
    {
        return [
            'vars_env' => [
                'api_url' => 'API_URL',
                'api_user' => 'API_USER',
                'api_password' => 'API_PASSWORD'
            ],
            'options' => [
                'prefixe_table' => '',
                'suffixe_table_avec_s' => true,
                'cle_id' => true,
                'template_sous_dossier' => false,
                'systeme_pref' => 'table',
                'utilise_entite' => false,
                'utilise_log' => false,
                'col_affichage_defaut'=>'name',
                'ged' => [
                    'fichier_ou_base' => true
                ],
                'table_sys' => [
                    "config" => [
                        "table_sql" => 'configs',
                        "cle" => 'id',
                        "col_name" => 'name',
                        "col_id_user" => 'id_individu'
                    ],
                    "preference" => [
                        "table_sql" => 'preferences',
                        "cle" => 'id',
                        "col_name" => 'name',
                        "col_id_user" => 'user_id'
                    ],
                    "utilisateur" => [
                        'objet' => 'user',
                        "table_sql" => 'users',
                        "cle_sql" => 'id'
                    ]
                ]
            ],
            'objets' => [
                'product' => [],
                'productextra' => ['table_sql' => 'products_extra', 'cle_sql' => 'product_id'],
                'productsupplier' => ['alias' => 'productSupplier', 'phpname' => 'productSupplier', 'table_sql' => 'products_suppliers'],
                'tax' => ['table_sql' => 'taxes'],
                'category' => ['table_sql' => 'categories'],
                'currency' => ['table_sql' => 'currencies'],
                'role' => [],
                'user' => [],
                'place' => [],
                'floor' => [],
                'cashregister' => ['phpname' => 'CashRegister'],
                'paymentmode' => ['phpname' => 'PaymentMode'],
                'tariffarea' => [],
                'discount' => [],
                'discountprofile' => ['phpname' => 'DiscountProfile'],
                'customer' => [],
                'resource' => ['cle' => 'label','cle_sql' => 'label'],
                'session' => ['phpname' => 'CashSession', 'table_sql' => 'sessions'],
                'sessionpayment' => [],
                'sessiontaxe' => [],
                'sessioncat' => [],
                'sessioncattaxe' => [],
                'sessioncustbalance' => [],
                'preference' => [],
                'ticket' => [],
                'tickettax' => ['table_sql' => 'tickettaxes'],
                'ticketpayment' => [],
                'ticketline' => [],
                'etape' => ['source' => 'etic/', 'phpname' => 'Etape'],
                'mot' => ['cle' => 'idMot', 'cle_sql' => 'id_mot'],
                'motgroupe' => ['cle' => 'idMotgroupe', 'cle_sql' => 'id_motgroupe'],
                'supplier' => []


            ],
            'liens_role_objet' => [
                'Administrator' => ['admin' => 'all', 'generique' => 'all', 'com' => 'all', 'geo' => 'all', 'doc' => 'all'],
                'USER' => ['generique' => 'all']
            ]

        ];
    }

}

