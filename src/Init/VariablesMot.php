<?php

namespace App\Init;

use Declic3000\Pelican\Init\VariableInterface;

class VariablesMot extends VariableInterface
{
    function getVariables()
    {
        return [
            'npai' => [
                'npai_adres' => 'Adresse non valide',
                'npai_email' => 'Email non valide',
                'npai_telep' => 'Téléphone non valide',
                'npai_tele2' => 'Téléphone pro non valide',
                'npai_mobil' => 'Mobile non valide'
            ]
        ];
    }

}