<?php

namespace App\Init;

use Declic3000\Pelican\Init\VariablePreferenceInterface;

class VariablesPreference extends VariablePreferenceInterface
{
    function getVariables()
    {




        $modele_objet_category = $this->prepare_modele_objet('category');
        $modele_objet_category['variables']['category']['datatable_product'] = $this->prepare_modele_datatable('show','category');

        $modele_objet_product = $this->prepare_modele_objet('product');
        $modele_objet_product['variables']['index']['datatable']['liste_selection_defaut'] = ['type_champs' => 'liste_selection_defaut', 'valeur' => ['visible' => 1]];


        return [

            /////////////////////////////
            /// IMPRIMANTE
            ////////////////////////////
            'print' => [
                'variables' => [
                    'label' => [
                        'papier' => [
                            'nb_colonne' => 3,
                            'nb_ligne' => 8,
                            'largeur_page' => 210,
                            'hauteur_page' => 297,
                            'marge_haut_page' => 0,
                            'marge_bas_page' => 0,
                            'marge_droite_page' => 0,
                            'marge_gauche_page' => 0,
                            'marge_haut_etiquette' => 4,
                            'marge_gauche_etiquette' => 4,
                            'marge_droite_etiquette' => 2,
                            'espace_etiquettesh' => 0,
                            'espace_etiquettesl' => 4,
                        ],
                        'depart' => 1,
                        'modele' => [
                            'type_champs' => 'selection',
                            'choices' => ['simple' => 'simple', 'au_poids' => 'au_poids'],
                            'valeur' => 'simple'
                        ],
                        'classement' => [
                            'type_champs' => 'radio',
                            'choices' => ['name' => 0, 'code' => 1, 'place' => 2],
                            'valeur' => 1
                        ]
                    ]
                ]
            ],


            /////////////////////////////
            /// EN_COURS
            ////////////////////////////

            'en_cours' => [
                'variables' => [
                    'id_entite' => 1
                ]
            ],


            /////////////////////////////
            /// SELECTION
            ////////////////////////////

            'selection' => [
                'variables' => [
                    'product' => []
                ]
            ],


            /////////////////////////////
            /// PAGE CATEGORY
            ////////////////////////////

            'ca' => $modele_objet_category,

            /////////////////////////////
            /// PAGE PRODUCT
            ////////////////////////////

            'product' => $modele_objet_product,


        ];

    }
}

