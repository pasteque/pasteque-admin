<?php

namespace App\Init;

use DateTime;
use Declic3000\Pelican\Init\VariableInterface;

class VariablesConfig extends VariableInterface
{
    function getVariables()
    {
        return [


            'general' => [
                'variables' => [
                    'pays' => 'FR',
                    'langue' => 'fr',
                ],
                'parametrage_par_entite' => true
            ],


            'module' => [
                'variables' => [
                    'paiement' => true,
                ],
                'parametrage_par_entite' => true
            ],


            'affichage' => [
                'variables' => [
                    'barre_recherche' => [
                        'objet' => [
                            'type_champs' => 'checkbox',
                            'choices' => [
                                'individu' => 'individu',
                                'membre' => 'membre'
                            ],
                            'valeur' => 'individu'
                        ],
                    ],
                ],
                'parametrage_par_entite' => true
            ],


            'champs' => [
                'variables' => [
                    'mot' => [
                        'nomcourt' => true
                    ],
                    'motgroupe' => [
                        'nomcourt' => false
                    ]
                ],
                'parametrage_par_entite' => true
            ],


            'liste_diffusion' => [
                'variables' => [
                    'spip' => [
                        'actif' => false,
                        'url' => '',
                        'cle' => ''
                    ],
                    'ovh' => [
                        'actif' => false,
                        'domaine' => '',
                        'applicationkey' => '',
                        'applicationsecret' => '',
                        'endpoint' => '',
                        'consumer_key' => ''
                    ]
                ],
                'parametrage_par_entite' => true
            ],


            'restriction_mode' => [
                'variables' => [
                    'nom' => [
                        'type_champs' => 'radio',
                        'choices' => [
                            'Aucun' => 'aucun',
                            'Mot de l\'operateur' => 'mot_operateur',
                            'Une restriction' => 'restriction'
                        ],
                        'valeur' => 'individu'
                    ]
                    /*
                 * 'non_concerne'=>[
                 * 'type_champs' => 'radio_multiple',
                 * 'choices' => array_flip(tab('operateur')), 'valeur'=>[]
                 * ]
                 */
                ],
                'parametrage_par_entite' => false
            ],

            'document' => [
                'variables' => [
                    'recu_fiscal' => [
                        'annuel' => true,
                        'date_debut' => new DateTime('2020-01-01'),
                        'numero_ordre' => 1
                    ]

                ],
                'parametrage_par_entite' => true
            ],
            'geo' => [
                'variables' => [

                    'position' => [
                        'lat' => '50.5',
                        'lon' => '3',
                        'zoom' => 8
                    ],

                    'fournisseur' => [
                        'url' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        'max_zoom' => 19,
                        'attribution' => '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                        'id' => '',
                        'accessToken' => ''
                    ],
                    'service_geocoding' => [
                        'solution' => [
                            'type_champs' => 'radio',
                            'choices' => [
                                'nominatim.openstreetmap.org' => 'nominatim',
                                'mapquest.com - nominatim' => 'nominatim_mapquest',
                                'mapquest.com - geocoding' => 'mapquest'
                            ],
                            'valeur' => 'nominatim'
                        ],
                        'key' => ''
                    ]
                ]
            ],


            'email' => [
                'variables' => [
                    'email_copie' => '',
                    'from' => 'ne-pas-repondre@simplasso.org',
                    'prefixe_sujet' => '[simplasso] ',
                ],
                'parametrage_par_entite' => true
            ],
            'systeme' => [
                'variables' => [
                    'numero_recu_fiscal' => 0,
                    'periode' => [],

                ],
                'parametrage_par_entite' => true,
                'systeme' => true
            ],


        ];


    }


}
