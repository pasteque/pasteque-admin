<?php

namespace App\Query;


use Declic3000\Pelican\Query\Query;

class SupplierQuery extends Query
{

    public $champs_recherche = ['label', 'reference', 'barcode'];

    public static array $liaisons = [
        'productsupplier' => [
            'objet' => 'productsupplier',
            'local' => 'id',
            'foreign' => 'supplier_id'
        ]
    ];


    function getWhere($params = [], $options = [], $preprefixe = "")
    {

        [$tab_liaisons, $where] = parent::getWhere($params,$options,$preprefixe);
        $pr = $preprefixe.$this->sac->descr('supplier.nom_sql');


        if ($this->requete->ouArgs('active', $params)==1) {
            $where[] = 'active = 1';
        }

        if ($this->requete->ouArgs('isDirect', $params)==1) {
            $where[] = 'is_direct = 1';
        }


        return [$tab_liaisons, $where];

    }

}
