<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Query;


use Declic3000\Pelican\Query\Query;

class ProductQuery extends Query
{

    public $champs_recherche = ['label', 'reference', 'barcode'];

    public static array $liaisons = [
        'productsupplier' => [
            'objet' => 'productsupplier',
            'local' => 'id',
            'foreign' => 'product_id'
        ]
    ];


    function getWhere($params = [], $options = [], $preprefixe = "")
    {

        [$tab_liaisons, $where] = parent::getWhere($params,$options,$preprefixe);
        $pr = $preprefixe.$this->sac->descr('product.nom_sql');

        if ($category = $this->requete->ouArgs('category', $params)) {

            if (!is_array($category)){
                $category = [$category];
            }
            $where[] = 'category_id IN('.implode(',',$category).')';
        }


        if ($supplier = $this->requete->ouArgs('supplier', $params)) {

            if (!is_array($supplier)){
                $supplier = [$supplier];
            }
            $tab_liaisons['productsupplier']='productsupplier';
            $where[] = 'supplier_id IN('.implode(',',$supplier).')';
        }


        if ($this->requete->ouArgs('visible', $params)==1) {
            $where[] = 'visible = 1';
        }


        if ($location = $this->requete->ouArgs('location', $params)) {
            if (!is_array($location))
                $location=[$location];
            $tab_liaisons['products_extra']='products_extra';
            $where[] = 'products_extra.product_id='.$pr.'.id AND products_extra.location IN ('.implode(',',$location).')';
        }



        if ($mots = $this->requete->ouArgs('mot' , $params)) {
            if (! is_array($mots)) {
                $mots = [$mots];
            }
            $operateur = 'in';
            $op = '= ' . count($mots);
            $mg='mot';
            $where[] = ' (select count(mi' . $mg . '.id_mot) from mots_products mi' . $mg . ' where mi' . $mg . '.id_mot ' . $this->operateur_sql($operateur, $mots) . ' AND mi' . $mg . '.product_id=' . $pr . '.id)' . $op;
        }


        if ($this->requete->ouArgs('sans_contenance' , $params)) {
            $where[] = ' scale_value = 0 and visible = 1';
        }
        if ($this->requete->ouArgs('prixkilo_eleve' , $params)) {
            $where[] = '((scale_type= 2 and ((price_sell*1000/scale_value)) > 20 ) or (scale_type= 1 and ((price_sell*1000/scale_value)) > 100 and scale_value > 0) and visible = 1';
        }
        if ($this->requete->ouArgs('prixkilo_tres_eleve' , $params)) {
            $where[] = '((scale_type= 2 and ((price_sell*1000/scale_value)) > 100 )or (scale_type= 1 and ((price_sell*1000/scale_value)) > 500)) and scale_value > 0) and visible = 1';
        }
        if ($this->requete->ouArgs('marge_bizarre' , $params)) {
            $where[] = ' (((price_sell-price_buy)/price_sell * 100) <10)  or (((price_sell-price_buy)/price_sell * 100) > 30 ) and visible = 1';
        }




        return [$tab_liaisons, $where];

    }

}
