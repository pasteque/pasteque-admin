<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Query;


use Declic3000\Pelican\Query\Query;

class SessionQuery extends Query
{

    public $champs_recherche = ['reference', 'label'];

    public function getWhere($params = [], $options = [], $preprefixe = "")
    {

        [$tab_liaisons, $where] = parent::getWhere($params,$options,$preprefixe);
        $pr = $preprefixe.$this->sac->descr('session.nom_sql');


        if ($date_start = $this->requete->ouArgs('date_start', $params)) {
            $where[]= 'open_date >= \''.$date_start->format('Y-m-d H:i:s').'\'';
        }

       if ($date_stop = $this->requete->ouArgs('date_stop', $params)) {
            $where[]= 'open_date <= \''.$date_stop->format('Y-m-d H:i:s').'\'';
        }

        return [$tab_liaisons,$where];

    }


}
