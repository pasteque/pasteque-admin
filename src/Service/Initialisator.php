<?php

namespace App\Service;

use App\Entity\Bloc;
use App\Entity\Composition;
use App\Entity\CompositionBloc;
use App\Entity\Courrier;
use App\Entity\Mot;
use App\Entity\Motgroupe;
use App\Entity\Unite;
use Declic3000\Pelican\Service\Chargeur;


class Initialisator extends \Declic3000\Pelican\Service\Initialisator
{


    function init_app(){

        $nb = $this->db->fetchOne("SELECT count(*) FROM sys_configs");
        if ($nb === 0) {
            $this->config_maj();
        }
        $nb = $this->db->fetchOne("SELECT count(*) FROM sys_preferences");
        if ($nb === 0) {
            $this->preference_maj();
        }
        $nb = $this->db->fetchOne("SELECT count(*) FROM asso_motgroupes");
        if ($nb === 0) {
            $this->initialiser_mot();
        }
        $this->sac->initSac(true);
    }



    function verifier_creer_motgroupe($tab_data, $cle = 'nomcourt')
    {

        $this->chargeur = new Chargeur($this->em);
        $mg = $this->chargeur->charger_objet_by('motgroupe', [$cle => $tab_data['nomcourt']]);
        if (empty($mg)) {
            $mg = new Motgroupe();
        }

        $mg->setNom('Système');
        $mg->setNomcourt('systeme');
        $mg->setSysteme(true);
        $mg->setobjetsEnLien('membre;individu');
        $mg->setImportance('1');
        $mg->setParent(null);
        $this->em->persist($mg);
        $this->em->flush();

    }


    function initialiser_mot()
    {


        $tab_groupe = (new \App\Init\VariablesMotGroupe())->getVariables();

        $tab_motgroupo = [];
        foreach ($tab_groupe as $k => $groupe) {

            $mg = $this->chargeur->charger_objet_by('motgroupe', ['nomcourt' => $k]);
            if (empty($mg)) {
                $mg = new Motgroupe();
            } else{
                $mg = $mg[0];
            }
            $mg->setNom($groupe['nom']);
            $mg->setNomcourt($k);
            $mg->setSysteme(true);
            $mg->setobjetsEnLien($groupe['objetEnLien']);
            $mg->setImportance($groupe['importance']);
            if (isset($groupe['parent']))
                $mg->setParent($tab_motgroupo[$groupe['parent']]);
            $this->em->persist($mg);
            $tab_motgroupo[$k] = $mg;

        }

        $this->em->flush();
        $tab_mots = (new \App\Init\VariablesMot())->getVariables();

        foreach ($tab_mots as $k_mg => $tab_mot) {
            foreach ($tab_mot as $k => $nom) {
                $m = $this->chargeur->charger_objet_by('mot', ['nomcourt' => $k]);
                $m = (empty($m)) ? new Mot() : $m[0];
                $m->setNom($nom);
                $m->setNomcourt($k);
                $m->setMotgroupe($tab_motgroupo[$k_mg]);
                $m->setImportance(1);
                $this->em->persist($m);
            }
        }
        $this->em->flush();

        return true;
    }

}


