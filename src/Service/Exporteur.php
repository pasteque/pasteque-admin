<?php

namespace App\Service;

use App\Component\Table\PaiementTable;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

class Exporteur
{


    protected $requete;
    protected $sac;
    protected $suc;
    protected $session;
    protected $selecteur;
    protected $db;
    protected $em;
    protected $translator;

    public function __construct(Requete $requete, Suc $suc, Sac $sac, Selecteur $selecteur, Connection $db, EntityManagerInterface $em,TranslatorInterface $translator)
    {
        $this->requete = $requete;
        $this->suc = $suc;
        $this->sac = $sac;
        $this->session = $requete->getRequest()->getSession();
        $this->selecteur = $selecteur;
        $this->db = $db;
        $this->em = $em;
        $this->translator = $translator;


    }

    function export_tableur($nom_fichier, $tab_data, $format = 'csv')
    {

        $nom_fichier .= '.' . $format;
        switch ($format) {
            case 'csv':
                header('Content-type: application/vnd.ms-excel');
                header('Content-disposition: attachment; filename="' . $nom_fichier . '"');
                foreach ($tab_data as $ligne) {
                    echo (str_replace(["\n", "\r", "\""], '', implode("\t", $ligne))) . PHP_EOL;
                }
                break;
            case 'ods':
            case 'xls':
            case 'xlsx':

                $spreadsheet = new Spreadsheet();

                $sheet = $spreadsheet->getActiveSheet();
                $sheet->fromArray($tab_data, null, 'A1');

                $content_type = 'application/vnd.oasis.opendocument.spreadsheet; charset=UTF-8';
                if ($format === 'xls') {
                    $content_type = 'application/vnd.ms-excel; charset=UTF-8';
                } elseif ($format === 'xlsx') {
                    $content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8';
                }


                header('Content-Type: ' . $content_type);

                header('Content-Disposition: attachment;filename="' . $nom_fichier . '"');
                header('Cache-Control: max-age=0');

                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');

                // If you're serving to IE over SSL, then the following may be needed
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
                header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header('Pragma: public'); // HTTP/1.0

                $writer = IOFactory::createWriter($spreadsheet, ucfirst((string) $format));
                $writer->save('php://output');
                exit();
                break;


        }
        return new Response('format introuvable');

    }

    function action_export($objet)
    {

        $message = 0;
        $prefs = $this->suc->pref($objet . '.export');

        $objet_selection = $objet;
        if ($objet === 'membrind') {
            $objet = 'individu';
        }


        $args = $this->args_export($objet, $prefs);

        $tab_champs = [];
        $tab_champs_ent = [];

        switch ($objet) {
            case 'membre':
            case 'individu':



                $tab_champs_individu_ent = explode(';',(string) $prefs['colonnes']);
                $tab_colonne_ind_extra = $this->sac->descr($objet.'.colonnes_extra');
                $tab_champs_extra = array_intersect($tab_champs_individu_ent,$tab_colonne_ind_extra);
                $tab_champs_individu = array_diff($tab_champs_individu_ent,$tab_colonne_ind_extra);


                if (!in_array('id_individu',$tab_champs_individu)){
                    $tab_champs_individu[]='id_individu';
                }
                $tab_champs = [$this->sac->descr($objet.'.nom_sql') . '.' . $this->sac->descr($objet . '.cle_sql')];

                $tab_entete = [];

                $pr_ind = $this->sac->descr('individu.nom_sql');
                foreach ($tab_champs_individu_ent as &$ci) {
                    $tab_entete[] = $this->translator->trans($ci);
                }
                foreach ($tab_champs_individu as &$ci) {
                    if ($ci === 'id_membre'){
                        $ci =   'membre.' . $ci;
                    }
                    else
                    {
                        $ci = $pr_ind . '.' . $ci;
                    }

                }

                $tab_champs = array_merge($tab_champs,$tab_champs_individu);

                $tab_champs_ent +=$tab_champs_individu_ent;

                break;

        }



        $this->selecteur->setObjet($objet);

        $sql = $this->selecteur->getSelectionObjet($args, $tab_champs,['individuTitulaire']);


        if (in_array('cotisation_etat',$tab_champs_extra)){
            $tab_cotis=[];
            $sql_id = $this->selecteur->getSelectionObjet($args, 'individu.id_individu');


            $tab_id_prestation = array_keys($this->sac->getPrestationDeType('cotisation'));
            $tab_res = $this->db->fetchAllAssociative('SELECT sri.id_individu as id FROM asso_servicerendus_individus sri WHERE  (select count(sr.id_servicerendu) from asso_servicerendus as `sr` where sr.id_servicerendu = sri.id_servicerendu  and id_prestation IN (' . implode(',', $tab_id_prestation) . ') AND sr.date_fin > NOW() AND sr.date_debut < NOW()) >= 1 and sri.id_individu IN('.$sql_id.')');
            foreach ($tab_res as $res) {
                $tab_cotis[$res['id'] . ''] = 'OK';
            }
            $tab_res = $this->db->fetchAllAssociative('SELECT sri.id_individu as id FROM asso_servicerendus_individus sri WHERE  (select count(sr.id_servicerendu) from asso_servicerendus as `sr` WHERE sr.id_servicerendu = sri.id_servicerendu AND sri.id_individu=sr.id_servicerendu  AND id_prestation IN (' . implode(',', $tab_id_prestation) . ')) AND sri.id_individu IN(' .$sql_id . ')');
            foreach ($tab_res as $res) {
                $tab_cotis[$res['id'] . ''] = 'Jamais';
            }


        }

        $tab_prestation_annee = [];
        $nomcourt_prestation = table_simplifier($this->sac->tab('prestation'), 'nomcourt');
        $tab_prestation_type = $this->sac->getPrestationTypeActive();

        $tab_me_pr_an = null;
        foreach ($tab_prestation_type as $id_prestation_type => $prestation_type) {
            if (in_array($prestation_type, $prefs['ajout_prestation_type'])) {
                $args_sr = [];
                $tab_id_prestation = array_keys($this->sac->getPrestationDeType($prestation_type));
                if (!empty($tab_id_prestation)) {
                    $this->selecteur->setObjet($objet);
                    $sql_id_objet = $this->selecteur->getSelectionObjet($args);
                    $args_sr['sql'] = 'id_membre IN (' . $sql_id_objet . ')';
                    $args_sr['id_prestation'] = $tab_id_prestation;
                    /*if ($prefs['annee']){
                        $args_sr['date_debut']=[$prefs['date_debut']->format('Y-m-d')];
                    }*/
                    $tab_cols_sr = ['YEAR(date_debut) as annee', 'id_prestation', 'id_membre'];
                    $this->selecteur->setObjet('servicerendu');
                    $sql_sr = $this->selecteur->getSelectionObjet($args_sr, $tab_cols_sr);
                   // $tab_nom_type_prestation = table_simplifier($this->sac->tab('prestation_type'));
                   // $nom_type_prestation=$tab_nom_type_prestation[$prestation_type];
                    $tab_cplt_servicerendu = $this->db->fetchAllAssociative($sql_sr);

                    //ordonne les service rendu
                    foreach ($tab_cplt_servicerendu as $sr) {

                        if (!isset($tab_prestation_annee[$prestation_type][$sr['annee']])) {
                            $tab_prestation_annee[$prestation_type][$sr['annee']] = $sr['annee'];
                        }
                        $tab_me_pr_an[$sr['id_membre']][$prestation_type][$sr['annee']][$sr['id_prestation']] = $sr['id_prestation'];
                    }
                }
                $tab_cplt_servicerendu = $tab_me_pr_an;
                foreach ($tab_prestation_annee as $k => $tab_annee) {
                    $tab_annee = array_values($tab_annee);
                    sort($tab_annee);
                    $premiere_annee = $tab_annee[0];
                    if ($premiere_annee == 0) {
                        $premiere_annee = $tab_annee[1];
                    }
                    $derniere_annee = $tab_annee[count($tab_annee) - 1];

                    $tab_temp = [];
                    for ($i = $premiere_annee; $i <= $derniere_annee; $i++) {
                        $tab_temp[] = $i;
                    }
                    $tab_prestation_annee[$k] = $tab_temp;

                }

            }
            // Trier le tableau des années et completer avec les années vides
        }



        $indice = 0;
        $pas_de_servicerendu = [];
        //$tab_entete = ['id', 'nom'] + $tab_champs;
        foreach ($tab_prestation_annee as $prestation => $tab_annee) {
            foreach ($tab_annee as $annee) {
                $tab_entete[] = $prestation . $annee;
                $pas_de_servicerendu[] = '';
            }
        }




        if (isset($prefs['avec_motgroupe'])) {
            foreach($prefs['avec_motgroupe'] as $id_motgroupe){
                $tab_entete[] = $this->sac->tab('motgroupe.' . $id_motgroupe.'.nom');
                $tab_id_mot=[1];
                $this->selecteur->setObjet('individu');
                $sql_id = $this->selecteur->getSelectionObjet($args, 'individu.id_individu');
                $tab_res = $this->db->fetchAllAssociative('SELECT id_individu,id_mot FROM asso_individus_mots im WHERE  id_mot IN ('.implode(',',$tab_id_mot) .') AND im.id_individu IN(' .$sql_id . ') ');





            }

        }


        $statement = $this->db->executeQuery($sql);

        $tab_export = [];
        $tab_mots = [];
        if ($statement) {
            $cle = $this->sac->descr($objet . '.cle_sql');
            while ($val = $statement->fetch()) {



                $tab_sr = [];
                if (isset($tab_cplt_servicerendu[$val[$cle]])) { // ajout les colonnes de service rendu

                    foreach ($tab_prestation_annee as $ktype => $tab_annee) {

                        foreach ($tab_annee as $annee) {
                            if (isset($tab_cplt_servicerendu[$val[$cle]][$ktype][$annee])) {
                                $tab_id_prestation = $tab_cplt_servicerendu[$val[$cle]][$ktype][$annee];
                                foreach ($tab_id_prestation as &$id_prestation) {
                                    $id_prestation = $nomcourt_prestation[$id_prestation];
                                }
                                $tab_sr[$ktype . ' ' . $annee] = implode(',', $tab_id_prestation);
                            } else {
                                $tab_sr[$ktype . ' ' . $annee] = '';
                            }
                        }
                    }
                } else {
                    $tab_sr = $pas_de_servicerendu;
                }
                if (isset($tab_mots[$val[$cle]])) { // ajout les colonnes groupe mots
                    $tab_sr[] = $tab_mots[$cle];
                } else {
                    $tab_sr[] = '';
                }


                if (in_array('cotisation_etat',$tab_champs_extra)){

                    if (isset($tab_cotis[$val['id_individu']])){
                        $val['cotisation_etat'] = $tab_cotis[$val['id_individu']];
                    }
                    else{
                        $val['cotisation_etat'] = 'Echu';
                    }

                }
                $val_tmp = [];
                foreach($tab_champs_ent as $c){
                    if(isset($val[$c])){
                        $val_tmp[] =$val[$c];
                    }
                    else{
                        $val_tmp[] ='';
                    }

                }
                $val = $val_tmp;


                $val += $tab_sr;
                // format de chaque ligne
                $tab_export[] = array_values($val);

                $indice++;
            }
        }

        if ($indice == 0) {
            $message .= 'Aucun adherent' . '<BR />';

        } else {


            $tab_export = ['entete' => $tab_entete] + $tab_export;
            $nom_fichier = 'liste ' . $objet . ' par ' . $this->suc->get('operateur.nom') . ' le ' . date('Y-m-d H:i:s');

            return $this->export_tableur($nom_fichier, $tab_export, $prefs['format']);


        }
        return new Response($message);
    }




    function vcard($objet)
    {

        $message = 0;
        $prefs = $this->suc->pref($objet . '.export_vcard');
        $args = $this->args_export($objet, $prefs);
        if (isset($args)) {
            switch ($objet) {
                case 'membre':
                case 'membrind' :
                    $this->selecteur->setObjet('membre');
                    $where = $this->selecteur->getSelectionObjet($args);
                    break;

                case 'individu' :
                    $this->selecteur->setObjet('individu');
                    if (is_array($args)) {
                        $where = $this->selecteur->getSelectionObjet($args);
                    } else {
                        $where = ' WHERE id_individu  IN(' . $args . ')';
                    }
                    break;
            }
        } else {
            $where = '';
        }
        $tab_champs_auteurs = ['id_individu', 'nom_famille', 'prenom', 'email'];
        $tab_champs_individus_sql = [];
        foreach ($tab_champs_auteurs as $champs_auteur) {
            $tab_champs_individus_sql[] = 'a.' . $champs_auteur;
        }

        switch ($objet) {
            case 'membre':
            case 'membrind' :
                //Récupération de la liste de membre et des informations du titulaire
                $order_by = 'nom_famille,prenom';
                $select = 'SELECT distinct id_membre as id,am.nom as nom,' . implode(',', $tab_champs_individus_sql);
                if ($where) {
                    $where = ' WHERE id_individu_titulaire=id_individu and id_membre IN(' . $where . ')';
                } else {
                    $where = ' WHERE id_individu_titulaire=id_individu ';
                }
                $from = ' FROM asso_individus a, asso_membres am ';
                $order_by = 'nom_famille,prenom';

                break;
            case 'individu' :

                $select = 'SELECT distinct a.id_individu as id, " " as  nom,' . implode(',', $tab_champs_individus_sql);
                $from = ' FROM asso_individus  a ';
                $order_by = 'nom_famille,prenom';
                break;
        }
        $statement = $this->db->executeQuery($select . $from . $where . ' ORDER BY ' . $order_by);


        $indice = 0;
        $export = '';
        if ($statement) {
            while ($val = $statement->fetch()) {
                $export .= "BEGIN:VCARD" . PHP_EOL
                    . "VERSION:3.0" . PHP_EOL
                    . "N:" . $val['nom_famille'] . ";" . $val['prenom'] . ";;;" . PHP_EOL
                    . "FN:" . $val['prenom'] . " " . $val['nom_famille'] . PHP_EOL
                    . "EMAIL;TYPE=INTERNET;TYPE=HOME:" . $val['email'] . PHP_EOL
                    . "CATEGORIES:2017-01-24-AD-Present" . PHP_EOL
                    . "END:VCARD" . PHP_EOL;
                $indice++;
            }
        }
        if ($indice == 0) {
            $message .= 'Aucun adherent' . '<BR />';

        } else {

            $nom_fichier = str_replace(' ', '_', 'liste ' . $objet . ' par ' . $this->suc->get('operateur.nom') . ' le ' . date('Y-m-d_His') . '.vcf');
            $response = new Response($export);
            $response->headers->set('Content-Type', 'text/x-vcard');
            $disposition = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $nom_fichier
            );
            $response->headers->set('Content-Disposition', $disposition);

            return $response;
        }
        return new Response($message);
    }


    function fwrite_stream($fp, $string)
    {
        for ($written = 0; $written < strlen((string) $string); $written += $fwrite) {
            $fwrite = fwrite($fp, substr((string) $string, $written));
            if ($fwrite === false) {
                return $fwrite;
            }
        }
        return $written;
    }

    function args_export($objet, $prefs)
    {

        $args = [];
        $objet_selection = $this->sac->conf('general.membrind') ? 'membrind' : $objet;
        if (isset($prefs['selection'])) {
            if ($prefs['selection'] === 'courante') {

                $args = $this->session->get('selection_courante_' . $objet_selection . '_index_datatable');
            } elseif ($prefs['selection'] !== 'tous') {
                $pref_selection = $this->suc->pref('selection.' . $objet_selection);
                foreach ($pref_selection as $ps) {
                    if ($ps['nom'] == $prefs['selection']) {
                        $args = $ps['valeurs'];
                    }
                }
            }
        }
        return $args;
    }


    function export_fin($objet, $tab, $format = 'csv', $message = '')
    {

        $nom_fichier = str_replace(' ', '_', 'liste ' . $this->translator->trans($objet) . ' par ' . $this->suc->get('operateur.nom') . ' le '
            . date('Y-m-d') . '.' . $format);
        $nb_ligne = count($tab);
        if ($format !== 'vcf') {
            if ($message and $nb_ligne < 4) {
                $message .= '<BR />Aucun résultat dans' . $this->translator->trans($objet) . $this->translator->trans(' avec les critères choisis') . '<BR />';
                return '';
            }
            $nb_col = count($tab[2]);
            if ($nb_col > 1) {
                for ($i = 0; $i < ($nb_col); $i++) {
                    $lettrefin = chr(65 + fmod($i, 26));
                    $lettredebut = (floor($i / 26)) ? chr(64 + floor($i / 26)) : '';
                    $formules[$i] = '=NBVAL(' . $lettredebut . $lettrefin . '4:' . $lettredebut . $lettrefin . ($nb_ligne) . ')';
                }
            }
//            arbre($message); echo 'ligne 29 export :'.$nom_fichier;   arbre($tab);
        }
        switch ($format) {
            case 'csv':
                header('Content-type: application/vnd.ms-excel');
                header('Content-disposition: attachment; filename="' . $nom_fichier . '"');
                $tab[0] = $formules;//todo voir pour ods xls et xlsx provoque ue erreur fichier introuvable
                foreach ($tab as $ligne) {
                    echo (str_replace(["\n", "\r", "\""], '', implode("\t", $ligne))) . PHP_EOL;
                }
                break;
            case 'ods':
            case 'xls':
            case 'xlsx':

                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->fromArray($tab, null, 'A1');

                $content_type = 'application/vnd.oasis.opendocument.spreadsheet';
                if ($format == 'xls') {
                    $content_type = 'application/vnd.ms-excel';
                }
                if ($format == 'xlsx') {
                    $content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                }
                header('Content-Type: ' . $content_type);
                header('Content-Disposition: attachment;filename="' . $nom_fichier . '"');
                header('Cache-Control: max-age=0');
                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');
                // If you're serving to IE over SSL, then the following may be needed
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
                header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header('Pragma: public'); // HTTP/1.0
                $writer = IOFactory::createWriter($spreadsheet, ucfirst((string) $format));
                $writer->save('php://output');
                break;
            case 'vcf':
                header('Content-Type: text/x-vcard');
                header('Content-Disposition: inline; filename= "' . $nom_fichier . '"');
                echo $tab;
                break;

        }
        exit();

    }


}
