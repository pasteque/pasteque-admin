<?php

namespace App\Service;

use App\Entity\Document;
use App\Entity\DocumentLien;
use App\Entity\Image;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Uploader;
use Doctrine\ORM\EntityManagerInterface;



class UploaderImage
{


    protected $sac;
    protected $em;
    protected $uploader;
    protected $synchro;

    function __construct( Sac $sac,EntityManagerInterface $em,Uploader $uploader,Synchro $synchro)
    {
     $this->sac = $sac;
     $this->em = $em;
     $this->uploader = $uploader;
     $this->synchro = $synchro;
    }

    function traitementFormulaireImage($form_image,$ob,$ob_ext,$objet,$usage='logo')
    {
        $nom_champs = $objet.'_'.$ob->getPrimaryKey().'_'.$usage;
        if ($form_image->isSubmitted() && $form_image->isValid()){
            $tab_fichier = $this->uploader->liste_fichier($nom_champs);
            if (!empty($tab_fichier)){

                $ob_ext->verifHasImage();
                $modifier = $ob->getHasImage();
                $fichier = $tab_fichier[0];
                $dir = $this->sac->get('dir.root');
                $path_upload = $dir . 'var/upload';
                if (!file_exists($path_upload)) {
                    if (!mkdir($concurrentDirectory = $path_upload) && !is_dir($concurrentDirectory)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                    }
                }
                $fichier = $path_upload .'/'.$fichier;

                $result = $this->synchro->envoi_image($fichier,$objet,$ob->getPrimaryKey(),$modifier);
                if ($result){
                    if ($modifier){
                        $image=$this->em->getRepository(Image::class)->findOneBy(['modelId'=>$ob->getPrimaryKey(),'model'=>$objet]);
                    }
                    else
                    {
                        $image = new Image();
                        $image->setModel($objet);
                        $image->setModelId($ob->getPrimaryKey());
                    }
                    $ob->setHasImage(true);
                    $image->setImage(file_get_contents($fichier));
                    $image->setMimeType('');
                    $this->em->persist($image);
                    $this->em->persist($ob);
                    $this->em->flush();
                }
            }
        }
    }

}

