<?php

namespace App\Service;


use Declic3000\Pelican\Service\Sac;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;

class ClientAPI
{
    private $api_config;
    private $session;
    private $client;

    function __construct(RequestStack $requestStack, Sac $sac)
    {
        $this->api_config = [
            "user"=>$sac->get('app.api_user'),
            "password"=>$sac->get('app.api_password')
        ];
        $request = $requestStack->getCurrentRequest();
        if ( $request  && $request->hasSession()) {
            $this->session = $requestStack->getSession();
        }
        else {
            $this->session = new Session();
        }
        $this->client=new Client(['base_uri' =>$sac->get('app.api_url')]);
    }


    function loginAPI()
    {
        $path = '/api/login';
        $header = [];
        $body = null;
        $query_params = ['user' => $this->api_config['user'], 'password' => $this->api_config['password']];
        try {
            $reponse = $this->client->post(
                $path, [
                'form_params' => $query_params,
                'headers' => $header,
                'body' => $body
            ]);
        } catch (\Exception $e) {
            throw new UserNotFoundException(sprintf('User "%s" not found.', $this->api_config['user']), 0, $e);
        }
        if (!empty($reponse->getBody()) && $reponse->getStatusCode() == 200) {
            $this->session->set('token_api',\GuzzleHttp\json_decode($reponse->getBody()->getContents()));
            $this->session->set('token_api_timelife',(new \DateTime())->getTimestamp());
        }
    }

    function preparation_header_requete($headerParams){

        $time = (new \DateTime())->getTimestamp() - ((int) $this->session->get('token_api_timelife'));
        if (!$this->session->has('token_api') || $time > 150){
            $this->loginAPI();
        }
        $token_api = $this->session->get('token_api');
        $headers=$headerParams;
        $headers['Token']=  $token_api;
        return $headers;
    }



    function envoyer_requete($method, $path, $options){

           try{
               return $this->client->request($method, $path, $options);
           }
        catch (ServerException $ex){
            print_r($ex->getMessage());
            print_r($ex->getResponse()->getBody()->getContents());
            exit();
        }
    }



    function traitement_reponse($reponse,$json){

        $header_reponse = $reponse->getHeaders();
        if (isset($header_reponse['Token'])){
            $this->session->set('token_api', $header_reponse['Token'][0]);
        }
        $content = $reponse->getBody()->getContents();
        if(!empty($content) && $json) {
            $content = \GuzzleHttp\json_decode($content);
        }
        return [$content,$reponse->getStatusCode(),null];
    }


    function appelAPIFichier($path, $body, $headerParams = [], $method = 'GET',$json=true)
    {
        $headers =$this->preparation_header_requete($headerParams);
        $options=[
            'headers'=>$headers,
            'body' => $body
        ];
        $reponse = $this->envoyer_requete($method, $path, $options);
        return $this->traitement_reponse($reponse,$json);
    }



    function appelAPI($path, $formParams = [], $headerParams = [], $method = 'GET',$json=true)
    {
        $headers =$this->preparation_header_requete($headerParams);
        $options=[
            'headers'=>$headers,
            'form_params' => $formParams
        ];
        $reponse = $this->envoyer_requete($method, $path, $options);
        return $this->traitement_reponse($reponse,$json);
    }
}