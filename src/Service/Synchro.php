<?php

namespace App\Service;

use App\Entity\Image;
use Declic3000\Pelican\Service\Sac;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;

class Synchro
{


    protected $sac;
    protected $em;
    protected $clientAPI;


    function __construct(EntityManagerInterface $em, Sac $sac, ClientAPI $clientAPI)
    {
        $this->em = $em;
        $this->sac = $sac;
        $this->clientAPI = $clientAPI;
        $this->clientAPI->loginAPI();
    }


    function synchronisation()
    {

        [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/sync');
        if ($statut == 200) {
            $reponse = json_decode(json_encode($reponse), true);
            $tab_process =
                [
                    ['cashRegisters', 'cashregister', ['reference'], []],
                    ['paymentmodes', 'paymentmode', ['reference'], ['dispOrder' => 'disp_order']],
                    ['categories', 'category', ['reference'], ['dispOrder' => 'disp_order', 'parent' => 'parent_id']],
                    ['taxes', 'tax', [], []],
                    ['products', 'product', ['reference'], ['dispOrder' => 'disp_order', 'tax' => 'tax_id', 'category' => 'category_id', 'priceBuy' => 'price_buy', 'priceSell' => 'price_sell']],
                    ['places', 'place', [], ['floor' => 'floor_id']],
                    ['floors', 'floor', [], ['dispOrder' => 'disp_order']],
                    ['roles', 'role', ['name'], []],
                    ['users', 'user', ['name'], ['role' => 'role_id']],
                    ['tariffareas', 'tariffarea', ['reference'], []],
                    ['customers', 'customer', [], []],
                    ['discounts', 'discount', [], []],
                    ['discountprofiles', 'discountprofile', [], []],
                    ['resources', 'resource', [], []],
                    ['currencies', 'currency', ['reference'], []]

                ];
            $db=$this->em->getConnection();
            $db->executeQuery('SET FOREIGN_KEY_CHECKS=0;');
            foreach ($tab_process as $process) {
                $nom_process = $process[0];
                if (isset($reponse[$nom_process])) {
                    if ($nom_process === 'roles') {
                        foreach ($reponse['roles'] as &$values) {
                            $values['permissions'] = json_encode($values['permissions']);
                        }
                    }
                    echo($nom_process.PHP_EOL);
                    $this->objectSync($reponse[$nom_process], $process[1], $process[2], $process[3]);
                }
            }
            $db->executeQuery('SET FOREIGN_KEY_CHECKS=1;');
        } else {
            echo('erreur');
            print_r($err);

        }
        return false;
    }


    function reinit_table()
    {
        $db=$this->em->getConnection();
        $db->executeQuery('SET FOREIGN_KEY_CHECKS=0;');
        $db->executeQuery('TRUNCATE TABLE ticketpayments');
        $db->executeQuery('TRUNCATE TABLE ticketlines');
        $db->executeQuery('TRUNCATE TABLE tickettaxes');
        $db->executeQuery('TRUNCATE TABLE tickets');
        $db->executeQuery('TRUNCATE TABLE products');
        $db->executeQuery('TRUNCATE TABLE users');
        $db->executeQuery('TRUNCATE TABLE roles');
        $db->executeQuery('TRUNCATE TABLE categories');
        $db->executeQuery('TRUNCATE TABLE taxes');
        $db->executeQuery('TRUNCATE TABLE currencies');
        $db->executeQuery('TRUNCATE TABLE cashregisters');
        $db->executeQuery('TRUNCATE TABLE sessions');
        $db->executeQuery('TRUNCATE ticketlines');
        $db->executeQuery('TRUNCATE ' . $this->sac->descr('tickettax.table_sql'));
        $db->executeQuery('TRUNCATE ' . $this->sac->descr('ticketpayment.table_sql'));
        $db->executeQuery('TRUNCATE ' . $this->sac->descr('ticket.table_sql'));
        $db->executeQuery('SET FOREIGN_KEY_CHECKS=1;');
    }


    function partialSync($reponse)
    {


    }


    function objectSync($reponse, $object, $fields_uniq_object = [], $tab_rename = []): void
    {


        $table = $this->sac->descr($object . '.table_sql');
        $primary_key = $this->sac->descr($object . '.cle_sql');
        $colonnes = $this->sac->descr($object . '.colonnes');
        $uniq_fields = empty($fields_uniq_object) ? '' : ',' . implode(',', $fields_uniq_object);
        $db=$this->em->getConnection();
        $tab_temp = $db->fetchAllAssociative('SELECT ' . $primary_key . $uniq_fields . ' FROM ' . $table);
        $tab = [$primary_key => []];
        foreach ($fields_uniq_object as $f) {
            $tab[$f][] = '';
        }
        foreach ($tab_temp as $temp) {
            $tab[$primary_key][] = $temp[$primary_key];
            foreach ($fields_uniq_object as $f) {
                $tab[$f][] = $temp[$f];
            }
        }
        if (isset($reponse[0])) {
            foreach (array_keys($reponse[0]) as $k) {
                if ($k !== decamelize($k)) {
                    $tab_rename[$k] = decamelize($k);
                }
            }
        }


        foreach ($reponse as $values) {
            if (!in_array($values[$primary_key], $tab[$primary_key])) {

                foreach ($fields_uniq_object as $f) {
                    if (in_array($values[$f], $tab[$f])) {
                        $values[$f] .= '_copy_' . uniqid();
                    }
                }

                foreach ($tab_rename as $old => $new) {
                    $values[$new] = $values[$old];
                    unset($values[$old]);
                }

                foreach ($values as &$value) {
                    if (is_bool($value))
                        $value = intval($value);
                }
                $values = array_intersect_key($values, $colonnes);
                if (in_array('created_at', array_keys($colonnes))) {
                    $maintenant = (new \DateTime())->format('Y-m-d H:i:s');
                    $values['created_at'] = $maintenant;
                    $values['updated_at'] = $maintenant;
                }
                $db->insert($table, $values);
            }
        }
    }



    function supprimer_image($objet,$id_objet){

        [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/image/'.$objet.'/'.$id_objet,  [],  [], 'DELETE');
        return ($statut === 200);
    }

    function envoi_image($fichier,$objet,$id_objet,$modification=true){

        $body = file_get_contents($fichier);
        $headerParams=['Content-Type'=>"image/jpeg"];
        if (!$modification){
            [$reponse, $statut, $err] = $this->clientAPI->appelAPIFichier('/api/image/'.$objet.'/'.$id_objet,$body,$headerParams,'PUT');
        }
        else{
            [$reponse, $statut, $err] = $this->clientAPI->appelAPIFichier('/api/image/'.$objet.'/'.$id_objet,$body,$headerParams,'PATCH');
        }
        return ($statut === 200);
    }




    function recuperation_image($model, $id)
    {

        [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/image/' . $model . '/' . $id, [], [], 'GET', false);

        if ($statut == 200) {

            $image = new Image();
            $image->setImage($reponse);
            $image->setModel($model);
            $image->setModelId($id);
            $image->setMimeType('');
            $this->em->persist($image);
            $this->em->flush();
            return true;
        }
        return false;

    }

    function synchro_ticket()
    {

        $db = $this->em->getConnection();
        $db->getConfiguration()->setSQLLogger(null);

        $tab_cashregister = table_simplifier($db->fetchAllAssociative('SELECT id FROM cashregisters'), 'id');


        $table = $this->sac->descr('session.table_sql');
        $primary_key = $this->sac->descr('session.cle_sql');



        foreach ($tab_cashregister as $cashregister) {
            $tab_session = $db->fetchAllAssociative('SELECT s.sequence as sequence FROM ' . $table . ' s WHERE (select count(t.id) as nb from tickets t where t.cashregister_id=' . $cashregister.' AND t.sequence = s.sequence )=0 AND cashregister_id=' . $cashregister);

            foreach ($tab_session as $session) {
                try {
                    [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/ticket/session/' . $cashregister . '/' . $session['sequence']);
                } catch (\Exception $ex) {
                    echo($ex->getMessage());
                }
                if ($reponse) {
                    $reponse = json_decode(json_encode($reponse), true);
                    $this->import_tickets($reponse, $cashregister, ((int)$session['sequence']), ['user' => 'user']);
                }
            }
        }

    }


    function import_tickets($data, $cashregister_id, $sequence, $tab_key)
    {
        $db=$this->em->getConnection();
        $primary_key = $this->sac->descr('ticket.cle_sql');
        $table = $this->sac->descr('ticket.table_sql');
        $colonnes = $this->sac->descr('ticket.colonnes');
        $tab_tickets = $db->fetchAllAssociative('SELECT ' . $primary_key . ' as id FROM ' . $table . ' WHERE cashregister_id=' . $cashregister_id . ' and sequence=' . $sequence);
        foreach ($tab_tickets as &$ticket_id) {
            $ticket_id = $ticket_id['id'];
        }


//    if(!empty($tab_tickets)){
//    $db->executeQuery('DELETE FROM '.descr( 'ticketline.table_sql').' WHERE ticket_id IN('.implode(',',$tab_tickets).')');
//    $db->executeQuery('DELETE FROM '.descr( 'tickettax.table_sql').' WHERE ticket_id IN('.implode(',',$tab_tickets).')');
//    $db->executeQuery('DELETE FROM '.descr( 'ticketpayment.table_sql').' WHERE ticket_id IN('.implode(',',$tab_tickets).')');
//    $db->executeQuery('DELETE FROM '.descr( 'ticket.table_sql').' WHERE id IN('.implode(',',$tab_tickets).')');
//    }

        foreach ($data as $values) {

            $tab = [];
            foreach ($values as $k => $v) {
                $tab[decamelize($k)] = $v;
            }
            $values = $tab;

            if (!in_array($values['id'], $tab_tickets)) {
                $values['cashregister_id'] = $cashregister_id;
                $values['discount_rate'] ??= 0;
                $values['sequence'] = $sequence;
                $values['date'] = new \DateTime("@" . $values['date']);
                $values['date'] = $values['date']->format('Y-m-d H:i:s');
                foreach ($tab_key as $k => $key)
                    $values[$k . '_id'] = $values[$key];
                $values_session = array_intersect_key($values, $colonnes);
                $db->insert($table, $values_session);

                $this->import_data_ticket('ticketline', $values['lines'], $values['id'], ['tax' => 'tax', 'product' => 'product'], ['dispOrder' => 'disp_order']);
                $this->import_data_ticket('tickettax', $values['taxes'], $values['id'], ['tax' => 'tax']);
                $this->import_data_ticket('ticketpayment', $values['payments'], $values['id'], ['paymentmode' => 'paymentMode', 'currency' => 'currency'], ['dispOrder' => 'disp_order']);
            }
        }


    }

    function import_data_ticket($objet, $data, $ticket_id, $tab_key, $tab_rename = [])
    {


        $table = $this->sac->descr($objet . '.table_sql');
        $colonnes = $this->sac->descr($objet . '.colonnes');

        $db=$this->em->getConnection();
        $db->delete($table, ['ticket_id' => $ticket_id]);

        if (isset($data[0])) {
            foreach (array_keys($data[0]) as $k) {
                if ($k !== decamelize($k)) {
                    $tab_rename[$k] = decamelize($k);
                }
            }
        }

        foreach ($data as $values) {

            $values['ticket_id'] = $ticket_id;
            foreach ($tab_key as $k => $key)
                $values[$k . '_id'] = $values[$key];

            foreach ($tab_rename as $old => $new) {
                $values[$new] = $values[$old];
                unset($values[$old]);
            }

            $values_session = array_intersect_key($values, $colonnes);

            $db->insert($table, $values_session);

        }

    }


    function synchronisation_session()
    {

        [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/cash/search/?dateStart=1981-01-16', [], [], 'GET', false);
        $reponse = \GuzzleHttp\json_decode($reponse, true);
        $em = $this->em;
        $db = $em->getConnection();
        $db->getConfiguration()->setMiddlewares([new \Doctrine\DBAL\Logging\Middleware(new \Psr\Log\NullLogger())]);
        $table = $this->sac->descr('session.table_sql');
        $primary_key = $this->sac->descr('session.cle_sql');
        $colonnes = $this->sac->descr('session.colonnes');
        $tab_temp = $db->fetchAllAssociative('SELECT ' . $primary_key . ' FROM ' . $table);
        $tab = [$primary_key => []];
        foreach ($tab_temp as $temp) {
            $tab[$primary_key][] = $temp[$primary_key];
        }
        foreach ($reponse as $values) {
            if (!in_array($values[$primary_key], $tab[$primary_key])) {
                $values['cashregister_id'] = $values['cashRegister'];
                foreach (['openDate', 'closeDate', 'csPeriod'] as $key) {
                    $values[decamelize($key)] = $values[$key];
                    unset($values[$key]);
                }
                $values_session = array_intersect_key($values, $colonnes);
                $values_session['cs_fyear'] = $values['csFYear'] + 0;
                $values_session['continuous'] = $values_session['continuous'] + 0;
                $values_session['open_date'] = new \DateTime("@" . $values_session['open_date']);
                $values_session['open_date'] = $values_session['open_date']->format('Y-m-d H:i:s');
                $values_session['close_date'] = new \DateTime("@" . $values_session['close_date']);
                $values_session['close_date'] = $values_session['close_date']->format('Y-m-d H:i:s');

                $db->insert($table, $values_session);
                $id_session = $db->lastInsertId();
                $this->import_data('sessionpayment', $values['payments'], $id_session, ['paymentmode' => 'paymentMode', 'currency' => 'currency']);
                $this->import_data('sessiontaxe', $values['taxes'], $id_session, ['tax' => 'tax'], ['baseFYear' => 'base_fyear', 'amountFYear' => 'amount_fyear']);
                $this->import_data('sessioncat', $values['catSales'], $id_session, []);
                $this->import_data('sessioncattaxe', $values['catTaxes'], $id_session, ['tax' => 'tax']);
                $this->import_data('sessioncustbalance', $values['custBalances'], $id_session, ['customer' => 'customer']);
            }
        }
    }


    /**
     * @throws Exception
     */
    function import_data($objet, $data, $id_session, $tab_key, $tab_rename=[])
    {

        $table = $this->sac->descr($objet . '.table_sql');
        $colonnes = $this->sac->descr($objet . '.colonnes');
        $db = $this->em->getConnection();
        $db->getConfiguration()->setMiddlewares([new \Doctrine\DBAL\Logging\Middleware(new \Psr\Log\NullLogger())]);
        $db->delete($table, ['cashsession_id' => $id_session]);
        foreach ($data as $values) {
            $values['cashsession_id'] = $id_session;
            foreach(array_keys($colonnes) as $key){
                if (isset($values[camelize2($key)]) && $key!=camelize2($key)){
                    $values[$key]=$values[camelize2($key)];
                    unset($values[camelize2($key)]);
                }
            }
            foreach ($tab_key as $k => $key) {
                $values[$k . '_id'] = $values['id'][$key];
            }
            foreach ($tab_rename as $old => $new) {
                $values[$new] = $values[$old];
                unset($values[$old]);
            }
            $values_session = array_intersect_key($values, $colonnes);
            $db->insert($table, $values_session);
        }
    }


    function synchro_images($objet='product')
    {
        $table_sql = $this->sac->descr($objet.'.table_sql');
        $db=$this->em->getConnection();
        $tab_product = $db->fetchAllAssociative('SELECT  id FROM '.$table_sql.' WHERE has_image=1');
        foreach ($tab_product as $id) {
            $id = $id['id'];
            $nb = $db->fetchOne('select count(*) from images where model_id=' . ((int)$id) . ' and model='.$db->quote($objet));
            if ($nb == 0) {
                //recup image
                $this->recuperation_image('product', $id);
            }
        }
    }
}




