<?php

namespace App\Controller;

use Declic3000\Pelican\Service\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;



class InspectorController extends Controller
{
    #[Route(path: '/inspector', name: 'inspector')]
    public function index()
    {

        $action = $this->requete->get('action');
        $args_twig = [
            'action' => $action
        ];

        $objet = 'product';

        $args_url_where = [];
        $tab_possible = [
            'product_sans_contenance',
            'product_prixkilo_eleve',
            'product_prixkilo_tres_eleve',
            'product_marge_bizarre'
        ];
        $args_twig['menu']['product']=$tab_possible;
        if (in_array($action, $tab_possible)) {

            $options_table=['colonnes_exclues'=>['productExtra.labelToPrint','visible']];
            $datatable = $this->createTable($objet,$options_table);
            $args_url_where[substr((string) $action, strlen($objet) + 1)] = true;
        }

        if (isset($datatable)) {
            $args_twig['datatable'] = $datatable->export_twig(true,  $args_url_where, false);

        }

        return $this->render($this->sac->fichier_twig(), $args_twig);
    }
}
