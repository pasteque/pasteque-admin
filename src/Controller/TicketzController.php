<?php

namespace App\Controller;

use App\Entity\CashSession;
use App\Entity\Ticket;
use App\Entity\TicketLine;
use App\Service\Synchro;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Selecteur;
use MongoDB\Driver\Session;
use Symfony\Component\Routing\Annotation\Route;


class TicketzController extends Controller
{
    #[Route(path: '/ticketz', name: 'ticketz')]
    public function index(Synchro $synchro, Selecteur $selecteur)
    {
        $em = $this->em;
        $format = $this->requete->get('format');
        $datefilter = $this->requete->get('datefilter');


        if (!empty($datefilter)) {

            $dates = explode(' - ', (string) $datefilter);
            $date_start = \DateTime::createFromFormat("d/m/Y H:i:s", $dates[0] . ' 00:00:00');
            $date_stop = \DateTime::createFromFormat("d/m/Y H:i:s", $dates[1] . ' 23:59:59');

        } else {
            $date_start = $date_stop = new \DateTime();
            $date_start = \DateTime::createFromFormat("d/m/Y H:i:s", $date_start->format('d/m/Y') . ' 00:00:00');
            $date_stop = \DateTime::createFromFormat("d/m/Y H:i:s", $date_stop->format('d/m/Y') . ' 23:59:59');

        }

        $params = ['date_start' => $date_start, 'date_stop' => $date_stop];

        $selecteur->setObjet('session');
        $tab_session = $selecteur->getTabId($params);

        if ($format === 'csv') {
            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=ticketz.csv");
            $tab_colonne = ['caisse', 'session', 'open date', 'close date', 'close cash', 'close cash', 'Expected cash', 'Ticket count', 'Amount'];
            $tab_tax = $this->sac->tab('tax');
            foreach ($tab_tax as $tax) {
                $tab_colonne[] = 'HT ' . $tax['rate'];
                $tab_colonne[] = $tax['label'];
            }
            $tab_paymentmode = $this->sac->tab('paymentmode');
            foreach ($tab_paymentmode as $payment) {
                $tab_colonne[] = $payment['label'];
            }

            echo (implode("\t", $tab_colonne)) . PHP_EOL;

            $tab_session = $em->getRepository(CashSession::class)->findBy(['id' => $tab_session]);
            foreach ($tab_session as $s) {
                $tab = [
                    $s->getCashregister()->getId(),
                    $s->getId(),
                    $s->getOpenDate()->format('d/m/Y'),
                    $s->getCloseDate()->format('d/m/Y'),
                    $s->getOpenCash(),
                    $s->getCloseCash(),
                    $s->getExpectedCash(),
                    $s->getTicketCount(),
                    $s->getCs()
                ];

                $tab_taxes_q = $s->getTaxes();
                $tab_taxes = [];
                foreach ($tab_taxes_q as $t) {
                    $tab_taxes[$t->getId()['tax']] = $t;
                }

                foreach ($tab_tax as $tax) {
                    $tab[] = isset($tab_taxes[$tax['id']]) ? $tab_taxes[$tax['id']]->getBase() : 0;
                    $tab[] = isset($tab_taxes[$tax['id']]) ? $tab_taxes[$tax['id']]->getAmount() : 0;
                }

                $tab_payments_q = $s->getPayments();
                $tab_payments = [];
                foreach ($tab_payments_q as $p) {
                    $tab_payments[$p->getId()['paymentMode']] = $p;
                }

                foreach ($tab_paymentmode as $payment) {
                    $tab[] = isset($tab_payments[$payment['id']]) ? $tab_payments[$payment['id']]->getAmount() : 0;
                }

                echo (implode("\t", str_replace('.',',',$tab)) . PHP_EOL);

            }

            exit();
        }

        $tab_dates = [];
        $tab_dates["selection"] = [$date_start->format('d/m/Y'), $date_stop->format('d/m/Y')];
        $date = new \DateTime();
        $tab_dates["aujourdhui"] = [$date->format('d/m/Y'), $date->format('d/m/Y')];
        $date = $date->sub(new \DateInterval('P1D'));
        $tab_dates["hier"] = [$date->format('d/m/Y'), $date->format('d/m/Y')];
        $date1 = new \DateTime();
        $date1->sub(new \DateInterval('P7D'));
        $date2 = new \DateTime();
        $tab_dates["7jours"] = [$date1->format('d/m/Y'), $date2->format('d/m/Y')];
        $date1 = new \DateTime();
        $date1->sub(new \DateInterval('P30D'));
        $tab_dates["30jours"] = [$date1->format('d/m/Y'), $date2->format('d/m/Y')];
        $date1 = \DateTime::createFromFormat("d/m/Y", '01/' . date('m') . '/' . date('Y'));
        $date2 = \DateTime::createFromFormat("d/m/Y", '01/' . date('m') . '/' . date('Y'))->add(new \DateInterval('P1M'))->sub(new \DateInterval('P1D'));
        $tab_dates["mois_en_cours"] = [$date1->format('d/m/Y'), $date2->format('d/m/Y')];
        $date1 = \DateTime::createFromFormat("d/m/Y", '01/' . date('m') . '/' . date('Y'))->sub(new \DateInterval('P1M'));
        $date2 = \DateTime::createFromFormat("d/m/Y", '01/' . date('m') . '/' . date('Y'))->sub(new \DateInterval('P1D'));
        $tab_dates["mois_dernier"] = [$date1->format('d/m/Y'), $date2->format('d/m/Y')];
        $tab_dates["date_start"] = $date_start->format('d/m/Y');
        $tab_dates["date_stop"] = $date_stop->format('d/m/Y');


        $args_twig = [
            'tab_cashregister' => $this->sac->tab('cashregister'),
            'tab_paymentmode' => $this->sac->tab('paymentmode'),
            'tab_tax' => $this->sac->tab('tax'),
            'tab_session' => $em->getRepository(CashSession::class)->findBy(['id' => $tab_session]),
            'tab_dates' => $tab_dates
        ];
        return $this->render('sales/ticketz.html.twig', $args_twig);
    }
    #[Route(path: '/session_details', name: 'session_details')]
    public function session_details(Synchro $synchro, Selecteur $selecteur)
    {
        $em = $this->em;
        $id_session = $this->requete->get('id_session');

        $session = $em->getRepository(CashSession::class)->find($id_session);
        $sequence = $session->getSequence();
        $tab_ticket = $em->getRepository(Ticket::class)->findBy(['sequence' => $sequence]);
        $tab_id=[];
        foreach($tab_ticket as $ticket){
            $tab_id[] = $ticket->getId();
        }
        $db=$em->getConnection();
        $tab_ticketline = $db->fetchAllAssociative('select * from ticketlines where ticket_id IN ('.implode(',',$tab_id).')');
        $tab_ticketline = table_regrouper($tab_ticketline,'ticket_id');
        $tab_tickettaxes = $db->fetchAllAssociative('select * from tickettaxes where ticket_id IN ('.implode(',',$tab_id).')');
        $tab_tickettaxes = table_regrouper($tab_tickettaxes,['ticket_id','tax_id']);
        $tab_ticketpayments = $db->fetchAllAssociative('select * from ticketpayments where ticket_id IN ('.implode(',',$tab_id).')');
        $tab_ticketpayments = table_regrouper($tab_ticketpayments,['ticket_id','paymentmode_id']);


        $args_twig = [
            'tab_ticket' => $tab_ticket,
            'tab_ticketline' => $tab_ticketline,
            'tab_tickettaxes' => $tab_tickettaxes,
            'tab_ticketpayments' => $tab_ticketpayments,
            'tab_cashregister' => $this->sac->tab('cashregister'),
            'tab_paymentmode' => $this->sac->tab('paymentmode'),
            'tab_tax' => $this->sac->tab('tax'),

        ];
        return $this->render('sales/session_details.html.twig', $args_twig);
    }

}
