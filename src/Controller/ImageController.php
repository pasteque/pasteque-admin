<?php

namespace App\Controller;


use App\Entity\Image;
use Declic3000\Pelican\Service\Ged;
use PHPImageWorkshop\ImageWorkshop;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;




class ImageController extends Controller
{
    #[Route(path: '/image', name: 'image')]
    public function image(Ged $ged )
    {
        $image_dir = $this->sac->get('dir.cache').'image';
        $model= $this->requete->get('objet');
        $modelId= $this->requete->get('id_objet');
        $type= $this->requete->get('type');
        $nom = $model.$modelId.'.jpg';
        $document = $this->em->getRepository(Image::class )->findOneBy(['model'=>$model,'modelId'=>$modelId]);


        if (!$document)
            return '';

        switch($type){

            case 'miniature' :


                $crop = $this->requete->get('crop');
                $dos = "200x200";
                $dos .= $crop ? "-crop":'';
                $nom_fichier = $ged->saveFile($document, $image_dir, $dos);
                $layer = ImageWorkshop::initFromPath($nom_fichier);
                if ($crop){
                    $layer->cropMaximumInPixel(0, 0, "MM");
                }
                $layer->resizeInPixel(200, null, true);
                $image = $layer->getResult("ffffff");

                ob_get_clean();
                header('Content-type: image/jpg');
                header('Content-Disposition: filename="' . $document->getModel().$document->getModelId() . '.jpg');
                imagejpeg($image, null, 95);
                //unlink($image_dir.$nom_fichier);
                break;
            default :
                ob_get_clean();
                $inline=true;
                $headers = [
                    'Content-type: ' . getMimeTypes('jpg'),
                    'Content-Disposition: ' . (($inline) ? 'inline' : 'attachment') . '; filename="' . $nom . '"'
                ];
                $response = new Response(stream_get_contents($document->getImage()), 200, $headers);
                $disposition = HeaderUtils::makeDisposition(
                    HeaderUtils::DISPOSITION_ATTACHMENT,
                    $nom
                );
                $response->headers->set('Content-Disposition', $disposition);
                return $response;
                break;
        }

    }



}
