<?php

namespace App\Controller;

use App\Entity\Resource;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/resource')]
class ResourceController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'resource_index', methods: 'GET')]
    public function index()
{
    return $this->index_defaut();
}


    
    #[Route(path: '/new', name: 'resource_new', methods: 'GET|POST')]
    public function new()
{
        return $this->new_defaut();
    }

    #[Route(path: '/{label}/edit', name: 'resource_edit', methods: 'GET|POST')]
    public function edit(Resource $ob)
{
    return $this->edit_defaut($ob);
}


    #[Route(path: '/{label}', name: 'resource_delete', methods: 'DELETE')]
    public function delete(Resource $ob)
{
    return $this->delete_defaut($ob);

}

    #[Route(path: '/{label}', name: 'resource_show', methods: 'GET')]
    public function show(Resource $ob)
{
    return $this->show_defaut($ob);

}
}
