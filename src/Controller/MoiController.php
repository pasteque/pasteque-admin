<?php

namespace App\Controller;


use App\Entity\User;
use App\Security\Utilisateur;
use Declic3000\Pelican\Service\Gendarme;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;



#[Route(path: '/moi')]
class MoiController extends ControllerObjet
{
    #[Route(path: '/', name: 'moi')]
    public function show(Gendarme $gendarme)
    {

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();
        $user = $em->getRepository(User::class)->find($this->suc->get('operateur.id'));




        $args_twig = [
            'objet_data' => $user,
        ];




        return $this->render($this->sac->fichier_twig(), $args_twig);
    }

}
