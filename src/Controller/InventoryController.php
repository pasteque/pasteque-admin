<?php

namespace App\Controller;

use Declic3000\Pelican\Service\Controller;
use  Declic3000\Pelican\Service\Requete;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/inventory')]
class InventoryController extends Controller
{
    #[Route(path: '/', name: 'inventory')]
    public function index()
    {
        return $this->render('inventory/index.html.twig', [   ]);
    }



    #[Route(path: '/exportation_kdc', name: 'inventory_exportation_kdc')]
    function exportation_kdc()
    {

        $db = $this->em->getConnection();
        ini_set("auto_detect_line_endings", true);
        $rep_data= $this->sac->get('dir.root').'data/';
        $rep= $rep_data.'inventaire/';
        $fichier = $rep.'kdc.txt';
        if (!file_exists($rep)) {
            if (!file_exists($rep_data)) {
                if (!mkdir($concurrentDirectory = $rep_data) && !is_dir($concurrentDirectory)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                }
            }
            if (!mkdir($concurrentDirectory = $rep) && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }
        $pointeur_fichier = fopen($fichier, "w");
        $nb = 0;
        $tab_produit = $db->fetchAllAssociative('SELECT barcode, trim(SUBSTR(label,1,39)) FROM products WHERE barcode > 99999 and visible=1 order by barcode');
        fwrite($pointeur_fichier, "code barre\tlibelle\r\n");
        foreach ($tab_produit as $produit) {
            fwrite($pointeur_fichier, utf8_decode(implode("\t", $produit)) . "\r\n");
            $nb++;
        }
        fclose($pointeur_fichier);
        $this->addFlash('success', $nb . '  produits exportés, prêt à être importé dans le KDC');
        return $this->redirectToRoute('inventory');

    }

    #[Route(path: '/exportation_ods', name: 'inventory_exportation_ods')]
    function exportation_ods()
    {

        $db = $this->em->getConnection();
        ini_set("auto_detect_line_endings", true);
        $fichier = $this->sac->get('dir.root').'data/inventaire/tableau_des_produit.csv';
        $pointeur_fichier = fopen($fichier, "w");
        $nb = 0;
        $tab_produit = $db->fetchAllAssociative('SELECT p.barcode, trim(p.label),\'\' as qt,trim(c.label) FROM products p, categories c WHERE c.id=p.category_id order by p.category_id, p.reference ');
        fwrite($pointeur_fichier, "code barre\tlibelle\tquantity\tcategorie\n");
        foreach ($tab_produit as $produit) {
            fwrite($pointeur_fichier, implode("\t", $produit) . "\n");
            $nb++;
        }
        fclose($pointeur_fichier);
        $this->addFlash('success', $nb . ' produits exportés, prêt à être intégrer du doc ods');
    return $this->redirectToRoute('inventory');

    }







    #[Route(path: '/inventory_import', name: 'inventory_import')]
    public function inventory_import()
    {

        $db = $this->em->getConnection();
        $db->executeQuery('CREATE TABLE IF NOT EXISTS `stocks` ( `id_stock` INT NOT NULL AUTO_INCREMENT , `product_id` INT NOT NULL , `quantity` FLOAT NOT NULL , PRIMARY KEY (`id_stock`)) ENGINE = InnoDB;');
        $db->executeQuery('Truncate `stocks`;');
        ini_set("auto_detect_line_endings", true);
        $fichier = $this->sac->get('dir.root').'data/inventaire/inventaire.csv';
        $pointeur_fichier = fopen($fichier, "r");
        $nb = 0;
        $id_depart = time();
        if ($pointeur_fichier <> 0) {
            $db->executeQuery('UPDATE  stocks SET quantity = 0');
            fgets($pointeur_fichier, 4096);
            while (!feof($pointeur_fichier)) {
                $ligne = fgets($pointeur_fichier, 4096);
                $tab = explode("\t", $ligne);

                //print_r($tab);
                if (!empty($tab[0])) {
                    $quantite = (float)str_replace(',', '.', $tab[2]);

                    if ($quantite > 0) {

                        $tab_produit = $db->fetchAllAssociative('select id,price_buy from products where barcode=\'' . $tab[0] . '\'');

                        if (isset($tab_produit[0])) {

                            if ($quantite > 0) {
                                $tab_current = $db->fetchAllAssociative('select product_id from stocks where product_id =\'' . $tab_produit[0]['id'] . '\'');

                                if (empty($tab_current))
                                    $db->executeQuery('INSERT into stocks (product_id,quantity) VALUES (\'' . $tab_produit[0]['id'] . '\',' . $quantite . ')');
                                else
                                    $db->executeQuery('UPDATE  stocks SET quantity = quantity+' . $quantite . ' where product_id=\'' . $tab_produit[0]['id'] . '\'');
                            }
                        } else {
                            $this->addFlash('error','Produit introuvable<br>');
                        }
                    }
                } elseif (!empty($tab[1])) {

                    $quantite = floatval(str_replace(',', '.', $tab[2]));
                    if ($quantite > 0) {

                        $tab_produit = $db->fetchAllAssociative('select id,priceBuy from products where label=\'' . str_replace("'", "''", utf8_decode($tab[1])) . '\'');

                        if (isset($tab_produit[0])) {


                            if ($quantite > 0) {
                                $tab_current = $db->fetchAllAssociative('select product_id from stocks where product_id =\'' . $tab_produit[0]['id'] . '\'');
                                if (empty($tab_current))
                                    $db->executeQuery('INSERT into stocks (product_id,quantity) VALUES (\'' . $tab_produit[0]['id'] . '\',' . $quantite . ')');
                                else
                                    $db->executeQuery('UPDATE  stocks SET quantity = quantity+' . $quantite . ' where product_id=\'' . $tab_produit[0]['id'] . '\'');
                            }
                        } else {
                            echo('select id,priceBuy from products where label=\'' . str_replace('\'', '\'\'', utf8_encode($tab[1])) . '\'');
                            $this->addFlash('error','Produit introuvable2');
                        }
                    }
                }


                $nb++;
            }
            $this->addFlash('success', $nb . ' lignes d\'inventaire ajoutées');
        }
        return $this->redirectToRoute('inventory');
    }




    #[Route(path: '/result', name: 'inventory_result')]
    function inventory_result()
    {

        ini_set("auto_detect_line_endings", true);
        $db = $this->em->getConnection();

        $tab_entete = ["code barre","libelle","prix_achat","prix_vente","prix_vente_ttc","taxe","quantite","total HT","Total TTC"];
        $nb = 0;
        $tab_produit =  $db->executeQuery('select
                                            p.id as id,
                                            c.label as categorie,
                                            p.barcode as code_barre,
                                            p.label as name,
                                            p.price_buy as prix_achat,
                                            round(p.price_sell,2) as prix_vente,
                                            round(p.price_sell+(p.price_sell*t.rate),2) as prix_vente_ttc,
                                            t.RATE*100 as taxe
                                            from products p
                                            LEFT OUTER JOIN categories c ON p.category_id=c.ID 
                                            LEFT OUTER JOIN taxes t ON p.tax_id=t.id 
                                            WHERE p.reference not like \'%_deleted_%\' 
                                            ORDER BY c.parent_id,c.id,code_barre');

        $total_ttc=0;
        foreach ($tab_produit as $produit) {
            $units = $db->fetchAllAssociative('select quantity from stocks s where s.product_id=\'' . $produit['id'] . '\'');
            $categorie=$produit['categorie'];
            unset($produit['id']);
            unset($produit['categorie']);
            $produit['quantite'] = 0;
            if (!empty($units[0]))
                $produit['quantite'] = $units[0]['quantity'];
            $produit['total_ht'] = '' . ($produit['quantite'] * $produit['prix_achat']);
            $total_ttc += $produit['total_ht'];
            foreach ($produit as &$p) {
                $p = str_replace('.', ',', $p);
            }
            $tab_prod[$categorie][]=$produit;
            $nb++;
        }

        $fichier = $this->sac->get('dir.root').'data/inventaire/inventaire_complet.csv';
        $pointeur_fichier = fopen($fichier, "w");
        fwrite($pointeur_fichier, implode("\t",$tab_entete));
        $cat_temp='';
        foreach($tab_prod as $cat=>$tab_prod0){
            foreach($tab_prod0 as $produit){
                if ($cat !== $cat_temp){
                fwrite($pointeur_fichier, "\n\n" . $cat . "\n");
                }
                fwrite($pointeur_fichier, implode("\t", $produit) . "\n");
                $cat_temp = $cat;
            }
        }

        fclose($pointeur_fichier);


        return $this->render('inventory/result.html.twig', ["tab_produit"=>$tab_prod,'total_ttc'=>$total_ttc]);

    }




}
