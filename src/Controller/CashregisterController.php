<?php

namespace App\Controller;

use App\Entity\CashRegister;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;




#[Route(path: '/cashregister')]
class CashregisterController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'cashregister_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/new', name: 'cashregister_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'cashregister_edit', methods: 'GET|POST')]
    public function edit(CashRegister $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'cashregister_delete', methods: 'DELETE')]
    public function delete(CashRegister $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'cashregister_show', methods: 'GET')]
    public function show(CashRegister $ob)
    {
        return $this->show_defaut($ob);

    }
}