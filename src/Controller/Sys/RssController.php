<?php

namespace App\Controller\Sys;

use _PHPStan_690619d82\Nette\Utils\DateTime;
use Declic3000\Pelican\Service\Sac;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


#[Route(path: '/rss')]
class RssController extends AbstractController
{


    public function __construct(private readonly Sac $sac, private readonly string $url_rss)
    {
    }

    #[Route(path: '/', name: 'rss')]
    public function index()
    {
        $dir_cache = $this->sac->get('dir.cache');
        $fichier_tmp = $dir_cache.'rss.xml';
        if(!(file_exists($fichier_tmp) && filemtime($fichier_tmp)-(time()+2*60*60)>0)){

            $content = file_get_contents($this->url_rss);
            file_put_contents($fichier_tmp,$content);
        }


        $request = file_get_contents($fichier_tmp);

        $xml = simplexml_load_string($request,\SimpleXMLElement::class,);
        $nb=count($xml->channel->item);
        $nb_max= min(4,$nb);
        $arg_twig=[];

        define('XMLNS_DUBLIN_CORE', 'http://purl.org/dc/elements/1.1/');
        for ($i = 0; $i < $nb_max; $i++) {
            $dc = $xml->channel->item[$i]->children(XMLNS_DUBLIN_CORE);
            $date= new \DateTime($dc->date);
            $arg_twig['tab_article'][$date->getTimestamp()] = [
                'title'=>$xml->channel->item[$i]->title,
                'description'=> substr(strip_tags($xml->channel->item[$i]->description),0,150).'...',
                'link'=>$xml->channel->item[$i]->link,
                'date_publication'=>$date,
                'creator'=>$dc->creator,
                'language'=>$dc->language,
                'format'=>$dc->format

            ];
        }
        return $this->render('sys/rss.html.twig', $arg_twig);
    }
}
