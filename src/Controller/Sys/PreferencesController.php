<?php

namespace App\Controller\Sys;


use App\Component\Form\PreferenceFormBuilder;
use Declic3000\Pelican\Service\Controller;
use  Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/preference')]
class PreferencesController extends Controller
{
    #[Route(path: '/', name: 'preferences')]
    public function index(Suc $suc, PreferenceFormBuilder $pref_form)
    {
        $tab_entree = $this->sac->get('tab_preference');
        $args_twig = [
            'tab_entree' => array_keys($tab_entree),
            'default' => !empty($this->requete->get('default')) 
        ];
        $k = $this->requete->get('pref');
        if ($k && isset($tab_entree[$k])) {
            $args_twig['nom_form'] = $k;
            $entree = $tab_entree[$k];
            $data = $suc->pref($k);
            if (is_string($data)) {
                $data = null;
            }
            $action=$args_twig['default']?'preference':'preference_default';
            $form = $pref_form->transforme_en_formulaire($k, $entree['variables'], $data, $action, 100);
            $args_twig['form'] = $form->createView();

        }
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }





    #[Route(path: '/editvalue/{pref}', name: 'preference_edit_value')]
    public function editValue(string $pref, PreferenceFormBuilder $pref_form)
    {
        $valeur = $this->requete->get('valeur');
        $pref_form->ecrire_preference($pref, $valeur);
        $tab_result = [
            'ok' => true,
            'message' => 'La preference a été enregistrée'
        ];
        $this->suc->initPreference();
        return $this->json($tab_result);
    }


    #[Route(path: '/edit/{pref}', name: 'preference_edit')]
    public function edit(string $pref, Suc $suc, PreferenceFormBuilder $pref_form)
    {

        $pref = str_replace('|', '.', $pref);

        $args_rep = ['url_redirect' => $this->requete->get('redirect')];
        $niveau = $this->requete->get('niveau', 1);
        $form = $pref_form->construire_form_preference($pref, $niveau, []);
        return $this->reponse_formulaire($form, $args_rep, 'inclure/form.html.twig');

    }


    #[Route(path: '/selection/new', name: 'preference_selection_new')]
    public function selection_new(Session $session, PreferenceFormBuilder $pref_form)
    {

        $data = [];
        $args_rep = [];
        $objet = $this->requete->get('objet');

        $builder = $this->pregenerateForm('ajout_selection', FormType::class, $data, [], false, ['objet' => $objet]);


        $builder = $builder
            ->add('nom', TextType::class, ['label' => 'Nom de la selection'])
            ->add('description', TextareaType::class, ['label' => 'Description de la selection']);
        $form = $builder->add('submit', SubmitType::class, ['label' => 'Enregistrer', 'attr' => ['class' => 'btn-primary']])
            ->getForm();

        $form->handleRequest($this->requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {
                $selection = $this->suc->pref('selection.' . $objet);

                if (empty($selection)) {
                    $selection = [];
                }

                $vselection = $session->get('selection_courante_' . $objet . '_index_datatable');


                $selection[] = [
                    'nom' => $data['nom'],
                    'description' => $data['description'],
                    'valeurs' => $vselection
                ];

                $pref_form->ecrire_preference('selection.' . $objet, $selection);
                $this->suc->initPreference();

                $args_rep['message'] = 'selection_ajouter_ok';
                $args_rep['url_redirect'] = '';
                $args_rep['vars']['modif_html']['choix_selection'] = $this->renderView('inclure/selection.html.twig', ['objet_bloc' => $objet]);
                $args_rep['vars']['declencheur_js'] = 'rafraichirSelection';


            }

        }

        return $this->reponse_formulaire($form, $args_rep, 'inclure/form.html.twig');


    }

    #[Route(path: '/selection/delete/{objet}/{num}', name: 'preference_selection_delete')]
    public function selection_delete(string $objet, $num, PreferenceFormBuilder $pref_form)
    {
        $selection = $this->suc->pref('selection.' . $objet);
        unset($selection[$num]);
        $pref_form->ecrire_preference('selection.' . $objet, $selection);

        $tab_result = [
            'ok' => true,
            'message' => 'La selection a bien été supprimée'
        ];
        $this->suc->initPreference();
        return $this->json($tab_result);
    }


}
