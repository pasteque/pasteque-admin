<?php

namespace App\Controller\Sys;



use Declic3000\Pelican\Service\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VarsJsController extends Controller
{
    #[Route(path: '/vars.js', name: 'vars_js')]
    public function index()
    {


        $fichier_js = $this->sac->get('dir.cache').'vars.js';
        if (!file_exists($fichier_js) || $this->requete->get('init')) {
            $args = [];
            file_put_contents($fichier_js, $this->renderView('sys/vars_js.js.twig', $args));
        }
        $response = new Response(file_get_contents($fichier_js));
        $response->headers->set('Content-Type', 'text/javascript');

        return $response;
    }
}
