<?php

namespace App\Controller\Sys;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class ErreurController extends AbstractController
{
    #[Route(path: '/erreur403', name: 'erreur403')]
    public function erreur403()
    {
       return new Response('erreur 403');
    }


    



}
