<?php

namespace App\Controller\Sys;

use Declic3000\Pelican\Service\Controller;
use Symfony\Component\Routing\Annotation\Route;


class CreditsController extends Controller
{
    #[Route(path: '/credits', name: 'credits')]
    public function index()
    {
        $fichier = __DIR__.'/../../.version';
        $version = '???';
        if (file_exists($fichier)){
            $version = file_get_contents($fichier);
        }

        $args_twig = [
            'version' => $version
        ];

        return $this->render($this->sac->fichier_twig($args_twig));
    }


}
