<?php

namespace App\Controller\Sys;


use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Facteur;;
use  Declic3000\Pelican\Service\LogMachine;;
use  Declic3000\Pelican\Service\Requete;
use  Declic3000\Pelican\Service\Suc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route(path: '/tools')] // Security("is_granted('ROLE_Administrator')"))
class ToolsController extends Controller
{
    #[Route(path: '/', name: 'tools')]
    public function index(UserInterface $user)
    {
        $onglet = $this->requete->get('onglet');
        $conf = [
            'ENV' => $_SERVER['APP_ENV']
        ];

        $options_table = [];
     //   $tache_table = $this->createTable('tache', $options_table);
        $args_twig=[
            'onglet'=>$onglet,
            'conf'=>$conf,
            'user'=>$user,
            //'tache_table'=>$tache_table->export_twig()
        ];
        return $this->render('sys/tools.html.twig', $args_twig);
    }

    
    #[Route(path: '/vider_cache', name: 'vider_cache')]
    public function vider_cache()
    {

        $tmp = __DIR__.'/../../../var';

        if (file_exists($tmp.'/cache/vars.js')){
            unlink($tmp.'/cache/vars.js');
            $this->addFlash('info', 'La table des configurations a bien été réinitialisé');
        }
        return $this->redirectToRoute('tools');

    }
    
    
    #[Route(path: '/rafraichir', name: 'rafraichir')]
    public function rafraichir()
    {
        $this->sac->clear();
        $this->sac->initSac(true);
        $this->addFlash('info', 'La table des configurations a bien été réinitialisé');
        return $this->redirectToRoute('tools');
    }





    #[Route(path: '/timeline', name: 'timeline')]
    public function timeline(Suc $suc,LogMachine $log)
    {
        $args_twig = $suc->pref('timeline.defaut');
        $args_twig['tab_operations'] = $log->getTimeline();
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }



    #[Route(path: '/test_mail', name: 'test_mail')]
    public function test_mail(Facteur $facteur)
    {
        $email = $this->getParameter('monolog.mailer.to_email');
        $ok = $facteur->courriel_twig($email, 'test', ['date'=>new \DateTime()]);
        if($ok){
            $this->addFlash('info', 'L\'email de test vient d\'être envoyer à cette adresse:'.$email);
        }else{
            $this->addFlash('error', 'Problème dans l\'envoi de l\'email, vérifier la configuration');
        }
        return $this->redirectToRoute('tools');
    }


}
