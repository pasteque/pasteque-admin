<?php

namespace App\Controller\Sys;

use App\Component\Form\PreferenceFormBuilder;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\Controller;
#[Route(path: '/config')]
class ConfigController extends Controller
{
    #[Route(path: '/', name: 'config')]
    public function index(PreferenceFormBuilder $pref_form)
    {
        include_once ('../src/inc/variables_config.php');
        $tab_entree = variables_config();
        $choix = [];
        foreach ($tab_entree as $k => $entree) {
            if (!isset($entree['systeme'])) {
                $data = $this->sac->conf($k);
                if (empty($data)){
                    $data=null;
                }
                $form =  $pref_form->transforme_en_formulaire($k, $entree['variables'],$data,'config',100);
                $choix[$k] = $form->createView();
            }
        }
        $args_twig = ['choix'=>$choix];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }



    
   


}
