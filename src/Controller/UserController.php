<?php

namespace App\Controller;

use App\Entity\User;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/user')]
class UserController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'user_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/new', name: 'user_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'user_edit', methods: 'GET|POST')]
    public function edit(User $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'user_delete', methods: 'DELETE')]
    public function delete(User $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'user_show', methods: 'GET')]
    public function show(User $ob)
    {
        return $this->show_defaut($ob);

    }
}
