<?php

namespace App\Controller;

use App\Entity\Currency;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/currency')]
class CurrencyController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'currency_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/new', name: 'currency_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'currency_edit', methods: 'GET|POST')]
    public function edit(Currency $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'currency_delete', methods: 'DELETE')]
    public function delete(Currency $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'currency_show', methods: 'GET')]
    public function show(Currency $ob)
    {
        return $this->show_defaut($ob);

    }
}