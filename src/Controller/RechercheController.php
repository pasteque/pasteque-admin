<?php
namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;

class RechercheController extends ControllerObjet
{

    
    #[Route(path: '/recherche', name: 'recherche')]
    public function index()
    {

        if ($this->requete->estAjax() && $this->requete->get('action') == 'json') {
            $tab_data = [];
            $objet='product';
            $table = $this->createTable($objet);
            $tab_data[$objet] = $table->liste_rechercher();
            
            
            $tab = [];
            foreach($tab_data as $objet => &$data ){
                $ob = $this->trans($objet);
                foreach($data as $d){
                    $d['objet'] = $ob;
                    $d['objet_url'] = $objet;
                    $tab[] = $d;
                }
            }
            
           
            return $this->json($tab);
        } else {
            
            $recherche = $this->requete->get('recherche');
            $args =['search'=>['value'=>$recherche]];


            $objet='product';
            $table = $this->createTable($objet);
            $tab_data[$objet] = $table->export_twig(false,$args);


            $args_twig = ['recherche'=>$recherche, 'tab_result' => $tab_data];
            return $this->render($this->sac->fichier_twig(), $args_twig);
        }
    }
}
