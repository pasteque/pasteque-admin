<?php

namespace App\Controller;

use App\Entity\Customer;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/customer')]
class CustomerController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'customer_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/new', name: 'customer_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'customer_edit', methods: 'GET|POST')]
    public function edit(Customer $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'customer_delete', methods: 'DELETE')]
    public function delete(Customer $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'customer_show', methods: 'GET')]
    public function show(Customer $ob)
    {
        return $this->show_defaut($ob);

    }
}