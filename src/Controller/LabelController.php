<?php

namespace App\Controller;

use App\Component\Form\PreferenceFormBuilder;
use App\Entity\ProductExtra;
use App\Entity\Product;
use App\Form\LabelType;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Selecteur;
use Knp\Snappy\Pdf;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class LabelController extends Controller
{


    #[Route(path: '/label_index', name: 'label_index')]
    public function index(PreferenceFormBuilder $preferenceFormBuilder,Pdf $pdf)
    {
        $session= $this->getSession();
        $tab_pe = $this->em->getRepository(ProductExtra::class)->findBy(['labelToPrint'=>true]);
        $args_twig = ['tab_pe' => $tab_pe];

        if (!empty($tab_pe)){
        $args_rep = [];
        $pref = $this->suc->pref('print.label');

        $product_id = $this->requete->get('product_id'); // impression d'une étiquette
        $data = [
            'depart' => $pref['depart']??  0,
            'modele' => 'au_kilo'
        ];


        if ($product_id) {
            $product_id = is_array($product_id) ? $product_id : [$product_id];
            $nb_label = count($product_id);
        } else {
            $objet='product';
            $valeur_selection = $session->get('selection_courante_'.$objet.'_index_datatable');
            $db=$this->em->getConnection();
            $selecteur = new Selecteur($this->requete,$db,$this->sac,$this->suc);
            $selecteur->setObjet('product');
            $tab_id=[];
            if ($valeur_selection){
                $tab_id= $selecteur->getTabId($valeur_selection);
            }
            $nb_label = count($tab_id);
        }
        $options = ['cols'=>$pref['papier']['nb_colonne'],'rows'=>$pref['papier']['nb_ligne'],'attr'=>['class'=>'ajaxjson_form']];
        $builder = $this->pregenerateForm('label', LabelType::class, $data,$options,true,['product_id'=>$product_id]);

        $form = $builder->add('submit', SubmitType::class,
            ['label' => $this->trans('generer le PDF'), 'attr' => ['class' => 'btn-primary antiblocagegit']])
            ->getForm();


        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $data = $form->getData();

                if ($preferenceFormBuilder->ecrire_preference('print.label', $data)) {
                    $this->suc->initPreference();
                }
                $data['papier'] = $this->suc->pref('print.label.papier');
                $args_rep=[];
                if ($product_id){
                    $args_rep['id'] = $product_id;
                }
                $args_rep['vars']=$this->print_label($selecteur,$pdf,$product_id);
                $args_rep['vars']['declencheur_js']='print_pdf_form';
                $args_rep['message']='';
                $args_rep['url_redirect']='';
                $args_twig['nb_label'] = $nb_label;
                $args_twig['js_init'] = 'position_table';
                $args_twig['form'] = $form->createView();
                $args_rep['html']=$this->renderView('inclure/form.html.twig', $args_twig);
                return $this->reponse_formulaire($form,$args_rep);
            }
        }
        $args_twig['nb_label'] = $nb_label;
        $args_twig['js_init'] = 'position_table';
        $args_twig['form_label'] = $form->createView();
        }
        return $this->render('label/index.html.twig', $args_twig);
    }


    #[Route(path: '/label/delete_list', name: 'label_delete_list')]
    public function label_delete_list()
    {
        $tab_pe = $this->em->getRepository(ProductExtra::class)->findBy(['labelToPrint' => true]);
        foreach ($tab_pe as $pe){
            $pe->setLabelToPrint(false);
            $this->em->persist($pe);
        }
        $this->em->flush();
        return $this->redirection('label_index');
    }



        #[Route(path: '/label_print_product', name: 'label_print_product')]
    public function label_print_product(Selecteur $selecteur,Pdf $pdf,PreferenceFormBuilder $preferenceFormBuilder)
    {

        $tab_pe = $this->em->getRepository(ProductExtra::class)->findBy(['labelToPrint'=>true]);
        $args_twig = ['tab_pe' => $tab_pe];

        $args_rep = [];
        $pref = $this->suc->pref('print.label');

        $product_id = $this->requete->get('product_id'); // impression d'une étiquette
        $data = [
            'depart' => $pref['depart']??  0
        ];


        $product_id = is_array($product_id) ? $product_id : [$product_id];
        $nb_label = count($product_id);

        $options = ['cols'=>$pref['papier']['nb_colonne'],'rows'=>$pref['papier']['nb_ligne']];
        $form_pref= $preferenceFormBuilder->construire_form_preference('print.label.papier');
        $builder = $this->pregenerateForm('label', LabelType::class, $data,$options,true,['product_id'=>$product_id]);



        $form = $builder->add('submit', SubmitType::class,
            ['label' => $this->trans('generer le PDF'), 'attr' => ['class' => 'btn-primary']])
            ->getForm();


        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $data = $form->getData();

                if ($preferenceFormBuilder->ecrire_preference('print.label', $data)) {
                    $this->suc->initPreference();
                }
                $data['papier'] = $this->suc->pref('print.label.papier');
                $args_rep=[];
                if ($product_id)
                    $args_rep['id'] = $product_id;
                $args_rep['vars']=$this->print_label($selecteur,$pdf,$product_id);
                $args_rep['vars']['declencheur_js']='print_pdf_form';
                $args_rep['message']='Impression en cours...';
                $args_rep['url_redirect']='';
            }
        }
        $args_rep['nb_label'] = $nb_label;
        $args_rep['js_init'] = 'position_table';
        $args_rep['form_pref'] = $form_pref;
        return $this->reponse_formulaire($form, $args_rep);
    }


    #[Route(path: '/label_print', name: 'label_print')]
    function label_print(Selecteur $selecteur,  Pdf $knpSnappyPdf)
    {
        $product_id = $this->requete->get('id');
        return $this->print_label($selecteur,$knpSnappyPdf,$product_id);
    }



    #[Route(path: '/label_pdf/{fichier}', name: 'label_pdf')]
    function label_pdf(string $fichier)
    {

        if (preg_match('`^label_[0-9\-]+\.pdf$`ui',$fichier)===1){
            $image_dir = $this->sac->get('dir.root') . 'var/print/';
            $outputfile = $image_dir . $fichier;
            if (file_exists($outputfile)) {
                $inline = true;
                $response = new BinaryFileResponse($outputfile);
                $disposition = HeaderUtils::makeDisposition(
                    $inline ? HeaderUtils::DISPOSITION_INLINE : HeaderUtils::DISPOSITION_ATTACHMENT,
                    basename($outputfile)
                );
                $response->headers->set('Content-Disposition', $disposition);
                return $response;
            }
        }
        return $this->redirection('404');
    }


    function mm2px($val, $dpi = 123)
    {

        return ($val * $dpi / 25.4);
    }


    function px2mm($val, $dpi = 123)
    {

        return ($val * 25.4) / $dpi;
    }




    function print_label(Selecteur $selecteur, Pdf $knpSnappyPdf, $product_id, $format = 'json')
    {

        // Chargement des paramètres de configuration

        $preference = $this->suc->pref('print.label');
        $pref = $preference['papier'];

        $modele = $preference['modele'];
        $depart = $preference['depart'];
        $em = $this->em;

        if ($product_id) {
            $tab_products = $em->getRepository(Product::class)->findBy(['id'=>$product_id]);
        } else {
            $objet='product';
            $selecteur->setObjet($objet);
            $tab_pe = $this->em->getRepository(ProductExtra::class)->findBy(['labelToPrint'=>true]);
            foreach($tab_pe as $pe){
                $tab_id[]=$pe->getProduct()->getId();
            }
            //$valeur_selection = $session->get('selection_courante_'.$objet.'_index_datatable');
            //$tab_id= $selecteur->getTabId($valeur_selection);

            $tab_products = $em->getRepository(Product::class)->findBy(['id'=>$tab_id]);

        }


        $nb_total = count($tab_products);

        $loader = new FilesystemLoader($this->sac->get('dir.root') . 'documents');
        $twig_page = new Environment($loader, ['autoescape' => false]);
        $generatorHTML = new BarcodeGeneratorHTML();


        $erreurs = [];
        if ($nb_total == 0) {
            return ('Aucune étiquette produit à imprimer');
        } else {


            $nb_colonne = max($pref['nb_colonne'], 1);
            $nb_ligne = $pref['nb_ligne'];
            $largeur_page = $pref['largeur_page'];
            $hauteur_page = $pref['hauteur_page'];
            $marge_haut_etiquette = $pref['marge_haut_etiquette'];
            $marge_gauche_etiquette = $pref['marge_gauche_etiquette'];
            $marge_droite_etiquette = $pref['marge_droite_etiquette'];
            $marge_haut_page = $pref['marge_haut_page'];
            $marge_bas_page = $pref['marge_bas_page'];
            $marge_gauche_page = $pref['marge_gauche_page'];
            $marge_droite_page = $pref['marge_droite_page'];
            $espace_etiquettesh = $pref['espace_etiquettesh'];
            $espace_etiquettesl = $pref['espace_etiquettesl'];
            $indice = 0;

            if (intval($depart) > 0) {
                $indice = intval($depart) - 1;
            }

            // Calcul des dimensions des étiquettes
            $largeur_etiquette = ($largeur_page - $marge_gauche_page - $marge_droite_page - (($nb_colonne - 1) * $espace_etiquettesl)) / $nb_colonne;
            $hauteur_etiquette = ($hauteur_page - $marge_haut_page - $marge_bas_page - (($nb_ligne - 1) * $espace_etiquettesh)) / $nb_ligne;


            $html = '';
            $css = '';
            $indice_page = 0;
            $k = 0;
            foreach ($tab_products as $product) {

                $indice_colonne = $indice % $nb_colonne;
                $indice_ligne = floor($indice / $nb_colonne);


                $coeff_barcode = match ($nb_colonne) {
                    2 => 1.6,
                    3 => 1.5,
                    default => 1,
                };

                if (strlen((string) $product->getBarcode()) == 13) {
                    try {
                        $barcode = $generatorHTML->getBarcode($product->getBarcode(), $generatorHTML::TYPE_EAN_13, $coeff_barcode);
                    } catch (\Exception) {
                        $barcode = 'Erreur Code';
                    }
                    $barcode .= $product->getBarcode();

                } else
                    $barcode = $product->getBarcode();
                $html_cellule = $twig_page->render('label_' . $modele . '.twig.html', ['barcode' => $barcode, 'product' => $product, 'css' => $css]);
                $positionx = (($largeur_etiquette + $espace_etiquettesl) * $indice_colonne);
                $positiony = (($hauteur_etiquette + $espace_etiquettesh) * $indice_ligne);

                $html .= '<div class="cadre" style="position:absolute;
                    height:' . ($hauteur_etiquette - $marge_haut_etiquette - $marge_haut_etiquette-2) . 'mm;
                    width:' . ($largeur_etiquette - $marge_gauche_etiquette - $marge_droite_etiquette-1) . 'mm;
                    left:' . $positionx . 'mm;
                    top:' . $positiony . 'mm;
                    padding-top:' . $marge_haut_etiquette . 'mm;
                    padding-left:' . $marge_gauche_etiquette . 'mm;
                    padding-right:' . $marge_droite_etiquette . 'mm;
                    padding-bottom:' . $marge_haut_etiquette . 'mm">
                    ' . $html_cellule . '
                    </div>';
                $indice++;


                if ($indice >= ($nb_colonne * $nb_ligne) || (($k + 1) == count($tab_products))) {
                    $page_css = ($indice_page > 0) ? 'nouvelle_page' : '';
                    $pages[] = $twig_page->render('document.html.twig', ['content' => $html, 'page_css' => $page_css]);
                    $indice = 0;
                    $indice_page++;
                    $html = '';
                }
                $k++;
            }



            $rep_print = $this->sac->get('dir.root') . 'var/print';
            if (!file_exists($rep_print)){
                mkdir($rep_print);
            }


            $css = 'body{font-size:10px;}';
            $css = $twig_page->render('document_style.css.twig', ['css' => $css]);

            $html='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $css . '</head><body>';
            $nb_page = count($pages);
            foreach ($pages as $num_page=>$page) {
                $derniere_page = ($nb_page-1===$num_page);
                $html .=  $page . ($derniere_page?'':'<p style="page-break-after: always;"/><br/>');
            }
            $html .='</body></html>';
            $nom_fichier_pdf = $this->sac->get('dir.root') . 'var/print/label_' . date('Y-m-d-h-i-s') . '.pdf';


            if ($format==='pdf'||$format ==='json') {
                $options = [
                    'disable-smart-shrinking' => true,
                    'margin-bottom' => $marge_bas_page,
                    'margin-left' => $marge_gauche_page,
                    'margin-right' => $marge_droite_page,
                    'margin-top' => $marge_haut_page,
                ];
                $knpSnappyPdf->generateFromHtml(
                    $html,
                    $nom_fichier_pdf,
                    $options
                );
                if ($format==='pdf') {
                    return $this->file($nom_fichier_pdf);
                }
                else{
                    $path_parts = pathinfo($nom_fichier_pdf);
                    return [
                        'pdf'=>$this->generateUrl("label_pdf",['fichier'=>$path_parts['basename']])
                    ];
                }

            } else {

                return $html;
            }


        }

        return $erreurs;

    }

}



