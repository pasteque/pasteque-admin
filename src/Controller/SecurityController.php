<?php

namespace App\Controller;

use App\Entity\Individu;
use App\Entity\User;
use App\Form\MotDePasseType;
use App\Service\ClientAPI;
use Declic3000\Pelican\Service\Controller;
use Declic3000\Pelican\Service\Facteur;;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class SecurityController extends Controller
{
    #[Route(path: '/login', name: 'login')]
    public function login(FormFactoryInterface $formbuilder, AuthenticationUtils $authenticationUtils, Environment $twig,ClientAPI $clientAPI)
    {

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $data=['username'=>$lastUsername];
        $form = $formbuilder->createNamedBuilder('login', FormType::class, $data)
            ->add(
                'username',
                TextType::class,
                ['label' => 'Login']
            )
            ->add('password', PasswordType::class, ['label' => 'mot_de_passe'])
            ->add('_target_path', HiddenType::class)
            ->getForm();

        return new Response($twig->render(
            'security/login.html.twig',
            ['last_username' => $lastUsername,
                'form' => $form->createView(),
                'error' => $error
            ]
        ));
    }


    #[Route(path: '/logout', name: 'logout')]
    public function logout(RequestStack $requestStack)
    {
        $this->container->get('security.token_storage')->setToken(null);

        if ($requestStack->getCurrentRequest()->hasSession()){
            $requestStack->getSession()->clear();
        }
        return $this->redirection('login');
    }


    #[Route(path: '/init_password', name: 'init_password')]
    function init_password(FormFactoryInterface $formBuilder,Request $requete, Sac $sac, TranslatorInterface $translator, UrlGeneratorInterface $url, Environment $twig, EntityManagerInterface $em)
    {

        $session = $requete->getSession();
        $error = null;
        $tokenExpired = false;
        $email = $requete->get('email');
        $token = $requete->get('token');
        $individu = $em->getRepository(User::class)->findOneBy(['email' => $email,'token'=>$token,'active' => 2]);
        if (!$individu) {
            $tokenExpired = true;
        } else if ($individu->isPasswordResetRequestExpired(3600)) {
            $tokenExpired = true;
        }
        if ($tokenExpired) {
            $session->getFlashBag()->set('info', 'Votre mot de passe a été réinitialisé et vous êtes maintenant connecté.');
            return new RedirectResponse($url->generate('login'));
        }
        $error = '';
        $data = [];
        $formbuilder = $formBuilder->createNamedBuilder('individu_form_mdp',  MotDePasseType::class , $data);
        $form = $formbuilder->add('submit', SubmitType::class,
            ['label' => 'Changer de mot de passe', 'attr' => ['class' => 'btn-primary']])
            ->getForm();

        $form->handleRequest($requete);
        if ($form->isSubmitted()) {
            $data = $form->getData();

            if ($form->isValid()) {

                $individu->setPass($data['password']);
                $individu->setToken(null);
                $em->persist($individu);
                $em->flush();
                $session->getFlashBag()->set('success', 'Votre mot de passe a bien été changé, vous pouvez vous connecter.');
                return new RedirectResponse($url->generate('login'));

            } else {
                $form->addError(new FormError($translator->trans('erreur_saisie_formulaire')));
            }
        }

        return new Response($twig->render('security/init_password.html.twig', ['user' => $individu, 'form' => $form->createView(), 'error' => $error]));

    }

    #[Route(path: '/motdepasseperdu', name: 'motdepasseperdu')]
    function motdepasseperdu(Request $requete, Sac $sac, TranslatorInterface $translator, UrlGeneratorInterface $url, Environment $twig, EntityManagerInterface $em, Facteur $facteur)
    {
        $error = null;
        $session = $requete->getSession();
        if ($requete->isMethod('POST')) {
            $email = $requete->get('email');
            $user = $em->getRepository(Individu::class)->findOneBy(['email' => $email, 'active' => 2]);
            if ($user) {
                $user->setTokenTime(time());
                if (!$user->getToken()) {
                    $user->setToken(creer_uniqid());
                }
                $em->persist($user);
                $em->flush();
                $args_twig = [
                    'url_reset' => $url->generate('init_password', ['email' => $email, 'token' => $user->getToken()], UrlGeneratorInterface::ABSOLUTE_URL)
                ];

                $facteur->courriel_twig($email, 'login_motdepasseperdu', $args_twig);
                $session->getFlashBag()->add('info', $translator->trans('Instructions_pour_réinitialiser_son_mot_de_passe_message'));
                $session->set('_security.last_username', $email);
                return new RedirectResponse($url->generate('login'));
            }
            $error = 'Aucun utilisateur ne corresponds à cette adresse';

        } else {
            $email = $requete->get('email') ?: ($requete->get('email') ?: $session->get('_security.last_username'));
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) $email = '';
        }

        return new Response($twig->render('security/motdepasseperdu.html.twig', ['email' => $email, 'fromAddress' => $sac->conf('email.from'), 'error' => $error]));
    }


}
