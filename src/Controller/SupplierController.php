<?php

namespace App\Controller;

use App\Action\ProductAction;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductSupplier;
use App\Entity\Supplier;
use App\Entity\Tax;
use App\EntityExtension\SupplierExt;
use App\Form\DesactivateProductsType;
use App\Form\MercurialeEtape1Type;
use App\Form\MercurialeEtape2Type;
use App\Form\TauxDeMargeType;
use App\Service\ClientAPI;
use App\Service\UploaderImage;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/supplier')]
class SupplierController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'supplier_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/product/mercurial', name: 'supplier_mercurial', methods: 'GET|POST')]
    public function supplier_mercurial(ClientAPI $clientAPI,ProductAction $pa)
    {
        $etape=$this->requete->get('etape',0);
        $data = ['supplier_id' => 15];

        $form_m1 = $this->generateForm('mercuriale', MercurialeEtape1Type::class, $data, [], true,['etape'=>1]);
        if ($etape>=1){
            $form_m1->handleRequest($this->requete->getRequest());
            $data2 = [];
            $options2=[];
            if ($form_m1->isSubmitted()) {
                if ($form_m1->isValid()) {
                    $etape=2;
                    $data2 = $form_m1->getData();
                    $json = json_decode((string) $data2['json'],true);
                    $supplier_id=$data2['supplier_id'];
                    $category_id=$data2['category_id'];
                    $options=$data2['options'];
                }
            } elseif ($etape==2){
                $json = json_decode( $_POST['mercuriale2']['json']??$_POST['mercuriale']['json'],true);
                $options = json_decode( $_POST['mercuriale2']['options']??$_POST['mercuriale']['options'],true);
                $supplier_id = $_POST['mercuriale2']['supplier_id'];
                $category_id = $_POST['mercuriale2']['category_id'];
            }

            if (!empty($json)) {
                $tab_ref = [];
                $tab_barcode = [];
                foreach ($json as $i => $val) {
                        if (isset($val['price']) && !empty($val['price'])) {
                            $json[$i]['barcode'] = preg_replace("/\s+/ui", '', (string) $val['barcode']);
                            $json[$i]['price'] = floatval(str_replace(',', '.', $val['price']));
                            $json[$i]['tva'] = floatval(str_replace(',', '.', $val['tva'] ?? ''));
                            if (isset($json[$i]['contenance'])) {
                                [$scale_type, $scale_value] = $this->extraireContenance($json[$i]['contenance']);
                                $json[$i]['scale_type'] = $scale_type;
                                $json[$i]['scale_value'] = (float)$scale_value;
                            }
                            $tab_barcode[] = $json[$i]['barcode'];
                            if (isset($json[$i]['ref'])) {
                                $tab_ref[] = $json[$i]['ref'];
                            }
                        }

                }

                $tab_barcode = array_unique($tab_barcode);
                $changements=[];
                $products = $this->em->getRepository(Product::class)->findBy(['barcode' => $tab_barcode]);
                $tab_produits = [];
                foreach ($products as $pr) {
                    $tab_produits[$pr->getBarcode()] = $pr;
                }

                $productsup = $this->em->getRepository(ProductSupplier::class)->findBy(['supplier' => $supplier_id, 'reference' => $tab_ref]);
                $tab_produitsup = [];
                foreach ($productsup as $prs) {
                    $tab_produitsup[$prs->getReference()] = $prs;
                }
                $produit_a_creer=false;
                foreach ($json as $i => $val) {
                    // La reference fournisseur existe
                    if (isset($val['ref']) && !empty($val['ref']) && isset($tab_produitsup[$val['ref']])) {
                        $prsup = $tab_produitsup[$val['ref']];
                        $pr = $prsup->getProduct();
                        if ($prsup->getPriceBuy() != $val['price']  && in_array('options_modification_reference',$options)) {
                            $label = 'Modification d\'un referencement produit-fournisseur pour le produit ' . $pr->getId() . '-' . $pr->getLabel() . ' ' . $pr->getPriceBuy() . '---->' . $val['price'];
                            $changements['produitsup_modif_' . $i]=['ligne'=>$i,'traitement'=>'produitsup_modif','label'=>$label,'product_id'=>$pr->getId(),'class'=>"produitsup_modif"];
                        }
                    } else {
                        // Le produit existe mais pas la reference
                        if (isset($val['barcode']) && !empty($val['barcode']) && isset($tab_produits[$val['barcode']])) {
                            $pr = $tab_produits[$val['barcode']];
                            if (isset($val['ref']) && !empty($val['ref']) && in_array('options_ajout_reference',$options) ) {
                                $label = 'Création d\'un referencement produit-fournisseur pour le produit ' . $pr->getId() . '-' . $pr->getLabel();
                                $changements['produitsup_creation_' . $i]=['ligne'=>$i,'traitement'=>'produitsup_creation','label'=>$label,'product_id'=>$pr->getId(),'class'=>"produitsup_creation"];
                            }
                        }
                        else{
                            $produit_a_creer=true;
                            if (in_array('options_ajout',$options) && isset($val['label'])){
                                $label = 'Création du produit ' . $val['barcode'] . '-' . $val['label'].' et sa ref fournisseur';
                                $changements['produit_creation_'.$i]=['ligne'=>$i,'traitement'=>'produit_creation','label'=>$label,'class'=>"creation"];
                            }
                        }
                    }
                    if (!$produit_a_creer){

                        if (($pr->getPriceBuy() != $val['price']) && in_array('options_modification_prix',$options)) {
                            $label = 'Changement du prix du produit ' . $pr->getId() . '-' . $pr->getLabel() . ' ' . $pr->getPriceBuy() . '€ ----> ' . $val['price'].'€';
                            $changements['produit_prix_' . $i]=['ligne'=>$i,'traitement'=>'produit_prix','label'=>$label,'product_id'=>$pr->getId(),'class'=>"modif_prix"];
                        }
                        if (isset($val['label']) && ($pr->getLabel() != $val['label']) && in_array('options_modification_nom',$options)) {
                            $label = 'Changement du nom du produit ' . $pr->getId() . '-' . $pr->getLabel() .  '---->' . $val['label'];
                            $changements['produit_nom_' . $i]=['ligne'=>$i,'traitement'=>'produit_nom','label'=>$label,'product_id'=>$pr->getId(),'class'=>"modif_label"];
                        }
                        if (in_array('options_modification_attr',$options)) {
                            if (isset($val['scale_type']) && ($pr->getScaleType() != $val['scale_type']) || isset($val['scale_value']) && ($pr->getScaleValue() != $val['scale_value'])) {
                                $label = 'Changement des propriété du produit ' . $pr->getId() . '-' . $pr->getLabel() .' '. $pr->getScaleValue().measureUnit($pr->getScaleType()). '---->' . $val['contenance'];
                                $changements['produit_attr_' . $i]=['ligne'=>$i,'traitement'=>'produit_attr','label'=>$label,'product_id'=>$pr->getId(),'class'=>"modif_attr"];
                            }
                        }
                    }
                }
                $options2['changements']= array_flip(table_simplifier($changements,'label'));
                $options2['changements_class']= table_colonne_cle_valeur($changements,'label','class');
                $tab_tax_0= $this->em->getRepository(Tax::class)->findAll();
                $tab_tax=[];
                foreach ($tab_tax_0 as $t){$tab_tax[($t->getRate()*100).'']=$t;}
                $tab_categorie_0= $this->em->getRepository(Category::class)->findAll();
                $tab_categorie=[];
                foreach ($tab_categorie_0 as $t){$tab_categorie[$t->getId().'']=$t;}
                $db = $this->em->getConnection();
                $code_new = $db->fetchFirstColumn('select MAX(cast(barcode as UNSIGNED)) from products where barcode<9999',[])[0];
                $data2['options']=json_encode($options);
                $form_m2 = $this->generateForm('mercuriale2', MercurialeEtape2Type::class, $data2, $options2, true, ['etape' => 2]);
                $form_m2->handleRequest($this->requete->getRequest());
                $tab_product_creation=[];
                $tab_product_erreur=[];
                $tab_product_modif=[];
                if ($form_m2->isSubmitted()) {

                    if ($form_m2->isValid()) {
                        $etape = 3;
                        $choix = $form_m2->get('choix')->getData();
                        $supplier_id = $form_m2->get('supplier_id')->getData();
                        foreach($choix as $code){
                            $c=$changements[$code];
                            $valeurs = $json[$c['ligne']];
                            $supplier = $this->em->getRepository(Supplier::class)->find($supplier_id);
                            if (isset($c['product_id'])){
                                $product = $this->em->getRepository(Product::class)->find($c['product_id']);
                            }

                            switch ($c['traitement']){
                                case 'produit_creation':
                                    $code = $valeurs['barcode'];
                                    if (empty($code)){
                                        $code_new++;
                                        $code = $code_new;
                                    }
                                    if ($valeurs['price']==0){
                                        $tab_product_erreur[]='Ligne '.$c['ligne'].' - '.$valeurs['label'];
                                        break;
                                    }

                                    if (isset($valeurs['label'])){


                                    $product = new Product();
                                    $product->setLabel($valeurs['label']);

                                    $tva = $tab_tax[$valeurs['tva'].''];
                                    $product->setBarcode($code);
                                    $product->setReference($code);
                                    $product->setTax($tva);
                                    $product->setCategory($tab_categorie[$category_id]);
                                    $product->setPriceBuy(1);
                                    $product->setPriceSell(1);

                                    if (isset($valeurs['scale_type'])){
                                    $product->setScaleType($valeurs['scale_type']);
                                    }
                                    if (isset($valeurs['scale_value'])){
                                        $product->setScaleValue($valeurs['scale_value']);
                                    }

                                    $product->setPriceSell(1);
                                    [$ok, $id] = $pa->creation_api($product);
                                    if ($ok){
                                        $product->setId($id);
                                        $this->em->persist($product);
                                        $this->em->flush();
                                        $pa->changePrix($product,((float)$valeurs['price']),20);
                                    }
                                    $pa->labelToPrint($product, true);
                                    $psupplier = new ProductSupplier();
                                    $psupplier->setProduct($product);
                                    $psupplier->setSupplier($supplier);
                                    $psupplier->setReference($valeurs['ref']);
                                    $psupplier->setColisage($valeurs['colisage']);
                                    $psupplier->setPriceBuy(floatval($valeurs['price']));
                                    $this->em->persist($psupplier);
                                    $tab_product_creation[$product->getId()]=$product->getLabel();
                                    }
                                    break;
                                case 'produit_prix':
                                    $pa->changePrix($product,$valeurs['price']);
                                    $tab_product_modif[$product->getId()]=$product->getLabel();
                                    break;
                                case 'produit_nom':
                                    $product->setLabel($valeurs['label']);
                                    $this->em->persist($product);
                                    $tab_product_modif[$product->getId()]=$product->getLabel();
                                    break;
                                case 'produit_attr':
                                    if (isset($valeurs['scale_type'])) {
                                        $product->setScaleType($valeurs['scale_type']);
                                    }
                                    if ($valeurs['scale_value']) {
                                        $product->setScaleValue($valeurs['scale_value']);
                                    }
                                    $this->em->persist($product);
                                    $tab_product_modif[$product->getId()]=$product->getLabel();
                                    break;
                                case 'produitsup_creation':
                                    $psupplier = new ProductSupplier();
                                    $psupplier->setProduct($product);
                                    $psupplier->setSupplier($supplier);
                                    $psupplier->setReference($valeurs['ref']);
                                    $psupplier->setColisage($valeurs['colisage']);
                                    $psupplier->setPriceBuy($valeurs['price']);
                                    $this->em->persist($psupplier);
                                    $pa->changePrix($product,$valeurs['price']);
                                    $tab_product_modif[$product->getId()]=$product->getLabel();
                                    break;
                                case 'produitsup_modif':
                                    $prs = $tab_produitsup[$valeurs['ref']];
                                    $prs->setColisage($valeurs['colisage']);
                                    $prs->setPriceBuy($valeurs['price']);
                                    $this->em->persist($prs);
                                    $pa->changePrix($product,$valeurs['price']);
                                    $tab_product_modif[$product->getId()]=$product->getLabel();
                                    break;
                                default:
                                    break;
                            }

                        }
                        $this->em->flush();

                    }
                }
            }
        }

        $tab_profile_fichier_csv = [
            'Mercuriale Azade CEB' => '{"barcode":0,"ref":5,"label":6,"price":10,"colisage":7,"contenance":6,"unite":6,"tva":12}',
            'Mercuriale Spennink' => '{"barcode":15,"ref":4,"label":5,"price":12,"colisage":8,"contenance":7,"unite":6,"tva":13}',
            '2 colonnes Code Prix' => '{"barcode":0,"ref":"","label":"","price":1,"colisage":"","contenance":"","unite":"","tva":""}'
            ];


        $args_twig = [
            'etape' => $etape,
            'form_m1' => $form_m1->createView(),
            'tab_profile_fichier_csv'=> $tab_profile_fichier_csv,
            'champs'=> [
                'barcode'=>'Code barre',
                'ref'=>'Reférence fournisseur',
                'label'=>'Label',
                'price'=>'Prix HT',
                'colisage'=>'Colisage',
                'contenance'=>'Contenance',
                'unite'=>'Unité de mesure',
                'tva'=>'Tva'
            ]
        ];
        if ($etape==2){
            $args_twig['form_m2']= $form_m2->createView();
        }
        if ($etape==3){
            $args_twig['tab_product_creation']= $tab_product_creation;
            $args_twig['tab_product_modif']= $tab_product_modif;
        }
        return $this->render('supplier/mercurial.html.twig', $args_twig);
    }


    
    #[Route(path: '/test_extraction', name: 'test_extraction', methods: 'GET|POST')]
    public function test_extraction(): never
    {
        dump($this->extraireContenance('45cl'));
        dump($this->extraireContenance('0.45l'));
        dump($this->extraireContenance('1.5kg'));
        dump($this->extraireContenance('140g'));
        dump($this->extraireContenance('1400 g'));
        dump($this->extraireContenance('1400 gr'));
exit();
    }


function extraireContenance($contenance){

        $contenance = trim(str_replace(',', '.', $contenance));
        $nombre='[0-9]+(\\.[0-9]+)?';
        $litre = "l|L|litre";
        $millilitre = "ml|millilitre|ML";
        $centilitre = "cl|centilitre|CL";
        $gramme = "g|gr|gramme";
        $kilogramme = "kg|kilo|kilogramme";
        $matches=[];
        $scale_value = null;
        $scale_unit = null;
        if (preg_match('`('.$nombre.')(\s)?('.$gramme.'|'.$kilogramme.'){1}`',$contenance,$matches)){
            $scale_unit=1;
            $scale_value=floatval($matches[1]);
            if(preg_match('`('.$nombre.')(\s)?('.$kilogramme.')`',$contenance,$matches)){
                $scale_value=$scale_value*1000;
            }
        }
        elseif(preg_match('`('.$nombre.')(\s)?('.$litre.'|'.$millilitre.'|'.$centilitre.')`',$contenance,$matches)){
            $scale_unit=2;
            $scale_value=floatval($matches[1]);
            if(preg_match('`('.$nombre.')(\s)?('.$millilitre.')`',$contenance,$matches)){
                $scale_value=$scale_value / 1000;
            }elseif(preg_match('`('.$nombre.')(\s)?('.$centilitre.')`',$contenance,$matches)){
                $scale_value=$scale_value / 100;
            }

        }
        return [$scale_unit,$scale_value];

    }


    
    #[Route(path: '/product/new', name: 'productsupplier_new', methods: 'GET|POST')]
    public function productsupplier_new()
    {
        $product_id = $this->requete->get('product_id');
        $args_rep=['product_id'=>$product_id];

        return $this->new_defaut($args_rep,'productsupplier');
    }

    #[Route(path: '/product/{id}/edit', name: 'productsupplier_edit', methods: 'GET|POST')]
    public function productsupplier_edit(ProductSupplier $ob)
    {
        return $this->edit_defaut($ob,[],'productsupplier');
    }

    #[Route(path: '/productsupplier/{id}', name: 'productsupplier_delete', methods: 'DELETE')]
    public function productsupplier_delete(ProductSupplier $ob)
    {
        return $this->delete_defaut($ob,'productsupplier');

    }

    
    #[Route(path: '/new', name: 'supplier_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'supplier_edit', methods: 'GET|POST')]
    public function edit(Supplier $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'supplier_delete', methods: 'DELETE')]
    public function delete(Supplier $ob)
    {
        return $this->delete_defaut($ob);

    }




    #[Route(path: '/{id}', name: 'supplier_show', methods: 'GET')]
    public function show(Supplier $ob,UploaderImage $uploaderImage)
    {
        $nom_champs = 'supplier_'.$ob->getPrimaryKey().'_logo';
        $ob_ext = new SupplierExt($ob,$this->sac,$this->em);
        $form_image = $this->generateFormUpload($nom_champs);
        $uploaderImage->traitementFormulaireImage($form_image,$ob,$ob_ext,'category');

        $options_table = [
            'options_js'=>[
                'url_label'=>$this->generateUrl('product_label_change',['id'=>0]),
                'url_visible'=>$this->generateUrl('product_visible_change',['id'=>0])
            ],
        ];
        $product_table = $this->createTable('product', $options_table);

        $args_twig = [
            'supplier' => $ob,
            'form_image'=>$form_image->createView(),
            'products' => $product_table->export_twig(true, ['supplier' => $ob->getPrimaryKey()])
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);

    }


}
