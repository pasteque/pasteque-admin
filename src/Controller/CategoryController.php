<?php

namespace App\Controller;

use App\Entity\Category;
use App\EntityExtension\CategoryExt;
use App\EntityExtension\ProductExt;
use App\Service\UploaderImage;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


#[Route(path: '/category')]
class CategoryController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'category_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/arbo', name: 'category_arbo', methods: 'GET')]
    public function category_arbo()
    {

        $tab_hierarchie = $this->sac->tab('category_tree');
        $tab_cat= $this->sac->tab('category');
        $tab_cat = arbre_linearise_niveau($tab_hierarchie,$tab_cat);
        $args_twig = [
            'tab_cat' => $tab_cat
        ];
        return $this->render('category/arbo.html.twig', $args_twig);
    }


    
    #[Route(path: '/new', name: 'category_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'category_edit', methods: 'GET|POST')]
    public function edit(Category $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'category_delete', methods: 'DELETE')]
    public function delete(Category $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'category_show', methods: 'GET|POST')]
    public function show(Category $ob,UploaderImage $uploaderImage)
    {

        $nom_champs = 'category_'.$ob->getPrimaryKey().'_logo';
        $ob_ext = new CategoryExt($ob,$this->sac,$this->em);
        $form_image = $this->generateFormUpload($nom_champs);
        $uploaderImage->traitementFormulaireImage($form_image,$ob,$ob_ext,'category');

        $options_table = [
            'colonnes_exclues' => ['category.label'],
            'options_js'=>['url_label'=>$this->generateUrl('product_label_change',['id'=>0])]
        ];
        $product_table = $this->createTable('product', $options_table);

        $args_twig = [
            'category' => $ob,
            'form_image'=>$form_image->createView(),
            'products' => $product_table->export_twig(true, ['category' => $ob->getPrimaryKey()])
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);

    }
}
