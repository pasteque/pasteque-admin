<?php

namespace App\Controller;

use App\Action\ServicerenduAction;

use App\Service\ClientAPI;
use App\Service\Synchro;
use Declic3000\Pelican\Service\Controller;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;




class AccueilController extends Controller
{
    #[Route(path: '/', name: 'accueil')]
    public function index()
    {
        $args_twig=[];
        return $this->render('accueil.html.twig',$args_twig);
    }

    #[Route(path: '/sync/', name: 'sync')]
    public function sync(RequestStack $requestStack)
    {

        $clientApi = new ClientAPI($requestStack,$this->sac);
        $synchro = new Synchro($this->em,$this->sac,$clientApi);

        $synchro->synchronisation();
        $this->sac->clear();
        $this->sac->initSac(true);

        $synchro->synchronisation_session();

        $this->sac->clear();
        $this->sac->initSac(true);

        $synchro->synchro_ticket();
        $this->addFlash('info','Synchro OK');
        return $this->redirectToRoute('accueil');
    }

}
