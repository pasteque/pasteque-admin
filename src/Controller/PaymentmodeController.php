<?php

namespace App\Controller;

use App\Entity\PaymentMode;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/paymentmode')]
class PaymentmodeController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'paymentmode_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/new', name: 'paymentmode_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'paymentmode_edit', methods: 'GET|POST')]
    public function edit(PaymentMode $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'paymentmode_delete', methods: 'DELETE')]
    public function delete(PaymentMode $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'paymentmode_show', methods: 'GET')]
    public function show(PaymentMode $ob)
    {
        return $this->show_defaut($ob);

    }
}
