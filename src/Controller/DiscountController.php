<?php

namespace App\Controller;

use App\Entity\Discount;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/discount')]
class DiscountController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'discount_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/new', name: 'discount_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'discount_edit', methods: 'GET|POST')]
    public function edit(Discount $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'discount_delete', methods: 'DELETE')]
    public function delete(Discount $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'discount_show', methods: 'GET')]
    public function show(Discount $ob)
    {
        return $this->show_defaut($ob);

    }
}
