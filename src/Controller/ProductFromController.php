<?php

namespace App\Controller;

use App\Action\ProductAction;
use App\Entity\Category;
use App\Entity\Image;
use App\Entity\Product;
use App\Entity\ProductExtra;
use App\EntityExtension\ProductExt;
use App\EntityExtension\ProduitExt;
use App\Form\DesactivateProductsType;
use App\Form\ProductfromType;
use App\Form\TauxDeMargeType;
use App\Service\ClientAPI;
use App\Service\Synchro;
use App\Service\UploaderImage;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ControllerObjet;
use Declic3000\Pelican\Service\Selecteur;
use Knp\Snappy\Pdf;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Symfony\Component\Routing\Annotation\Route;


#[Route(path: '/productfrom')]
class ProductFromController extends ControllerObjet
{


    #[Route(path: '/listing', name: 'productfrom_listing')]
    public function product_listing(Selecteur $selecteur, Pdf $knpSnappyPdf)
    {
        $format = $this->requete->get('format', 'html');
        $inactif = $this->requete->get('inactif');
        $vrac = $this->requete->get('vrac',true);
        $petit_code = $this->requete->get('petit_code',true)?9999:0;


        $tab_produit_o = $this->em->getRepository(Product::class)->findByBarcodeAndVisible($petit_code, $inactif,$vrac);
        $tab_produit = [];
        $twig = 'listing';


        foreach ($tab_produit_o as $prod) {


            $coeff_barcode = 1;

            $generatorHTML = new BarcodeGeneratorHTML();
            try {
                $barcode_image = $generatorHTML->getBarcode($prod->getBarcode(), $generatorHTML::TYPE_CODE_128, $coeff_barcode);
            } catch (\Exception) {
                $barcode_image = 'Erreur Code';
            }
            $id_cat = $prod->getCategory()->getId();
            $temp = [
                'id' => $prod->getId(),
                'barcode_image' => $barcode_image,
                'barcode' => $prod->getBarcode(),
                'label' => $prod->getLabel(),
                'category' => $id_cat,
                'prix_achat' => $prod->getPriceBuy(),
                'prix' => $prod->getTaxedPrice(),
                'contenance' => $prod->getScaleValue(),
                'contenance_unite' => $prod->getScaleType(),
            ];
            $prod_extra = $prod->getProductExtra();
            if ($prod_extra){
                $prod_from = $prod_extra->getFromProduct();
                if ($prod_from){
                    $temp['product_from']=[
                        'id' => $prod_from->getId(),
                        'label' => $prod_from->getLabel(),
                        'barcode' => $prod_from->getBarcode(),
                        'prix_achat' =>  $prod_from->getPriceBuy(),
                        'contenance' => $prod_from->getScaleValue(),
                        'contenance_unite' => $prod_from->getScaleType()
                    ];
                }
            }
            $tab_produit[] = $temp;
            $tab_nb_by_cat[$id_cat] = ($tab_nb_by_cat[$id_cat] ?? 0) + 1;
        }


        $tab_hierarchie = $this->sac->tab('category_tree');
        $tab_cat = $this->sac->tab('category');
        $tab_cat = arbre_linearise_niveau($tab_hierarchie, $tab_cat);

        $tab_temp = [];
        foreach ($tab_cat as $id => $c) {
            $niveau = $c['niveau'];
            $nb = $tab_nb_by_cat[$id] ?? 0;
            $tab_cat[$id]['nb'] = $nb;
            $tab_temp = array_slice($tab_temp, 0, $niveau, true);
            foreach ($tab_temp as $c_parent) {
                $tab_cat[$c_parent]['nb'] += $nb;
            }
            $tab_temp[$c['niveau']] = $id;
        }


        $args_twig = [
            'tab_cat' => $tab_cat,
            'tab_produit' => table_regrouper($tab_produit, 'category'),
            'date_du_jour' => new \DateTime(),
            'pdf' => ($format === 'pdf'),
            'args' => [
                'inactif' => $inactif,
                'petit_code' => $petit_code,
                'vrac'=> $vrac
            ]
        ];

        return $this->render('productfrom/' . $twig . '.html.twig', $args_twig);

    }

    #[Route(path: '/{id}/correction_prix', name: 'product_correction_prix')]
    public function correction_prix(Product $product,ProductAction $pa)
    {
        $productFrom = $product->getProductExtra()->getFromProduct();
        $new_price = ($productFrom->getPriceBuy()*$product->getScaleValue())/$productFrom->getScaleValue();
        $pa->changePrix($product,$new_price);
        $this->em->persist($product);
        $this->em->flush();
        $args_rep=['ok'=>true,'html'=>'<td class="td10 etat" ><div class="text-success">Prix actualisé</div></td>'];
        return $this->json( $args_rep);
    }






    #[Route(path: '/correction_prix_de_masse', name: 'product_correction_prix_de_masse')]
    public function correction_prix_de_masse(ProductAction $pa)
    {
        $format = $this->requete->get('format', 'html');
        $inactif = $this->requete->get('inactif');
        $petit_code = $this->requete->get('petit_code',true)?9999:0;


        $tab_produit_o = $this->em->getRepository(Product::class)->findByBarcodeAndVisible($petit_code, $inactif,true);
        $tab_produit = [];
        $twig = 'listing';


        foreach ($tab_produit_o as $product) {


            if ($pe = $product->getProductExtra()){
                if ($productFrom = $pe->getFromProduct()) {
                    $new_price = ($productFrom->getPriceBuy()*$product->getScaleValue())/$productFrom->getScaleValue();
                    if (round($new_price,2) != round($product->getPriceBuy(),2)){
                        $pa->changePrix($product,round($new_price,2));
                        $this->em->persist($product);
                        $this->em->flush();
                    }

                }
            }
        }

        return $this->redirection('productfrom_listing', []);

    }



}