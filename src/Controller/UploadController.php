<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\DocumentLien;
use Declic3000\Pelican\Service\Controller;

use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Uploader;

use Symfony\Component\Routing\Annotation\Route;


class UploadController extends Controller
{

    #[Route(path: '/upload/{nom}', name: 'upload')]
    function upload(string $nom)
    {


        $output_dir = $this->sac->get('dir.root').'var/upload/';
        if (isset($_FILES['fichiers'])) {
            $ret = [];


            if (!file_exists($output_dir)) {
                if (!mkdir($output_dir) && !is_dir($output_dir)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $output_dir));
                }
            }

            $ret['error'] = $_FILES['fichiers']["error"];
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData()
            if (!is_array($_FILES['fichiers']["name"])) //single file
            {

                $fileName = time() . '_' . $_FILES['fichiers']["name"];
                move_uploaded_file($_FILES['fichiers']["tmp_name"], $output_dir . $fileName);
                $ret['files'][] = $fileName;
            } else  //Multiple files, file[]
            {
                $fileCount = count($_FILES['fichiers']["name"]);
                for ($i = 0; $i < $fileCount; $i++) {
                    $fileName = time() . '_' . $_FILES['fichiers']["name"][$i];
                    move_uploaded_file($_FILES['fichiers']["tmp_name"][$i], $output_dir . $fileName);

                    $ret['files'][] = $fileName;
                }

            }
            $ged = new Ged($this->em,$this->sac);
            $uploader= new Uploader($ged,$this->sac,$this->requete);
            $uploader->liste_ajoute_fichier($nom,$ret['files']);

        }

        return $this->json($ret);
    }



}

