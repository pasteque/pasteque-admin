<?php

namespace App\Controller;

use App\Entity\Product;
use Declic3000\Pelican\Service\ControllerObjet;
use Declic3000\Pelican\Service\Selecteur;
use Knp\Snappy\Pdf;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/commande')]
class CommandeController extends ControllerObjet
{
    #[Route(path: '/', name: 'commande_index')]
    public function index()
    {

        $args_twig = [];
        return $this->render('commande/index.html.twig', $args_twig);
    }

    #[Route(path: '/compilation', name: 'commande_compilation')]
    public function compilation()
    {
        $args_twig = [];
        return $this->render('commande/compilation.html.twig', $args_twig);

    }

    #[Route(path: '/feuille', name: 'commande_feuille')]
    public function feuille(Selecteur $selecteur, Pdf $knpSnappyPdf)
    {
        $format = $this->requete->get('format', 'html');
        $destinataire = $this->requete->get('destinataire', 'caissier');
        $mot = table_filtrer_valeur_premiere($this->tab('mot'), 'nomcourt', 'catalogue');
        $selecteur->setObjet('product');
        $tab_id_produit = $selecteur->getTabId(['mot' => $mot['id_mot']]);

        $tab_produit_o = $this->em->getRepository(Product::class)->findBy(['id' => $tab_id_produit],['label'=>'ASC']);
        $tab_produit = [];
        $twig ='feuille';
        if ($destinataire === 'adh'){
            $twig .='_adh';
        }

        foreach ($tab_produit_o as $prod) {

            $psuppliers = $prod->getProductsuppliers();
            $psupplier = null;
            $supplier = null;
            foreach ($psuppliers as $ps) {
                if ($ps->isFavori()) {
                    $psupplier = $ps;
                    $supplier = $psupplier->getSupplier();
                    break;
                }

            }
            $coeff_barcode = 1;
            $colisage = $psupplier ? $psupplier->getColisage() : '';
            $generatorHTML = new BarcodeGeneratorHTML();
            try {
                $barcode_image = $generatorHTML->getBarcode($colisage.'*'.$prod->getBarcode() , $generatorHTML::TYPE_CODE_128, $coeff_barcode);
            } catch (\Exception) {
                $barcode_imag = 'Erreur Code';
            }
            $id_cat=$prod->getCategory()->getId();
            $tab_produit[] = [
                'id' => $prod->getId(),
                'barcode_image' => $barcode_image,
                'barcode' => $colisage.'*'.$prod->getBarcode(),
                'label' => $prod->getLabel(),
                'category' => $id_cat,
                'prix' => $prod->getTaxedPrice(),
                'prix_total' => $psupplier ? ($prod->getTaxedPrice() * $psupplier->getColisage()) : 'xxxx',
                'contenance' => $prod->getScaleValue(),
                'contenance_unite' => $prod->getScaleType(),
                'colisage' => $psupplier ? $psupplier->getColisage() : 'xxxx',
                'supplier_name' => $supplier ? $supplier->getName() : 'Renseigner un fournisseur pour ce produit'
            ];
            $tab_nb_by_cat[$id_cat] = ($tab_nb_by_cat[$id_cat]??0)+1;
        }


        $tab_hierarchie = $this->sac->tab('category_tree');
        $tab_cat= $this->sac->tab('category');
        $tab_cat = arbre_linearise_niveau($tab_hierarchie,$tab_cat);

        $tab_temp=[];
        foreach( $tab_cat as $id=>$c){
            $niveau = $c['niveau'];
            $nb = $tab_nb_by_cat[$id]??0;
            $tab_cat[$id]['nb']=$nb;
            $tab_temp = array_slice($tab_temp,0,$niveau,true);
            foreach($tab_temp as $c_parent){
                $tab_cat[$c_parent]['nb'] += $nb;
            }
            $tab_temp[$c['niveau']]= $id;
        }



        $args_twig = [
            'tab_cat' => $tab_cat,
            'tab_produit' => table_regrouper($tab_produit,'category'),
            'date_du_jour' => new \DateTime(),
            'pdf' =>($format === 'pdf')
        ];

        if ($args_twig['pdf']) {
            $args_twig['layout'] = '_pdf';
            $outputfile = $this->sac->get('dir.root') . 'var/bulletin_commande.pdf';
            if (file_exists($outputfile)) {
                unlink($outputfile);
            }
            $html = $this->renderView('commande/'.$twig.'.html.twig', $args_twig);
            $knpSnappyPdf->generateFromHtml(
                $html,
                $outputfile
            );
            return $this->file($outputfile);
        } else {

            return $this->render('commande/'.$twig.'.html.twig', $args_twig);
        }
    }

}
