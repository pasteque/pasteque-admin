<?php

namespace App\Controller;

use Declic3000\Pelican\Service\Controller;
use  Declic3000\Pelican\Service\Requete;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/sales')]
class SalesController extends Controller
{
    #[Route(path: '/', name: 'sales')]
    public function index()
    {


        $format = $this->requete->get('format');
        $regroup = $this->requete->get('regroup', "annee");
        $db = $this->em->getConnection();
        $regroup_sql = match ($regroup) {
            'jour' => '%Y-%m-%d',
            'mois' => '%Y-%m',
            'semaine' => '%x-%v',
            default => '%Y',
        };
        $tab_date = $db->fetchAllAssociative("select distinct DATE_FORMAT(date,'".$regroup_sql."') as dateg from stat_sales order by dateg");
        $tab_date = table_simplifier($tab_date, 'dateg');
        $tab = $db->fetchAllAssociative("select p.id as id,barcode,p.label,category_id ,FORMAT(SUM(quantity),2) as quantity,DATE_FORMAT(date,'".$regroup_sql."') as dateg from stat_sales s LEFT OUTER JOIN products p on s.product_id = p.id  group by s.product_id,dateg order by s.product_id,dateg");
        $tab_cat=table_simplifier($this->sac->tab('category'),'label');

        foreach ($tab as $t) {
            if (!isset($tabr[$t['id']])) {
                $tabr[$t['id']] = [
                    'label' => $t['label'],
                    'barcode' => $t['barcode'],
                    'category' => $tab_cat[$t['category_id']],
                    'stat' => []];
            }
            $tabr[$t['id']]['stat'][$t['dateg']] = $t['quantity'];
        }


        $tab_colonne = ['id', 'label','barcode','category'];
        foreach ($tab_date as $i => $d) {
            $tab_colonne[] = $d;
        }
        $tab_result = [];
        foreach ($tabr as $id_prd => $prd) {
            $temp = [$id_prd, $prd['label'],$prd['barcode'],$prd['category']];
            foreach ($tab_date as $i => $d) {
                $temp[] = $prd['stat'][$d] ?? 0;
            }
            $tab_result[] = $temp;
        }
        if ($format === 'csv') {
            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=sales.csv");
            echo(array2csv($tab_result));
            exit();
        }
        array_unshift($tab_result, $tab_colonne);
        return $this->render('sales/index.html.twig', [
            'regroup' => $regroup,
            'tab_stat' => $tab_result,
        ]);
    }


}
