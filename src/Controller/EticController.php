<?php

namespace App\Controller;

use App\Entity\Etape;
use App\Entity\Motgroupe;
use App\Form\ProductscaleType;
use App\Service\ClientAPI;
use App\Service\Synchro;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ControllerObjet;
use Doctrine\DBAL\Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/etic')]
class EticController extends ControllerObjet
{
    #[Route(path: '/', name: 'etic')]
    public function index()
    {

        $args_twig = [
            'tab_etape' => $this->em->getRepository(Etape::class)->findAll()
        ];
        return $this->render('etic/index.html.twig', $args_twig);
    }

    #[Route(path: '/etape/new', name: 'etape_new')]
    public function etic_etape_new()
    {
        return $this->new_defaut([],'etape');

    }


    #[Route(path: '/etape/{idEtape}/edit', name: 'etape_edit')]
    public function etic_etape_edit(Etape $ob): \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\JsonResponse|array|string|\Symfony\Component\HttpFoundation\Response
    {
        return $this->edit_defaut($ob,[],'etape');

    }


    #[Route(path: '/{idEtape}', name: 'etape_delete', methods: 'DELETE')]
    public function etape_delete(Etape $ob)
    {
        return $this->delete_defaut($ob);

    }


    /**
     * @throws Exception
     */
    #[Route(path: '/{idEtape}/etape_go', name: 'etic_etape_go')]
    public function etape_edit(Chargeur $chargeur, Synchro $synchro,ClientAPI $clientAPI)
    {
        $args_rep = [];
        $db = $this->em->getConnection();
        $id_etape = $this->sac->get('id');
        $id_location = $this->requete->get('id_location');
        $id_product = $this->requete->get('id_product');
        $where_location = '';

        if (empty($id_product)) {
            if ($id_location == 0) {
                $where_location = ' AND location IS NULL';
            } else {
                $where_location = ' AND location = ' . $id_location;
            }

            $sql = 'SELECT etp.product_id from etapes_products etp LEFT OUTER JOIN products_extra pex on pex.product_id = etp.product_id where  etape_id=' . intval($id_etape) . ' and etat=0 ' . $where_location . ' LIMIT 0,1';
            $id_p = $db->fetchFirstColumn($sql);

        } else {
            $id_p = $id_product;

        }

        if (!$id_p) {
            echo('aucun produit');
            exit();
        }

        $product = $chargeur->charger_objet('product', $id_p);
        if ($product->hasImage()) {

            $nb = $db->fetchFirstColumn('select count(*) from images where modelId=\'' . $id_p . '\' and model=\'product\'');
            if ($nb == 0) {
                //synchro image
                include_once($this->sac->get('root.dir') . '/src/inc/fonctions_sync.php');
                $synchro->recuperation_image('product', $id_p);

            }


        }


        $data = $product->toArray();
        $data['product_id'] = $data['id'];
        $builder = $this->createForm(ProductscaleType::class, $data);

        $tab_param = ['id' => $id_etape, 'id_location' => $id_location, 'action' => 'saisie'];
        if (!empty($id_product)) {
            $tab_param['id_product'] = $id_product;
        }


        $form = $builder->add('submit', SubmitType::class,
            ['label' => 'Save', 'attr' => ['class' => 'btn-primary']])
            ->getForm();


        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted()) {
            $data_form = $form->getData();

            if ($form->isValid()) {

                $product = $chargeur->charger_objet('product', $data_form['product_id']);

                $product->fromArray($data_form);
                $pr = $product->toStruct();
                [$reponse, $statut, $err] = $clientAPI->appelAPI('/api/product', [], $pr, [], 'POST');
                if ($statut == 200) {
                    $this->em->persist($product);
                    $this->em->flush();
                }

                $db=$this->em->getConnection();
                $db->update('etapes_products', ['etat' => '1'], ['product_id' => $product->getId(), 'etape_id' => intval($id_etape)]);
                $args_rep['url_redirect'] = $this->generateUrl('etape', ['action' => 'saisie', 'id' => $id_etape, 'id_location' => $id_location]);
            }


        }

        $args_rep['product'] = $product;
        $args_rep['js_init'] = 'product_form';
        $args_rep['titre_formulaire'] = 'Etic Etape 1 : Le poids au kilo';
        return $this->reponse_formulaire($form, $args_rep, 'etic/product_form_etic_etape.html.twig');
    }


    #[Route(path: '/etape_liste_produit', name: 'etape_liste_produit')]
    public function etape_liste_produit()
    {

        $args_rep = [];
        $db = $this->em->getConnection();
        $id_etape = $this->sac->get('id');
        $id_location = $this->requete->get('id_location');
        $where_location = '';
        if ($id_location == 0) {
            $where_location = ' AND location IS NULL';
        } elseif ($id_location > 0) {
            $where_location = ' AND location = ' . $id_location;
        }
        $sql = 'SELECT etp.product_id,p.label,p.scaleValue,p.scaleType,p.barcode,etp.etat from etapes_products etp LEFT OUTER JOIN products p on p.id=etp.product_id LEFT OUTER JOIN products_extra pex on pex.product_id = etp.product_id where  etape_id=' . intval($id_etape) . ' ' . $where_location;
        $tab_product = $db->fetchAllAssociative($sql);
        $args_twig = [
            'tab_product' => $tab_product,
            'id_etape' => $id_etape,
            'id_location' => $id_location,
        ];
        return $this->render('etic/inclure/product_list.html.twig', $args_twig);

    }

    /**
     * @throws Exception
     */
    #[Route(path: '/etic_etape', name: 'etic_show')]
    public function etic_etape(Chargeur $chargeur)
    {
        $tab_location = getLocation();
        $db = $this->em->getConnection();
        $id = $this->requete->get('id');
        $product = $chargeur->charger_objet('etape', $id);
        foreach ($tab_location as $id_location => &$location) {
            $location = ['nom' => $location];
            $sql = 'SELECT count(etp.product_id)from etapes_products etp LEFT OUTER JOIN products_extra pex on pex.product_id = etp.product_id where  etape_id=' . intval($id) . ' and etat=0 AND location=' . intval($id_location);
            $location['a_faire'] = $db->fetchOne($sql);
            $sql = 'SELECT count(etp.product_id) from etapes_products etp LEFT OUTER JOIN products_extra pex on pex.product_id = etp.product_id where  etape_id=' . intval($id) . ' and etat=1 AND location=' . intval($id_location);
            $location['fait'] = $db->fetchOne($sql);
            if ($location['fait'] + $location['a_faire'] == 0) {
                unset($tab_location[$id_location]);
            }

        }
        $tab_location['0'] = ['nom' => 'Sans emplacement défini'];
        $sql = 'SELECT count(etp.product_id) from etapes_products etp LEFT OUTER JOIN products_extra pex on pex.product_id = etp.product_id where etape_id=' . intval($id) . ' and etat=0 AND location is null';
        $tab_location['0']['a_faire'] = $db->fetchOne($sql);
        $sql = 'SELECT count(etp.product_id) from etapes_products etp LEFT OUTER JOIN products_extra pex on pex.product_id = etp.product_id where etape_id=' . intval($id) . ' and etat=1 AND location is null';
        $tab_location['0']['fait'] = $db->fetchOne($sql);

        $args_twig = [
            'objet_data' => $product,
            'tab_location' => $tab_location
        ];
        return $this->render('etic/etape.html.twig', $args_twig);
    }


}
