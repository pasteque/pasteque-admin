<?php

namespace App\Controller;

use App\Entity\Role;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/role')]
class RoleController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'role_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/new', name: 'role_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'role_edit', methods: 'GET|POST')]
    public function edit(Role $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'role_delete', methods: 'DELETE')]
    public function delete(Role $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'role_show', methods: 'GET')]
    public function show(Role $ob)
    {
        return $this->show_defaut($ob);

    }
}
