<?php

namespace App\Controller;

use App\Form\MotFusionType;
use Symfony\Component\Routing\Annotation\Route;
use Declic3000\Pelican\Service\ControllerObjet;
use App\Entity\Mot;
use App\Entity\Motgroupe;

#[Route(path: 'mot')]
class MotController extends ControllerObjet
{
    #[Route(path: '/motgroupe', name: 'motgroupe')]
    public function motgroupe()
    {

        $db = $this->em->getConnection();
        $tab_count = $db->fetchAllAssociative('select id_mot,count(id_rouleau) as nb from mot_rouleau group by id_mot');
        $tab_count = table_simplifier(table_valeur_cle($tab_count,'id_mot'),'nb');
        $args_twig=[
            'tab_count' => $tab_count,
            'choix'=> table_simplifier(table_filtrer_valeur($this->sac->tab('motgroupe'),'parent_id',0)),
            'tab_mots_orphelins'=> table_simplifier(table_filtrer_valeur($this->sac->tab('mot'),'id_motgroupe',0))
        ];

        return $this->render('motgroupe/motgroupe.html.twig', $args_twig);
    }
    

    
    #[Route(path: '/groupe/', name: 'motgroupe_index', methods: 'GET')]
    public function motgroupe_index()
    {
        return $this->index_defaut("motgroupe");
    }
    
    
    
    #[Route(path: '/groupe/new', name: 'motgroupe_new', methods: 'GET|POST')]
    public function motgroupe_new()
    {
        return $this->new_defaut();
    }
    
    #[Route(path: '/groupe/{idMotgroupe}/edit', name: 'motgroupe_edit', methods: 'GET|POST')]
    public function motgroupe_edit(Motgroupe $ob)
    {
        return $this->edit_defaut($ob);
    }
    
    
    #[Route(path: '/groupe/{idMotgroupe}', name: 'motgroupe_delete', methods: 'DELETE')]
    public function motgroupe_delete(Motgroupe $ob)
    {
        return $this->delete_defaut($ob);
        
    }
    
    #[Route(path: '/groupe/{idMotgroupe}', name: 'motgroupe_show', methods: 'GET')]
    public function motgroupe_show(Motgroupe $ob)
    {
        return $this->show_defaut($ob);
        
    }

    
    
    #[Route(path: '/', name: 'mot_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }

    
    #[Route(path: '/new', name: 'mot_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }


    #[Route(path: '/fusion', name: 'mot_fusion', methods: 'GET|POST')]
    function mot_fusion(){

        $id_mot = $this->requete->get('idMot');
        $em = $this->getDoctrine()->getManager();
        if(!$id_mot){
            return $this->reponse_erreur('mot à fusionner non renseigné ');
        }
        $objet_data = $em->getRepository(Mot::class)->find($id_mot);
        $args_rep = [];
        $data=['nouveau_nom' => $objet_data->getNom()];
        $form = $this->generateForm('objet_mot',MotFusionType::class,$data,[],true,['idMot'=>$id_mot]);

        $form->handleRequest($this->requete);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $data = $form->getData();

                if (!empty($data['nouveau_nom'])){
                    $objet_data->setNom($data['nouveau_nom']);

                }

                $db = $this->em->getConnection();
                foreach($data['id_mot_fusion'] as $id_mot_disparu){
                    $tab_objet=['individu','membre'];
                    foreach($tab_objet as $objet){
                        $descr=$this->sac->descr($objet);
                        $table = 'asso_mots_'.$descr['nom_sql'].'s';
                        $tab_ind_present = $db->fetchAllAssociative('SELECT '.$descr['cle_sql'].' as id FROM '.$table.' WHERE id_mot='.$id_mot);
                        $tab_ind_present = table_simplifier($tab_ind_present,'id');
                        $tab_a_ajouter = $db->fetchAllAssociative('SELECT '.$descr['cle_sql'].' as id FROM '.$table.' WHERE id_mot='.$id_mot_disparu);
                        $tab_a_ajouter = table_simplifier($tab_a_ajouter,'id');
                        $tab_a_ajouter = array_diff($tab_a_ajouter,$tab_ind_present);
                        foreach($tab_a_ajouter as $id){
                            $db->insert($table, [$descr['cle_sql']=>$id ,'id_mot'=>$id_mot]);
                        }
                    }
                    $objet_data_fusion = $em->getRepository(Mot::class)->find($id_mot_disparu);
                    $em->remove($objet_data_fusion);
                    $em->persist($objet_data);
                }
                $em->flush();
                $this->sac->clear();
                $this->sac->initSac(true);
            }
        }
        return $this->reponse_formulaire($form,$args_rep);
    }




    #[Route(path: '/{idMot}/edit', name: 'mot_edit', methods: 'GET|POST')]
    public function edit(Mot $ob)
    {
        return $this->edit_defaut($ob);
    }






    #[Route(path: '/{idMot}', name: 'mot_delete', methods: 'DELETE')]
    public function delete(Mot $ob)
    {
        return $this->delete_defaut($ob);
        
    }





    #[Route(path: '/{idMot}', name: 'mot_show', methods: 'GET')]
    public function show(Mot $ob)
    {
        $options_table=[];

        $obg = $ob->getMotgroupe();
        $tab_objet_en_lien = explode(';',$obg->getObjetsEnLien());
        foreach($tab_objet_en_lien as $objet){
             $temp = $this->createTable($objet,$options_table);
            $datatable[$objet]=$temp->export_twig(false,['mot'=>$ob->getPrimaryKey()]);
        }


        $args_twig = [
            'objet_data' => $ob,
            'datatable' => $datatable,
            'tab_objet_en_lien' => $tab_objet_en_lien,
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);


        
    }
    
    
    
    
}
