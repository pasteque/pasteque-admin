<?php

namespace App\Controller;

use App\Action\ProductAction;
use App\Entity\Category;
use App\Entity\Image;
use App\Entity\Product;
use App\Entity\ProductExtra;
use App\EntityExtension\ProductExt;
use App\EntityExtension\ProduitExt;
use App\Form\DesactivateProductsType;
use App\Form\ProductfromType;
use App\Form\TauxDeMargeType;
use App\Service\ClientAPI;
use App\Service\Synchro;
use App\Service\UploaderImage;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ControllerObjet;
use Declic3000\Pelican\Service\Selecteur;
use Knp\Snappy\Pdf;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Symfony\Component\Routing\Annotation\Route;


#[Route(path: '/product')]
class ProductController extends ControllerObjet
{
    
    #[Route(path: '/', name: 'product_index', methods: 'GET')]
    public function index()
    {
        $options_table = ['options_js'=>['url_label'=>$this->generateUrl('product_label_change',['id'=>0]),'url_visible'=>$this->generateUrl('product_visible_change',['id'=>0])]];
        return $this->index_defaut('product',$options_table);
    }


    
    #[Route(path: '/new', name: 'product_new', methods: 'GET|POST')]
    public function new()
    {
        $args_twig['js_init'] = 'product_form';
        return $this->new_defaut([], null, [], true, $args_twig);
    }

    #[Route(path: '/{id}/edit', name: 'product_edit', methods: 'GET|POST')]
    public function edit(Product $ob)
    {
        $args_twig = [
            'js_init' => 'product_form',
            'js_init_args' => ['url_mot_search' => $this->generateUrl('mot_index')]
        ];
        return $this->edit_defaut($ob, [], null, [], true, $args_twig);
    }


    #[Route(path: '/{id}/from', name: 'product_from', methods: 'GET|POST')]
    public function product_from(Product $ob,Chargeur $chargeur)
    {
        $product_extra = $ob->getProductExtra();

        $data = [
            'product_id'=>$ob->getId(),

        ];
        if ($product_extra){
            $data['from'] = $product_extra->getId();
        }
        $form = $this->generateForm('Productfrom', ProductfromType::class, $data, [], true);
        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                if (!$product_extra){
                    $product_extra=new ProductExtra();
                    $product_extra->setProduct($ob);
                }

                $id_from = $data['from_product_id'];
                $productFrom = $chargeur->charger_objet('product',$id_from);
                $product_extra->setFromProduct($productFrom);
                $this->em->persist($product_extra);
                $this->em->flush();
                $args_twig['ok']=true;
                $args_twig['modif_html']=[
                    'dd'=>"",
                    'ddd'=>"",
                    'ss'=>""
                ];
                $args_twig['url_redirect']=($this->requete->get('redirect'));


            }
        }
        $args_twig['js_init'] = 'product_form';
        return $this->reponse_formulaire($form,$args_twig);
    }



    #[Route(path: '/{id}/product_from_delete', name: 'product_from_delete', methods: 'GET')]
    function product_from_delete(Product $ob)
    {
        $product_extra = $ob->getProductExtra();
        $product_extra->setFromProduct(null);
        $this->em->persist($product_extra);
        $this->em->flush();
        return $this->redirectToRoute('product_show',['id'=>$ob->getId()]);
    }





    #[Route(path: '/{id}/image_delete', name: 'product_image_delete', methods: 'GET|POST')]
    public function product_image_delete(Product $ob, Synchro $synchro)
    {

        $result = $synchro->supprimer_image('product', $ob->getPrimaryKey());
        if ($result) {
            $ob->setHasImage(false);
            $image = $this->em->getRepository(Image::class)->findOneBy(['modelId' => $ob->getPrimaryKey(), 'model' => 'product']);
            if ($image) {
                $this->em->remove($image);
            }
            $this->em->persist($ob);
            $this->em->flush();
        }

        return $this->json(['ok' => $result]);
    }


    #[Route(path: '/classify', name: 'product_classify', methods: 'GET|POST')]
    public function classify(Selecteur $selecteur)
    {
        $selecteur->setObjet('category');
        $tab_id = $selecteur->getTabId(['niveau' => 1]);
        $em = $this->em;
        $tab_category = $em->getRepository(Category::class)->findBy(['id' => $tab_id]);
        $args_twig = [
            'tab_categories' => $tab_category,
            'content' => ''
        ];
        return $this->render('product/classify.html.twig', $args_twig);
    }


    #[Route(path: '/edition_massive', name: 'product_edition_massive', methods: 'GET|POST')]
    public function product_edition_massive(ClientAPI $clientAPI)
    {

        $data = ['taux' => 20];
        $form_marge = $this->generateForm('taux_de_marge', TauxDeMargeType::class, $data, [], true);

        $form_marge->handleRequest($this->requete->getRequest());

        if ($form_marge->isSubmitted()) {

            if ($form_marge->isValid()) {
                $nb_maj = 0;
                $data = $form_marge->getData();
                $nouvelle_marge = 1 - ($data['taux'] / 100);
                $tab_produits = $this->em->getRepository(Product::class)->findAll();
                foreach ($tab_produits as $produit) {
                    $prix_achat = $produit->getPriceBuy();
                    $taux_tva = 1 + $produit->getTax()->getRate();
                    $nouveau_prix_de_vente = ($prix_achat / $nouvelle_marge) * $taux_tva;
                    $nouveau_prix_de_vente = round($nouveau_prix_de_vente / 0.05) * 0.05;
                    $nouveau_prix_de_vente = $nouveau_prix_de_vente / $taux_tva;
                    $produit->setPriceSell($nouveau_prix_de_vente);
                    $ob_tab = $produit->toStruct();
                    $ob_tab['id'] = $produit->getId();
                    [$reponse, $statut, $err] = $clientAPI->appelAPI('/api/product', $ob_tab, [], 'POST');
                    if ($statut == 200) {
                        $this->em->persist($produit);
                    }
                }
                $this->em->flush();
            }
        }

        $data = ['nb_jour' => 90];
        $form_desactivate = $this->generateForm('desactivate_products', DesactivateProductsType::class, $data, [], true);
        $form_desactivate->handleRequest($this->requete->getRequest());
        if ($form_desactivate->isSubmitted()) {
            if ($form_desactivate->isValid()) {
                $data = $form_desactivate->getData();
                $db = $this->em->getConnection();
                $date = (new \Datetime())->sub(new \DateInterval('P'.$data['nb_jour'].'D'));
                $tab_id = $db->fetchFirstColumn("select id from products p WHERE visible=1 AND (SELECT SUM(quantity) FROM stat_sales s WHERE p.id = s.product_id AND date> '".$date->format('Y-m-d')."') is NULL");
                $tab_produits = $this->em->getRepository(Product::class)->findBy(['id'=>$tab_id]);
                foreach ($tab_produits as $produit) {
                    if(isset($tab_id)){
                        $produit->setVisible(false);
                        $ob_tab = $produit->toStruct();
                        $ob_tab['id'] = $produit->getId();
                        [$reponse, $statut, $err] = $clientAPI->appelAPI('/api/product', $ob_tab, [], 'POST');
                        if ($statut == 200) {
                            $this->em->persist($produit);
                        }
                    }
                }
                $this->em->flush();

            }
        }


        $args_twig = [
            'form_marge' => $form_marge->createView(),
            'form_desactivate' => $form_desactivate->createView()
        ];
        return $this->render('product/edition_massive.html.twig', $args_twig);

    }


    #[Route(path: '/listing', name: 'product_listing')]
    public function product_listing(Selecteur $selecteur, Pdf $knpSnappyPdf)
    {
        $format = $this->requete->get('format', 'html');
        $inactif = $this->requete->get('inactif');
        $petit_code = $this->requete->get('petit_code');


        $tab_produit_o = $this->em->getRepository(Product::class)->findByBarcodeAndVisible($petit_code,$inactif);
        $tab_produit = [];
        $twig ='listing';


        foreach ($tab_produit_o as $prod) {


            $coeff_barcode = 1;

            $generatorHTML = new BarcodeGeneratorHTML();
            try {
                $barcode_image = $generatorHTML->getBarcode($prod->getBarcode() , $generatorHTML::TYPE_CODE_128, $coeff_barcode);
            } catch (\Exception) {
                $barcode_image = 'Erreur Code';
            }
            $id_cat=$prod->getCategory()->getId();
            $tab_produit[] = [
                'id' => $prod->getId(),
                'barcode_image' => $barcode_image,
                'barcode' => $prod->getBarcode(),
                'label' => $prod->getLabel(),
                'category' => $id_cat,
                'prix' => $prod->getTaxedPrice(),
                'contenance' => $prod->getScaleValue(),
                'contenance_unite' => $prod->getScaleType()
            ];
            $tab_nb_by_cat[$id_cat] = ($tab_nb_by_cat[$id_cat]??0)+1;
        }


        $tab_hierarchie = $this->sac->tab('category_tree');
        $tab_cat = $this->sac->tab('category');
        $tab_cat = arbre_linearise_niveau($tab_hierarchie,$tab_cat);

        $tab_temp=[];
        foreach( $tab_cat as $id=>$c){
            $niveau = $c['niveau'];
            $nb = $tab_nb_by_cat[$id]??0;
            $tab_cat[$id]['nb']=$nb;
            $tab_temp = array_slice($tab_temp,0,$niveau,true);
            foreach($tab_temp as $c_parent){
                $tab_cat[$c_parent]['nb'] += $nb;
            }
            $tab_temp[$c['niveau']]= $id;
        }



        $args_twig = [
            'tab_cat' => $tab_cat,
            'tab_produit' => table_regrouper($tab_produit,'category'),
            'date_du_jour' => new \DateTime(),
            'pdf' =>($format === 'pdf'),
            'args'=>['inactif'=>$inactif,'petit_code'=>$petit_code]
        ];

        if ($args_twig['pdf']) {
            $args_twig['layout'] = '_pdf';
            $outputfile = $this->sac->get('dir.root') . 'var/bulletin_commande.pdf';
            if (file_exists($outputfile)) {
                unlink($outputfile);
            }
            $html = $this->renderView('product/'.$twig.'.html.twig', $args_twig);
            $knpSnappyPdf->generateFromHtml($html,$outputfile);
            return $this->file($outputfile);
        } else {

            return $this->render('product/'.$twig.'.html.twig', $args_twig);
        }
    }



    #[Route(path: '/grid', name: 'product_grid')]
    public function product_grid(Selecteur $selecteur, Pdf $knpSnappyPdf)
    {
        $format = $this->requete->get('format', 'html');
        $noir_blanc = $this->requete->get('couleur', 1);
        $visible = true;
        $petit_code = 99999999;

        $tab_produit_o = $this->em->getRepository(Product::class)->findByBarcodeAndVisible($petit_code,$visible);
        $tab_produit = [];
        $twig ='grid';

        foreach ($tab_produit_o as $prod) {

            $id_cat=$prod->getCategory()->getId();
            $tab_produit[] = [
                'id' => $prod->getId(),
                'barcode' => $prod->getBarcode(),
                'label' => $prod->getLabel(),
                'category' => $id_cat,
                'prix' => $prod->getTaxedPrice(),
                'contenance' => $prod->getScaleValue(),
                'contenance_unite' => $prod->getScaleType()
            ];
            $tab_nb_by_cat[$id_cat] = ($tab_nb_by_cat[$id_cat]??0)+1;
        }


        $tab_hierarchie = $this->sac->tab('category_tree');
        $tab_cat = $this->sac->tab('category');
        $tab_cat = arbre_linearise_niveau($tab_hierarchie,$tab_cat);

        $tab_temp=[];
        foreach( $tab_cat as $id=>$c){
            $niveau = $c['niveau'];
            $nb = $tab_nb_by_cat[$id]??0;
            $tab_cat[$id]['nb']=$nb;
            $tab_temp = array_slice($tab_temp,0,$niveau,true);
            foreach($tab_temp as $c_parent){
                $tab_cat[$c_parent]['nb'] += $nb;
            }
            $tab_temp[$c['niveau']]= $id;
        }



        $args_twig = [
            'tab_cat' => $tab_cat,
            'tab_produit' => table_regrouper($tab_produit,'category'),
            'date_du_jour' => new \DateTime(),
            'pdf' =>($format === 'pdf'),
            'noir_blanc' =>($noir_blanc == 0),
            'args'=>['visible'=>$visible,'petit_code'=>$petit_code]
        ];

        if ($args_twig['pdf']) {
            $args_twig['layout'] = '_pdf';
            $options=[ 'orientation'=>'Landscape'];
            $outputfile = $this->sac->get('dir.root') . 'var/product_grid.pdf';
            if (file_exists($outputfile)) {
                unlink($outputfile);
            }
            $html = $this->renderView('product/'.$twig.'.html.twig', $args_twig);
            $knpSnappyPdf->generateFromHtml($html,$outputfile,$options);
            return $this->file($outputfile);
        } else {

            return $this->render('product/'.$twig.'.html.twig', $args_twig);
        }
    }




    #[Route(path: '/classify_browse', name: 'classify_browse', methods: 'GET|POST')]
    function classify_browse(Selecteur $selecteur)
    {
        $category = $this->requete->get('product');
        $selecteur->setObjet('product');

        $sql = $selecteur->getSelectionObjet(['category_id' => $category], ['id', 'label', 'barcode as code']);
        [$order, $tab_left] = $selecteur->tri_sql('product', ['barcode'=>'ASC']);
        $db = $this->em->getConnection();
        $tab_product = $db->fetchAllAssociative($sql.$order);
        return $this->json($tab_product);
    }


    #[Route(path: '/classify_modify', name: 'classify_modify', methods: 'GET|POST')]
    function classify_modify(Chargeur $chargeur, ClientAPI $clientAPI)
    {
        $category_id = $this->requete->get('category');
        $tab_products = json_decode((string) $this->requete->get('tab_products'));
        $category = $chargeur->charger_objet('category', $category_id);
        $em = $this->em;
        foreach ($tab_products as $id_product) {
            $product = $chargeur->charger_objet('product', $id_product);
            $product->setCategory($category);
            $pr = $product->toStruct();
            [$reponse, $statut, $err] = $clientAPI->appelAPI('/api/product', $pr, [], 'POST');
            if ($statut == 200) {
                $em->persist($product);
                $em->flush();
            }
        }
        return $this->json('ok');
    }


    #[Route(path: '/id_libre', name: 'product_id_libre', methods: 'GET|POST')]
    public function id_libre()
    {
        $db = $this->em->getConnection();
        $tab_product = $db->fetchAllAssociative('select barcode, label from products where barcode<1000 order by barcode');
        $tab_id_libre = array_fill(0, 1000, 'Libre comme l\'air');
        foreach ($tab_product as $product) {
            if (isset($tab_id_libre[$product['barcode']])) {
                $tab_id_libre[$product['barcode']] = $product['label'];
            }
        }

        return $this->render('product/id_libre.html.twig', ['tab_id_libre' => $tab_id_libre]);
    }







    #[Route(path: '/{id}/label', name: 'product_label_change', methods: 'GET|POST')]
    public function product_label_change(Product $ob, ProductAction $pa)
    {
        return $this->json(['ok'=>$pa->labelToPrint($ob)]);
    }


    #[Route(path: '/{id}/visible', name: 'product_visible_change', methods: 'GET|POST')]
    public function product_visible_change(Product $ob,ProductAction $pa)
    {
        $ob->setVisible(!$ob->getVisible());
        $this->em->persist($ob);
        $this->em->flush();
        $ok = $pa->modification_api($ob);
        if (!$ok){
            $ob->setVisible(!$ob->getVisible());
            $this->em->persist($ob);
            $this->em->flush();
        }
        return $this->json(['ok'=>$ok]);
    }



    #[Route(path: '/{id}', name: 'product_delete', methods: 'DELETE')]
    public function delete(Product $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'product_show', methods: 'GET|POST')]
    public function show(Product $ob, UploaderImage $uploaderImage)
    {

        $nom_champs = 'product_' . $ob->getPrimaryKey() . '_logo';
        $ob_ext = new ProductExt($ob, $this->sac, $this->em);

        $form_image = $this->generateFormUpload($nom_champs);
        $uploaderImage->traitementFormulaireImage($form_image, $ob, $ob_ext, 'product');

        $tab_stat = $ob_ext->getStat();

        $args_twig = [
            'product' => $ob,
            'product_ext' => $ob_ext,
            'form_image' => $form_image->createView(),
            'tab_stat' => $tab_stat
        ];

        return $this->render($this->sac->fichier_twig(), $args_twig);
    }


}
