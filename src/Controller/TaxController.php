<?php

namespace App\Controller;

use App\Entity\Tax;
use Declic3000\Pelican\Service\ControllerObjet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/tax')]
class TaxController extends ControllerObjet
{

    
    #[Route(path: '/', name: 'tax_index', methods: 'GET')]
    public function index()
    {
        return $this->index_defaut();
    }


    
    #[Route(path: '/new', name: 'tax_new', methods: 'GET|POST')]
    public function new()
    {
        return $this->new_defaut();
    }

    #[Route(path: '/{id}/edit', name: 'tax_edit', methods: 'GET|POST')]
    public function edit(Tax $ob)
    {
        return $this->edit_defaut($ob);
    }


    #[Route(path: '/{id}', name: 'tax_delete', methods: 'DELETE')]
    public function delete(Tax $ob)
    {
        return $this->delete_defaut($ob);

    }

    #[Route(path: '/{id}', name: 'tax_show', methods: 'GET')]
    public function show(Tax $ob)
    {
        return $this->show_defaut($ob);

    }
}
