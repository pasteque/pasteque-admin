<?php
namespace App\Security\Hasher;

use Symfony\Component\PasswordHasher\Exception\InvalidPasswordException;
use Symfony\Component\PasswordHasher\Hasher\CheckPasswordLengthTrait;
use Symfony\Component\PasswordHasher\LegacyPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

/**
* class PasswordEncoder
*/
final class NanoSha2PasswordHasher implements LegacyPasswordHasherInterface
{

    use CheckPasswordLengthTrait;
    private $algorithm;

    public function __construct()
    {
        $this->algorithm = new NanoSha2();;

    }


    /**
    * {@inheritdoc}
    */
    public function hash(string $plainPassword, string $salt = null): string
    {
        if ($this->isPasswordTooLong($plainPassword)) {
            throw new InvalidPasswordException();
        }

        return $this->algorithm->hash(($salt??'').$plainPassword);
    }

    /**
    * {@inheritdoc}
    */
    public function verify(string $hashedPassword, string $plainPassword, string $salt = null): bool
    {

        if ($this->isPasswordTooLong($plainPassword)) {
            throw new InvalidPasswordException();
        }

        return hash_equals($hashedPassword, $this->hash($plainPassword,$salt));
    }


    /**
     * {@inheritdoc}
     */
    public function needsRehash(string $hashedPassword): bool
    {
        return false;
    }



}
