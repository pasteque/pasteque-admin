<?php

namespace App\Security;

use App\Entity\Autorisation;
use  Declic3000\Pelican\Service\Chargeur;
use App\Service\ChargeurDb;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Selecteur;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityListener
{

    protected $em;
    protected $session;
    protected $token;
    protected $selecteur;
    protected $sac;

    public function __construct(UsageTrackingTokenStorage $tokenStorage, EntityManagerInterface $em, RequestStack $requestStack, Selecteur $selecteur,Sac $sac)
    {
        $this->em = $em;
        $this->sac = $sac;
        $this->session = $requestStack->getSession();
        $this->selecteur = $selecteur;
        if ($tokenStorage) {
            $this->token = $tokenStorage->getToken();
        }
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $this->token->getUser();
        $vars_suc = [
            'operateur' => [
                'nom' => $user->getNom(),
                'pseudo' => $user->getUsername(),
                'id' => $user->getPrimaryKey(),
                'email' => $user->getEmail()
            ]
        ];
        $this->session->set('variable', $vars_suc);
    }




}