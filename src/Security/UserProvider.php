<?php
namespace App\Security;


use App\Service\ClientAPI;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;


class UserProvider implements UserProviderInterface
{

    protected $clientAPI;
    protected $session;



    public function __construct(ClientAPI $clientAPI,RequestStack $requestStack)
    {
        $this->clientAPI   = $clientAPI;
        if($request = $requestStack->getCurrentRequest() and  $request->hasSession()){
            $this->session   = $requestStack->getSession();
        }
    }


    public function loadUserByIdentifier(string $identifier): UserInterface
    {

        [$user, $statut, $err] = $this->clientAPI->appelAPI('/api/user/getByName/'.$identifier);

        if ($statut == "200" && isset($user->id)){
            [$role, $statut, $err] = $this->clientAPI->appelAPI('/api/role/'.$user->role);
            $user->roles=['ROLE_'.$role->name];
            $this->session->set('user',$user);
        }
        return new Utilisateur($user->id, $user->name, $user->password, '',
            $user->roles, $user->name, true, true, true, true);
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof Utilisateur) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $user::class));
        }
        return $this->loadUserByIdentifier($user->getUsername());
    }


    public function supportsClass($class): bool
    {
        return $class === \App\Security\Utilisateur::class;
    }
}
