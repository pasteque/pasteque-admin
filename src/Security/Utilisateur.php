<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Security;

use Symfony\Component\Security\Core\User\LegacyPasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * User is the user implementation used by the in-memory user provider.
 *
 * This should not be used for anything else.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
final class Utilisateur implements UserInterface,LegacyPasswordAuthenticatedUserInterface
{

    private $username;
    private $email;
    private $token;






    public function __construct(private $id,$username, private $password, private $salt, private readonly array $roles = [],private $nom="",
                                private $enabled = true, private $accountNonExpired = true, private $credentialsNonExpired = true, private $accountNonLocked = true)
     {
        if (empty($username)) {
             throw new \InvalidArgumentException('The username cannot be empty.');
        }
            $this->username = $username;

     }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * {@inheritdoc}
     */
    public function getPrimaryKey()
    {
        return $this->id;
    }





    /**
     * {@inheritdoc}
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';
        $tab_role =  $roles;
        $tab_niveau = [1 => 'VIS', 2 => ['CRE', 'MOD'], 3 => 'SUP'];
        foreach($tab_role as $role){
            $tab_objet = getLienProfilGroupeObjet($role);
            if (!empty($tab_objet)) {
                for ($i = 1; $i <= 3; $i++) {
                    $tab_n = is_array($tab_niveau[$i]) ? $tab_niveau[$i] : [$tab_niveau[$i]];
                    foreach ($tab_n as $n) {
                        foreach ($tab_objet as $o) {
                            $tab_role[] = 'ROLE_OBJET_' . $o . '_' . $n;
                        }
                    }
                }
            }
        }
        return  array_unique($tab_role);
    }


    /**
     * {@inheritdoc}
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
		return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }
    public function getNom()
    {
        return $this->nom;
    }

    public function getEmail()
    {
        return $this->email;
    }



    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials(): void
    {
    }


    public function getToken()
    {
        return $this->token;
    }


    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }
}
