<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Ignore;

/**
 * Class Resource. Legacy class used while there are still resources in use.
 * Please do not create new resources.
 * @package Pasteque
 */
#[ORM\Table(name: 'resources')]
#[ORM\Entity]
class Resource extends Entity
{
    const TYPE_TEXT = 0;
    const TYPE_IMAGE = 1;
    const TYPE_BIN = 2;

    protected function getDirectFieldNames() {
        return ['label', 'resType', 'content'];
    }
    protected function getAssociationFields() {
        return [];
    }

    public function getId() { return $this->getLabel(); }

    /**
     * Name of the resource
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ORM\Id]
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) { $this->label = $label; }

    /**
     * Type of the resource. See constants.
     * @var int resType
     */
    #[ORM\Column(type: 'integer')]
    protected $type = 0;
    public function getType() { return $this->type; }
    public function setType($type) { $this->type = $type; }

    /**
     * Content
     * @var binary
     */
    #[ORM\Column(type: 'blob')]
    #[Ignore]
    protected $content;
    public function getContent() { return $this->content; }
    public function setContent($content) { $this->content = $content; }

    public function toStruct() {
        // Handle base64 encoding of binary bin and not binary text
        $struct = ['label' => $this->getLabel(), 'type' => $this->getType()];
        switch ($this->getType()) {
            case static::TYPE_TEXT:
                $struct['content'] = stream_get_contents($this->getContent());
                break;
            case static::TYPE_IMAGE:
            case static::TYPE_BIN:
            default:
                // It is safer to base64encode because json_encode
                // may crash everything with binary
                $struct['content'] = base64_encode(stream_get_contents($this->getContent()));
                break;
        }
        return $struct;
    }
}
