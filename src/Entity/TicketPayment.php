<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use \App\System\DAO\DAOCondition;

/**
 * Class TicketPayment
 * @package Pasteque
 */
#[ORM\Table(name: 'ticketpayments')]
#[ORM\Entity]
class TicketPayment extends Entity // Embedded class
{
    public function getDirectFieldNames() {
        return ['dispOrder', 'amount', 'currencyAmount'];
    }
    public function getAssociationFields() {
        return [
                [
                 'name' => 'ticket',
                 'class' => \App\Entity\Ticket::class,
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'paymentMode',
                 'class' => \App\Entity\PaymentMode::class
                 ],
                [ // This array index (2) is hardcoded in fromStruct
                 'name' => 'currency',
                 'class' => \App\Entity\Currency::class
                 ]
                ];
    }
    public function getId() {
        return ['ticket' => $this->getTicket()->getId(), 'dispOrder' => $this->getDispOrder()];
    }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'ticket_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Ticket::class, inversedBy: 'payments')]
    #[ORM\Id]
    protected $ticket;
    public function getTicket() { return $this->ticket; }
    public function setTicket($ticket) { $this->ticket = $ticket; }

    /**
     * Display order or number of the line
     * @var integer
     */
    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    protected $dispOrder;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }

    /**
     * Type of the Payment
     * @var string
     */
    #[ORM\JoinColumn(name: 'paymentmode_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: PaymentMode::class)]
    protected $paymentMode;
    public function getPaymentMode() { return $this->paymentMode; }
    public function setPaymentMode($paymentMode) { $this->paymentMode = $paymentMode; }

    /**
     * Amount of the payment in the main currency
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $amount;
    public function getAmount() { return round($this->amount, 5); }
    public function setAmount($amount) {
        $this->amount = round($amount, 5);
    }

    /**
     * Id of the Currency of the Payment
     * @var int
     */
    #[ORM\JoinColumn(name: 'currency_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Currency::class)]
    protected $currency;
    public function getCurrency() { return $this->currency; }
    public function setCurrency($currency) { $this->currency = $currency; }

    /**
     * Amount of the Payment in the used Currency
     * @var float
     */
    #[ORM\Column(type: 'float')]
    public $currencyAmount;
    public function getCurrencyAmount() {
        return round($this->currencyAmount, 5);
    }
    public function setCurrencyAmount($currencyAmount) {
        $this->currencyAmount = round($currencyAmount, 5);
    }


    public function toStruct() {
        $struct = parent::toStruct();
        // Add 'type' for compatibility with Desktop
        // which doesn't use PaymentMode.
        $struct['type'] = $this->getPaymentMode()->getReference();
        return $struct;
    }

}
