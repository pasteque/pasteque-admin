<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use \App\Entity\Floor;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Place
 * @package Pasteque
 */
#[ORM\Table(name: 'places')]
#[ORM\Entity]
class Place extends Entity
{

    protected function getDirectFieldNames() {
        return [];
    }



    protected function getAssociationFields() {
        return [ ];
    }

    /**
     * ID of the place
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * Label of the place
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) { $this->label = $label; }

    /**
     * Position X for draw.
     * @var int x
     */
    #[ORM\Column(type: 'integer')]
    protected $x = 0;
    public function getX() { return $this->x; }
    public function setX($x) { $this->x = $x; }

    /**
     * Position Y for draw.
     * @var int y
     */
    #[ORM\Column(type: 'integer')]
    protected $y = 0;
    public function getY() { return $this->y; }
    public function setY($y) { $this->y = $y; }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'floor_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Floor::class, inversedBy: 'places')]
    protected $floor;
    public function getFloor() { return $this->floor; }
    public function setFloor($floor) { $this->floor = $floor; }

}
