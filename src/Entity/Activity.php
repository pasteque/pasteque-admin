<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Preference
 * @package Pasteque
 */
#[ORM\Table(name: 'activities')]
#[ORM\Entity]
class Activity extends Entity
{


    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }

    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'smallint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 120, nullable: false)]
    private $name;


}

