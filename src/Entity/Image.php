<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Image.
 * Images are loosely coupled to the other models not to send them with
 * other data. If embedded, the images would be always sent, if not
 * embedded, updating images would be a mess with the ids.
 * Instead other models has a flag named "hasImage" and images are
 * identified by the model name and it's id. This flag is managed by
 * ImageAPI and should not be edited in an other way.
 * @package Pasteque
 */
#[ORM\Table(name: 'images')]
#[ORM\Entity]
class Image extends Entity
{
    const MODEL_CATEGORY = 'category';
    const MODEL_PRODUCT = 'product';
    const MODEL_USER = 'user';
    const MODEL_CUSTOMER = 'customer';
    const MODEL_PAYMENTMODE = 'paymentmode';
    const MODEL_PAYMENTMODE_VALUE = 'paymentmodevalue';

    protected function getDirectFieldNames() {
        return ['model', 'modelId', 'mimeType', 'image'];
    }
    protected function getAssociationFields() {
        return [];
    }

    public function getId() {
        return ['model' => $this->getModel(), 'modelId' => $this->getModelId()];
    }

    /**
     * Type of model the image is for. See constants.
     * @var string
     */
    #[ORM\Id]
    #[ORM\Column(type: Types::STRING)]
    protected $model;
    public function getModel() { return $this->model; }
    public function setModel($model) { $this->model = $model; }

    /**
     * ID of the model the image is for.
     * It is a string to allow multi-fied ids as a JSON string.
     * @var string
     */
    #[ORM\Id]
    #[ORM\Column(type: Types::STRING)]
    protected $modelId;
    public function getModelId() { return $this->modelId; }
    /** Set the model id. It accepts a single value (int, string),
     * or an array that is convented in json format. */
    public function setModelId($modelId) {
        if (is_array($modelId)) {
            $this->modelId = json_encode($modelId);
        } else {
            $this->modelId = $modelId;
        }
    }

    /**
     * Mime type
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $mimeType;
    public function getMimeType() { return $this->mimeType; }
    public function setMimeType($mimeType) { $this->mimeType = $mimeType; }

    /**
     * The actual image
     * @var binary
     */
    #[ORM\Column(type: 'blob')]
    protected $image;
    public function getImage() { return $this->image; }
    public function setImage($image) { $this->image = $image; }

}
