<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Ignore;

/**
 * ProductsExtra
 *
 * @package Pasteque
 */
#[ORM\Table(name: 'products_extra')]
#[ORM\Index(name: 'location', columns: ['location'])]
#[ORM\Entity]
class ProductExtra extends Entity
{

    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {

    }

    public function getId(){
        return $this->getProduct()->getId();
    }

    #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id', nullable: true)]
    #[ORM\OneToOne(targetEntity: Product::class, inversedBy: 'productExtra')]
    #[ORM\Id]
    #[Ignore]
    private $product;

    /**
     * @var string
     */
    #[ORM\Column(name: 'description', type: 'text', nullable: true)]
    private $description;

    /**
     * @var string
     */
    #[ORM\Column(name: 'emb_code', type: 'string', length: 25, nullable: true)]
    private $embCode;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'is_transformed', type: 'boolean', nullable: true)]
    private $isTransformed;

    /**
     * @var float
     */
    #[ORM\Column(name: 'volume', type: 'float', precision: 7, scale: 2, nullable: true)]
    private $volume;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'measure', type: 'boolean', nullable: true)]
    private $measure;

    /**
     * @var integer
     *
     *
     */
    #[ORM\Column(name: 'location', type: 'integer', nullable: true)]
    private $location;





    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level1', type: 'text', length: 65535, nullable: true)]
    private $adminLevel1;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level2', type: 'text', length: 65535, nullable: true)]
    private $adminLevel2;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level3', type: 'text', length: 65535, nullable: true)]
    private $adminLevel3;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level4', type: 'text', length: 65535, nullable: true)]
    private $adminLevel4;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level5', type: 'text', length: 65535, nullable: true)]
    private $adminLevel5;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level6', type: 'text', length: 65535, nullable: true)]
    private $adminLevel6;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level7', type: 'text', length: 65535, nullable: true)]
    private $adminLevel7;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level8', type: 'text', length: 65535, nullable: true)]
    private $adminLevel8;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level9', type: 'text', length: 65535, nullable: true)]
    private $adminLevel9;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level10', type: 'text', length: 65535, nullable: true)]
    private $adminLevel10;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level11', type: 'text', length: 65535, nullable: true)]
    private $adminLevel11;

    /**
     * @var string
     */
    #[ORM\Column(name: 'admin_level12', type: 'text', length: 65535, nullable: true)]
    private $adminLevel12;

    /**
     * @var float
     */
    #[ORM\Column(name: 'coord_lat', type: 'float', precision: 10, scale: 0, nullable: true)]
    private $coordLat;

    /**
     * @var float
     */
    #[ORM\Column(name: 'coord_lon', type: 'float', precision: 10, scale: 0, nullable: true)]
    private $coordLon;


    /**
     * @var boolean
     */
    #[ORM\Column(name: 'label_to_print', type: 'boolean', nullable: false)]
    private $labelToPrint = true;


    /**
     * @var Product
     */
    #[ORM\JoinColumn(name: 'from_product', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \Product::class)]
    private $fromProduct = null;



    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $productId
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }




    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getEmbCode(): string
    {
        return $this->embCode;
    }

    /**
     * @param string $embCode
     */
    public function setEmbCode(string $embCode): void
    {
        $this->embCode = $embCode;
    }

    /**
     * @return bool
     */
    public function isTransformed(): bool
    {
        return $this->isTransformed;
    }

    /**
     * @param bool $isTransformed
     */
    public function setIsTransformed(bool $isTransformed): void
    {
        $this->isTransformed = $isTransformed;
    }

    /**
     * @return float
     */
    public function getVolume(): float
    {
        return $this->volume;
    }

    /**
     * @param float $volume
     */
    public function setVolume(float $volume): void
    {
        $this->volume = $volume;
    }

    /**
     * @return bool
     */
    public function isMeasure(): bool
    {
        return $this->measure;
    }

    /**
     * @param bool $measure
     */
    public function setMeasure(bool $measure): void
    {
        $this->measure = $measure;
    }

    /**
     * @return int
     */
    public function getLocation(): ?int
    {
        return $this->location;
    }

    /**
     * @param null|int $location
     */
    public function setLocation(?int $location): void
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getAdminLevel1(): string
    {
        return $this->adminLevel1;
    }

    /**
     * @param string $adminLevel1
     */
    public function setAdminLevel1(string $adminLevel1): void
    {
        $this->adminLevel1 = $adminLevel1;
    }

    /**
     * @return string
     */
    public function getAdminLevel2(): string
    {
        return $this->adminLevel2;
    }

    /**
     * @param string $adminLevel2
     */
    public function setAdminLevel2(string $adminLevel2): void
    {
        $this->adminLevel2 = $adminLevel2;
    }

    /**
     * @return string
     */
    public function getAdminLevel3(): string
    {
        return $this->adminLevel3;
    }

    /**
     * @param string $adminLevel3
     */
    public function setAdminLevel3(string $adminLevel3): void
    {
        $this->adminLevel3 = $adminLevel3;
    }

    /**
     * @return string
     */
    public function getAdminLevel4(): string
    {
        return $this->adminLevel4;
    }

    /**
     * @param string $adminLevel4
     */
    public function setAdminLevel4(string $adminLevel4): void
    {
        $this->adminLevel4 = $adminLevel4;
    }

    /**
     * @return string
     */
    public function getAdminLevel5(): string
    {
        return $this->adminLevel5;
    }

    /**
     * @param string $adminLevel5
     */
    public function setAdminLevel5(string $adminLevel5): void
    {
        $this->adminLevel5 = $adminLevel5;
    }

    /**
     * @return string
     */
    public function getAdminLevel6(): string
    {
        return $this->adminLevel6;
    }

    /**
     * @param string $adminLevel6
     */
    public function setAdminLevel6(string $adminLevel6): void
    {
        $this->adminLevel6 = $adminLevel6;
    }

    /**
     * @return string
     */
    public function getAdminLevel7(): string
    {
        return $this->adminLevel7;
    }

    /**
     * @param string $adminLevel7
     */
    public function setAdminLevel7(string $adminLevel7): void
    {
        $this->adminLevel7 = $adminLevel7;
    }

    /**
     * @return string
     */
    public function getAdminLevel8(): string
    {
        return $this->adminLevel8;
    }

    /**
     * @param string $adminLevel8
     */
    public function setAdminLevel8(string $adminLevel8): void
    {
        $this->adminLevel8 = $adminLevel8;
    }

    /**
     * @return string
     */
    public function getAdminLevel9(): string
    {
        return $this->adminLevel9;
    }

    /**
     * @param string $adminLevel9
     */
    public function setAdminLevel9(string $adminLevel9): void
    {
        $this->adminLevel9 = $adminLevel9;
    }

    /**
     * @return string
     */
    public function getAdminLevel10(): string
    {
        return $this->adminLevel10;
    }

    /**
     * @param string $adminLevel10
     */
    public function setAdminLevel10(string $adminLevel10): void
    {
        $this->adminLevel10 = $adminLevel10;
    }

    /**
     * @return string
     */
    public function getAdminLevel11(): string
    {
        return $this->adminLevel11;
    }

    /**
     * @param string $adminLevel11
     */
    public function setAdminLevel11(string $adminLevel11): void
    {
        $this->adminLevel11 = $adminLevel11;
    }

    /**
     * @return string
     */
    public function getAdminLevel12(): string
    {
        return $this->adminLevel12;
    }

    /**
     * @param string $adminLevel12
     */
    public function setAdminLevel12(string $adminLevel12): void
    {
        $this->adminLevel12 = $adminLevel12;
    }

    /**
     * @return float
     */
    public function getCoordLat(): float
    {
        return $this->coordLat;
    }

    /**
     * @param float $coordLat
     */
    public function setCoordLat(float $coordLat): void
    {
        $this->coordLat = $coordLat;
    }

    /**
     * @return float
     */
    public function getCoordLon(): float
    {
        return $this->coordLon;
    }

    /**
     * @param float $coordLon
     */
    public function setCoordLon(float $coordLon): void
    {
        $this->coordLon = $coordLon;
    }

    /**
     * @return bool
     */
    public function getLabelToPrint(): bool
    {
        return $this->labelToPrint;
    }

    /**
     * @param bool $labelToPrint
     */
    public function setLabelToPrint(bool $labelToPrint): void
    {
        $this->labelToPrint = $labelToPrint;
    }


    /**
     * @return ?Product
     */
    public function getFromProduct()
    {
        return $this->fromProduct;
    }

    /**
     * @param ?Product $productId
     */
    public function setFromProduct(?Product $product): void
    {
        $this->fromProduct = $product;
    }





}

