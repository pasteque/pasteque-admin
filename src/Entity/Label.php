<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Label
 * @package Pasteque
 */
#[ORM\Table(name: 'labels')]
#[ORM\Entity]
class Label extends Entity
{

    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }



    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'code', type: 'text', length: 65535, nullable: true)]
    private $code;

    /**
     * @var string
     */
    #[ORM\Column(name: 'label', type: 'text', length: 65535, nullable: false)]
    private $label;

    /**
     * @var string
     */
    #[ORM\Column(name: 'label_category', type: 'string', length: 25, nullable: false)]
    private $labelCategory;

    /**
     * @var string
     */
    #[ORM\Column(name: 'description', type: 'text', nullable: false)]
    private $description;

    /**
     * @var string
     */
    #[ORM\Column(name: 'url', type: 'text', length: 65535, nullable: false)]
    private $url;


}

