<?php

namespace App\Entity;



use Doctrine\ORM\Mapping as ORM;

#[ORM\MappedSuperclass]
abstract class Entity extends \Declic3000\Pelican\Entity\Entity {


    public function getPrimaryKey(){

        $get = 'getId';
        return $this->$get();
    }








    public function __toString(): string {
        if(method_exists($this,'getLabel')){
            return (string) $this->getLabel();
        }elseif(method_exists($this,'getName')){
            return (string) $this->getName();
        }
        return '';
    }


    protected function getAllowedFields() {
        return $this->getDirectFieldNames();
    }


    // Required for toStruct for references. */
    public function getId() { return $this->id; }

    protected function directFieldToStruct($field) {
        $value = call_user_func([$this, 'get' . ucfirst((string) $field)]);
        return match (gettype($value)) {
            'resource' => base64_encode(stream_get_contents($value)),
            default => $value,
        };
    }

    protected function associationFieldToStruct($field) {
        $value = call_user_func([$this, 'get' . ucfirst((string) $field['name'])]);
        // Association field
        if (!empty($field['array'])) {
            // Value is a ArrayCollection
            $struct = [];
            for ($i = 0; $i < $value->count(); $i++) {
                if (empty($field['embedded'])) {
                    $struct[] = $value->get($i)->getId();
                } else {
                    $struct[] = $value->get($i)->toStruct();
                }
            }
            return $struct;
        } else {
            if ($value === null) {
                return null;
            } else {
                if (empty($field['embedded'])) {
                    return $value->getId();
                } else {
                    return $value->toStruct();
                }
            }
        }
    }




    /** List primitive typed field names of the model in an array, excluding id. */
    protected function getDirectFieldNames(){
        return [];
    }
    /** List reference field of the model in an array. A field is an associative
     * array with the following keys:
     * name: The field name (declared in code)
     * class: The full class name of the reference.
     * array (optional): can only be true if set, flag for XtoMany fields.
     * null (optional): can only be true if set, flag for nullable.
     * embedded (optional): can only be true if set, flag for subclasses.
     * Embedded values can be created on the fly from struct and are embedded
     * in toStruct. Non embedded fields are referenced only by id.
     * Embedded classes don't have their own id.
     * internal (optional): can only be true if set. Internal fields are not
     * read and exported in structs.
     * */
    protected function getAssociationFields(){
        return [];
    }




    /** Unlink the model from DAO and all methods.
     * All references are converted to their Id.
     * @return array
     * An associative array with raw data, suitable for
     * json encoding. */
    public function toStruct() {
        // Get Doctrine fields and render them to delete the proxies
        $data = ['id' => $this->getId()];
        foreach ($this->getDirectFieldNames() as $field) {
            $data[$field] = $this->directFieldToStruct($field);
        }
        foreach ($this->getAssociationFields() as $field) {
            if (empty($field['internal'])) {
                $data[$field['name']] = $this->associationFieldToStruct($field);
            }
        }
        return $data;
    }





}
