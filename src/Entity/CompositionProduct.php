<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Class CompositionProduct
 * @package Pasteque
 */
#[ORM\Table(name: 'compositionproducts')]
#[ORM\Entity]
class CompositionProduct extends Entity
{
    protected function getDirectFieldNames() {
        return ['dispOrder'];
    }
    protected function getAssociationFields() {
        return [
                [
                 'name' => 'compositionGroup',
                 'class' => \App\Entity\CompositionGroup::class,
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'product',
                 'class' => \App\Entity\Product::class,
                 ]
                ];
    }

    public function getId() { return ['compositionGroup' => $this->getCompositionGroup()->getId(),
            'product' => $this->getProduct()->getId()]; }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'compositiongroup_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: CompositionGroup::class, inversedBy: 'compositionProducts')]
    protected $compositionGroup;
    public function getCompositionGroup() { return $this->compositionGroup; }
    public function setCompositionGroup($compositionGroup) {
        $this->compositionGroup = $compositionGroup;
    }

    /**
     * Order of display
     * @var int order
     */
    #[ORM\Column(type: 'integer', name: 'disp_order')]
    protected $dispOrder = 0;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Product::class)]
    #[ORM\Id]
    protected $product;
    public function getProduct() { return $this->product; }
    public function setProduct($product) { $this->product = $product; }

}
