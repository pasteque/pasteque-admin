<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Class CashSessionCatTax. Sum of the cs amount by category by tax.
* This class is for fast data analysis only.
* For declarations see FiscalTicket.
* @package Pasteque
//
*/
#[ORM\Table(name: 'sessioncattaxes')]
#[ORM\Entity]
class CashSessionCatTax extends Entity // Embedded class
{
    public function getDirectFieldNames() {
        // Not associative to be able to delete empty categories.
        return ['reference', 'label', 'base', 'amount'];
    }
    public function getAssociationFields() {
        return [
                [
                 'name' => 'cashSession',
                 'class' => \App\Entity\CashSessionCat::class,
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'tax',
                 'class' => \App\Entity\Tax::class,
                ]
                ];
    }

    public function getId() {
        return ['cashSession' => $this->getCashSession()->getId(),
                'reference' => $this->getReference(),
                'tax' => $this->getTax()->getId()];
    }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'cashsession_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: CashSession::class, inversedBy: 'taxes')]
    #[ORM\Id]
    protected $cashSession;
    public function getCashSession() { return $this->cashSession; }
    public function setCashSession($cashSession) { $this->cashSession = $cashSession; }

    /**
     * Code of the category, user-friendly ID.
     * It is automatically set from label if not explicitely set.
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ORM\Id]
    protected $reference;
    public function getReference() { return $this->reference; }
    public function setReference($ref) { $this->reference = $ref; }

    /**
     * Label of the category
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) {
        $this->label = $label;
        if ($this->getReference() === null) {
            $this->setReference($label);
        }
    }

    /**
     * Id of the tax
     * @var integer
     */
    #[ORM\JoinColumn(name: 'tax_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Tax::class)]
    #[ORM\Id]
    protected $tax;
    public function getTax() { return $this->tax; }
    /** Set the tax. If taxRate is null, it will be set with
     * the rate of the tax. */
    public function setTax($tax) {
        $this->tax = $tax;
    }

    /**
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $base;
    public function getBase() { return round($this->base, 5); }
    public function setBase($base) {
        $this->base = round($base, 5);
    }

    /**
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $amount;
    public function getAmount() { return round($this->amount, 5); }
    public function setAmount($amount) {
        $this->amount = round($amount, 5);
    }

}
