<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Preference
 * @package Pasteque
 */
#[ORM\Table(name: 'preferences')]
#[ORM\Entity]
class Preference extends Entity
{

    use TimestampableEntity;
    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables','observation'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }

    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $name;
    public function getName() { return $this->name; }
    public function setName($name) { $this->name = $name; }


    /**
     * @var string
     */
    #[ORM\Column(type: 'text')]
    protected $variables;
    public function getVariables() { return json_decode($this->variables,true); }
    public function setVariables($variables) { $this->variables = json_encode($variables); }



    /**
     * @var ?string
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected $observation = null;
    public function getObservation() { return json_decode((string) $this->observation,true); }
    public function setObservation($observation) { $this->observation = json_encode($observation); }



    /**
     * ID of the assigned role
     * @var integer
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected $user_id;
    public function getUserId() { return $this->user_id; }
    public function setUserId($user_id) { $this->user_id = $user_id; }





}
