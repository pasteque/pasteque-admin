<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * EtapeProduct
 */
#[ORM\Table(name: 'etapes_products')]
#[ORM\Entity]
class EtapeProduct extends Entity
{


    protected function getDirectFieldNames() {
        return ['product_id', 'etape_id', 'etat'];
    }

    protected function getAssociationFields() {
        return [];
    }




    /**
     * @var integer
     */
    #[ORM\Column(name: 'etape_id', type: 'bigint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    protected $etape_id;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'product_id', type: 'bigint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    protected $product_id;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'etat', type: 'boolean', nullable: false)]
    protected $etat;

 



}

