<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PaymentModeReturn
 * This defines how payment with a higher value than requested are handled.
 * @package Pasteque
 */
#[ORM\Table(name: 'paymentmodereturns')]
#[ORM\Entity]
class PaymentModeReturn extends Entity // Embedded class
{
    protected function getDirectFieldNames() {
        return ['minAmount'];
    }
    protected function getAssociationFields() {
        return [
                [
                 'name' => 'paymentMode',
                 'class' => \App\Entity\PaymentMode::class,
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'returnMode',
                 'class' => \App\Entity\PaymentMode::class,
                 'null' => false
                 ]
                ];
    }

    public function getId() {
        if ($this->getPaymentMode() === null) { return null; }
        return ['paymentMode' => $this->getPaymentMode()->getId(), 'minAmount' =>$this->getMinAmount()];
    }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'paymentmode_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: PaymentMode::class, inversedBy: 'returns')]
    protected $paymentMode;
    public function getPaymentMode() { return $this->paymentMode; }
    /** Set the payment mode. For new PaymentMode when the id is not already set
     * it also set returnMode when null.
     * That means you can create a self referencing structure without knowing
     * the PaymentMode id (which doesn't exists on create)
     */
    public function setPaymentMode($paymentMode) {
        $this->paymentMode = $paymentMode;
        if ($this->getReturnMode() === null) {
            $this->setReturnMode($paymentMode);
        }
    }

    /**
     * @var float
     */
    #[ORM\Id]
    #[ORM\Column(type: 'float')]
    protected $minAmount = 0.0;
    public function getMinAmount() { return $this->minAmount; }
    public function setMinAmount($minAmount) { $this->minAmount = $minAmount; }

    /**
     * Can be null when creating a new return for a new PaymentMode.
     * In that case, when assigning the return mode to a payment mode,
     * it will then be linked to it.
     * @var integer
     */
    #[ORM\JoinColumn(name: 'returnmode_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: PaymentMode::class)]
    protected $returnMode;
    public function getReturnMode() { return $this->returnMode; }
    public function setReturnMode($returnMode) { $this->returnMode = $returnMode; }


}
