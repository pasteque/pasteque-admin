<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Ignore;

/**
 * ProductsSuppliers
 * @package Pasteque
 */
#[ORM\Table(name: 'products_suppliers')]
#[ORM\Index(name: 'SELLER', columns: ['activity'])]
#[ORM\Index(name: 'STEP', columns: ['parent_id'])]
#[ORM\UniqueConstraint(name: 'product_id', columns: ['product_id', 'supplier_id', 'parent_id'])]
#[ORM\Entity]
class ProductSupplier extends Entity
{




    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }


    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
         * @var Product
         */
        #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id')]
        #[ORM\ManyToOne(targetEntity: \Product::class, inversedBy: 'productSuppliers')]
        #[Ignore]
        private $product;

    /**
     * @var Supplier
     */
    #[ORM\JoinColumn(name: 'supplier_id', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \App\Entity\Supplier::class, inversedBy: 'productSuppliers')]
    #[Ignore]
    private $supplier;

    /**
     * @var null|string
     */
    #[ORM\Column(name: 'reference', type: 'string', length: 40, nullable: true)]
    private $reference;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'colisage', type: 'smallint', nullable: true)]
    private $colisage = null;


    /**
     * @var bool
     */
    #[ORM\Column(name: 'favori', type: 'boolean', nullable: false)]
    private $favori = true;


    /**
     * Buy price without taxes, used for estimated margin computation.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $priceBuy = null;



    /**
     * @var integer
     */
    #[ORM\Column(name: 'activity', type: 'smallint', nullable: true)]
    private $activity;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'parent_id', type: 'integer', nullable: true)]
    private $parentId;

    /**
     * @var float
     */
    #[ORM\Column(name: 'pourcent', type: 'float', precision: 2, scale: 2, nullable: true)]
    private $pourcent;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param string|null $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return string|null
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return string
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * @param null|string $reference
     */
    public function setReference(?string $reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @return int
     */
    public function getActivity(): int
    {
        return $this->activity;
    }

    /**
     * @param int $activity
     */
    public function setActivity(int $activity): void
    {
        $this->activity = $activity;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId(int $parentId): void
    {
        $this->parentId = $parentId;
    }

    /**
     * @return float
     */
    public function getPourcent(): float
    {
        return $this->pourcent;
    }

    /**
     * @param float $pourcent
     */
    public function setPourcent(float $pourcent): void
    {
        $this->pourcent = $pourcent;
    }

    /**
     * @return float
     */
    public function getPriceBuy() { return round($this->priceBuy, 5); }
    /**
     * @param ?float $priceBuy
     */
    public function setPriceBuy(?float $priceBuy) { $this->priceBuy = round($priceBuy, 5); }

    /**
     * @return int
     */
    public function getColisage(): ?int
    {
        return $this->colisage;
    }

    /**
     * @param int $colisage
     */
    public function setColisage(?int $colisage): void
    {
        $this->colisage = $colisage;
    }

    /**
     * @return bool
     */
    public function isFavori(): bool
    {
        return $this->favori;
    }

    /**
     * @param bool $favori
     */
    public function setFavori(bool $favori): void
    {
        $this->favori = $favori;
    }


}

