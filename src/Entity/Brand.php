<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Brand
 * @package Pasteque
 */
#[ORM\Table(name: 'brands')]
#[ORM\Entity]
class Brand extends Entity
{

    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }




    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'code', type: 'text', length: 65535, nullable: true)]
    private $code;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'text', length: 65535, nullable: false)]
    private $name;

    /**
     * @var string
     */
    #[ORM\Column(name: 'description', type: 'text', nullable: true)]
    private $description;

    /**
     * @var string
     */
    #[ORM\Column(name: 'text', type: 'text', nullable: true)]
    private $text;

    /**
     * @var string
     */
    #[ORM\Column(name: 'url', type: 'text', length: 65535, nullable: true)]
    private $url;


}

