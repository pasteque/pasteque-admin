<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
/**
 * SuppliersActivity
 */
#[ORM\Table(name: 'suppliers_activities')]
#[ORM\Entity]
class SupplierActivity extends Entity
{


    protected function getDirectFieldNames() {
        return [];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }






    /**
     * @var integer
     */
    #[ORM\Column(name: 'activity_id', type: 'smallint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $idActivity;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'supplier_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $idSupplier;


}

