<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
/**
 * Packagings
 *
 * @package Pasteque
 */
#[ORM\Table(name: 'packagings')]
#[ORM\Entity]
class Packaging extends Entity
{
    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }


    /**
     * @var integer
     */
    #[ORM\Column(name: 'id_packaging', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $idPackaging;

    /**
     * @var string
     */
    #[ORM\Column(name: 'code', type: 'text', length: 65535, nullable: true)]
    private $code;

    /**
     * @var string
     */
    #[ORM\Column(name: 'label', type: 'string', length: 80, nullable: false)]
    private $label;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'material_id', type: 'integer', nullable: true)]
    private $materialId;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'has_image', type: 'boolean', nullable: false)]
    private $hasImage;

    /**
     * @var string
     */
    #[ORM\Column(name: 'description', type: 'text', length: 65535, nullable: true)]
    private $description;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'text', type: 'integer', nullable: true)]
    private $text;

    /**
     * @var string
     */
    #[ORM\Column(name: 'url', type: 'text', length: 65535, nullable: true)]
    private $url;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'is_recyclable', type: 'boolean', nullable: true)]
    private $isRecyclable;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'is_reusable', type: 'boolean', nullable: true)]
    private $isReusable;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'is_refundable', type: 'boolean', nullable: true)]
    private $isRefundable;


}

