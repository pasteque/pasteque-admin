<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CashSessionTax. Sum of the taxes amount by tax.
 * This class is for fast data analysis only.
 * For declarations see FiscalTicket.
 * @package Pasteque
 */
#[ORM\Table(name: 'sessiontaxes')]
#[ORM\Entity]
class CashSessionTax extends Entity // Embedded class
{
    public function getDirectFieldNames() {
        return ['taxRate',
                'base', 'basePeriod', 'baseFYear',
                'amount', 'amountPeriod', 'amountFYear'];
    }
    public function getAssociationFields() {
        return [
                [
                 'name' => 'cashSession',
                 'class' => \App\Entity\CashSession::class,
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'tax',
                 'class' => \App\Entity\Tax::class,
                 ]
                ];
    }
    public function getId() {
        return ['cashSession' => $this->getCashSession()->getId(), 'tax' => $this->getTax()->getId()];
    }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'cashsession_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: CashSession::class, inversedBy: 'taxes')]
    #[ORM\Id]
    protected $cashSession;
    public function getCashSession() { return $this->cashSession; }
    public function setCashSession($cashSession) { $this->cashSession = $cashSession; }

    /**
     * Id of the tax
     * @var integer
     */
    #[ORM\JoinColumn(name: 'tax_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Tax::class)]
    #[ORM\Id]
    protected $tax;
    public function getTax() { return $this->tax; }
    /** Set the tax. If taxRate is null, it will be set with
     * the rate of the tax. */
    public function setTax($tax) {
        $this->tax = $tax;
        if ($this->getTaxRate() == null) {
            $this->setTaxRate($tax->getRate());
        }
    }

    /**
     * Rate of the tax at the time of the ticket
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $taxRate;
    public function getTaxRate() { return $this->taxRate; }
    public function setTaxRate($taxRate) { $this->taxRate = $taxRate; }

    /**
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: false)]
    protected $base = 0.0;
    public function getBase() { return round($this->base, 5); }
    public function setBase($base) {
        $this->base = round($base, 5);
    }

    /**
     * Tax base total by period.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: false)]
    protected $basePeriod = 0.0;
    public function getBasePeriod() {
        return round($this->basePeriod, 5);
    }
    public function setBasePeriod($basePeriod) {
            $this->basePeriod = round($basePeriod, 5);
    }

    /**
     * Tax base total by fiscal year.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: false)]
    protected $baseFYear = 0.0;
    public function getBaseFYear() {
        return round($this->baseFYear, 5);
    }
    public function setBaseFYear($baseFYear) {
            $this->baseFYear = round($baseFYear, 5);
    }

    /**
     * Total amount of tax.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: false)]
    protected $amount = 0.0;
    public function getAmount() { return round($this->amount, 5); }
    public function setAmount($amount) {
        $this->amount = round($amount, 5);
    }

    /**
     * Tax amount total by period.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: false)]
    protected $amountPeriod = 0.0;
    public function getAmountPeriod() {
        return round($this->amountPeriod, 5);
    }
    public function setAmountPeriod($amountPeriod) {
            $this->amountPeriod = round($amountPeriod, 5);
    }

    /**
     * Tax amount total by fiscal year.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: false)]
    protected $amountFYear = 0.0;
    public function getAmountFYear() {
        return round($this->amountFYear, 5);
    }
    public function setAmountFYear($amountFYear) {
            $this->amountFYear = round($amountFYear, 5);
    }


}
