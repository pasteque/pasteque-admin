<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
/**
 * @package Pasteque
 */
#[ORM\Table(name: 'products_packagings')]
#[ORM\Entity]
class ProductPackaging extends Entity
{



    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }


    /**
     * @var integer
     */
    #[ORM\Column(name: 'product_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $productId;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'packaging_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $packagingId;

    /**
     * @var float
     */
    #[ORM\Column(name: 'weight', type: 'float', precision: 10, scale: 0, nullable: false)]
    private $weight;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'is_logistical', type: 'boolean', nullable: true)]
    private $isLogistical;


}

