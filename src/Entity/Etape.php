<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Etape
 */
#[ORM\Table(name: 'etapes')]
#[ORM\Entity]
class Etape extends Entity
{


    protected function getDirectFieldNames() {
        return ['id', 'nom', 'numero','date_evt'];
    }
    protected function getAssociationFields() {
        return [

        ];
    }


    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'bigint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'numero', type: 'integer', nullable: false)]
    protected $numero;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', nullable: false)]
    protected $nom;


    /**
     * @var string
     */
    #[ORM\Column(name: 'descriptif', type: 'string', nullable: true)]
    protected $descriptif ='';


    /**
     * @var datetime
     */
    #[ORM\Column(name: 'date_evt', type: 'datetime', nullable: false)]
    protected $date_evt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getNumero(): int
    {
        return $this->numero;
    }

    /**
     * @param int $numero
     */
    public function setNumero(int $numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getDescriptif(): string
    {
        return $this->descriptif;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif(string $descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @return \Datetime
     */
    public function getDateEvt(): \Datetime
    {
        return $this->date_evt;
    }

    /**
     * @param \Datetime $date_evt
     */
    public function setDateEvt(\Datetime $date_evt)
    {
        $this->date_evt = $date_evt;
    }




}

