<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package Pasteque
 */
#[ORM\Table(name: 'users')]
#[ORM\Entity]
class User extends Entity
{
    protected function getDirectFieldNames() {
        return ['id', 'name', 'password', 'active', 'hasImage', 'card'];
    }
    protected function getAssociationFields() {
        return [
                [
                 'name' => 'role',
                 'class' => \App\Entity\Role::class
                 ]
                ];
    }

    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $name;
    public function getName() { return $this->name; }
    public function setName($name) { $this->name = $name; }

    /** User password.
     * @var string
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected $password;
    public function getPassword() { return $this->password; }
    /** Low-level password set.
     * use rAPI->setPassword instead to add encryption. */
    public function setPassword($password) { $this->password = $password; }

    /**
     * ID of the assigned role
     * @var integer
     */
    #[ORM\JoinColumn(name: 'role_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: \Role::class)]
    protected $role;
    public function getRole() { return $this->role; }
    public function setRole($role) { $this->role = $role; }

    /**
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $active = true;
    public function getActive() { return $this->active; }
    public function isActive() { return $this->getActive(); }
    public function setActive($active) { $this->active = $active; }

    /**
     * True if an image can be found for this model.
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $hasImage = false;
    public function getHasImage() { return $this->hasImage; }
    public function hasImage() { return $this->getHasImage(); }
    public function setHasImage($hasImage) { $this->hasImage = $hasImage; }

    /**
     * Code of User's card
     * @var string
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected $card;
    public function getCard() { return $this->card; }
    public function setCard($card) { $this->card = $card; }

    public function authenticate($password) {
        if (str_starts_with((string) $password, 'empty:')) {
            // Empty is always empty
            $password = '';
        } else if (str_starts_with((string) $password, 'plain:')) {
            // Remove no-encryption prefix not to care about it later
            $password = substr((string) $password, 6);
        }
        $currPwd = $this->getPassword();
        if ($currPwd === null || $currPwd == ""
                || str_starts_with((string) $currPwd, "empty:")) {
            // No password
            return true;
        } else if (str_starts_with((string) $currPwd, "sha1:")) {
            // SHA1 encryption
            if (str_starts_with((string) $password, 'sha1:')) {
                $hash = $password;
            } else {
                $hash = 'sha1:' . sha1((string) $password);
            }
            return ($currPwd == $hash);
        } else if (str_starts_with((string) $currPwd, "plain:")) {
            // Clear password (legacy)
            return ($currPwd == "plain:" . $password);
        } else {
            // Default clear password (legacy)
            return ($currPwd == $password);
        }
    }
}


class_alias(User::class, '\App\Entity\Utilisateur');
