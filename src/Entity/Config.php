<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Config
 * @package Pasteque
 */
#[ORM\Table(name: 'configs')]
#[ORM\Entity]
class Config extends Entity
{
    use TimestampableEntity;

    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [

        ];
    }

    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * @var string
     */
    #[ORM\Column(type: 'text', length: 65535, nullable: true)]
    protected $observation;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $name;
    public function getName() { return $this->name; }
    public function setName($name) { $this->name = $name; }


    /**
     * @var string
     */
    #[ORM\Column(type: 'text')]
    protected $variables;
    public function getVariables() { return json_decode($this->variables,true); }
    public function setVariables($variables) { $this->variables = json_encode($variables); }

    /**
     * @return string
     */
    public function getObservation(): string
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation(string $observation)
    {
        $this->observation = $observation;
    }


}
