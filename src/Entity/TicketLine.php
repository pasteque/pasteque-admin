<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class TicketLine. This class is for fast data analysis only.
 * For declarations see FiscalTicket.
 * @package Pasteque
 */
#[ORM\Table(name: 'ticketlines')]
#[ORM\Entity]
class TicketLine extends Entity // Embedded class
{
    public function getDirectFieldNames() {
        return ['dispOrder', 'productLabel',
                'unitPrice', 'taxedUnitPrice',
                'quantity', 'price', 'taxedPrice',
                'taxRate',
                'discountRate', 'finalPrice', 'finalTaxedPrice' ];
    }
    public function getAssociationFields() {
        return [
                [
                 'name' => 'ticket',
                 'class' => \App\Entity\Ticket::class,
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'product',
                 'class' => \App\Entity\Product::class,
                 'null' => true
                 ],
                [
                 'name' => 'tax',
                 'class' => \App\Entity\Tax::class,
                 ]
                ];
    }
    public function getId() {
        return ['ticket' => $this->getTicket()->getId(), 'dispOrder' => $this->getDispOrder()];
    }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'ticket_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Ticket::class, inversedBy: 'lines')]
    #[ORM\Id]
    protected $ticket;
    public function getTicket() { return $this->ticket; }
    public function setTicket($ticket) { $this->ticket = $ticket; }

    /**
     * Display order or number of the line
     * @var integer
     */
    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    protected $dispOrder;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }

    /**
     * Id of the product
     * @var integer
     */
    #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: Product::class)]
    protected $product;
    public function getProduct() { return $this->product; }
    /** Set the product. If productLabel is null, it will be set with
     * the label of the product. */
    public function setProduct($product) {
        $this->product = $product;
        if ($this->getProductLabel() === null) {
            $this->setProductLabel($product->getLabel());
        }
    }

    /**
     * Label of product at the ticket time (or when product is null)
     * @var string null
     */
    #[ORM\Column(type: 'string')]
    protected $productLabel;
    public function getProductLabel() { return $this->productLabel; }
    public function setProductLabel($productLabel) {
        $this->productLabel = $productLabel;
    }

    /**
     * Unit price without tax and before applying discount.
     * It is null when taxedUnitPrice is set.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $unitPrice = null;
    public function getUnitPrice() {
        if ($this->unitPrice === null) { return null; }
        else { return round($this->unitPrice, 5); }
    }
    public function setUnitPrice($unitPrice) {
        if ($unitPrice === null) { $this->unitPrice = null; }
        else { $this->unitPrice = round($unitPrice, 5); }
    }

/**
     * Unit price with tax and before applying discount.
     * It is null when unitPrice is set.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $taxedUnitPrice = null;
    public function getTaxedUnitPrice() {
        if ($this->taxedUnitPrice === null) { return null; }
        else { return round($this->taxedUnitPrice, 5); }
    }
    public function setTaxedUnitPrice($taxedUnitPrice) {
        if ($taxedUnitPrice === null) { $this->taxedUnitPrice = null; }
        else { $this->taxedUnitPrice = round($taxedUnitPrice, 5); }
    }

    /**
     * Quantity of product on this ticket line
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $quantity = 1.0;
    public function getQuantity() { return round($this->quantity, 5); }
    public function setQuantity($quantity) {
        $this->quantity = round($quantity, 5);
    }

    /**
     * In quantity price without tax and before applying discount.
     * It is null when taxedPrice is set.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $price = null;
    public function getPrice() {
        if ($this->price === null) { return null; }
        else { return round($this->price, 2); }
    }
    public function setPrice($price) {
        if ($price === null) { $this->price = null; }
        else { $this->price = round($price, 2); }
    }

    /**
     * In quantity price with tax and before applying discount.
     * It is null when price is set.
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $taxedPrice = null;
    public function getTaxedPrice() {
        if ($this->taxedPrice === null) { return null; }
        else { return round($this->taxedPrice, 2); }
    }
    public function setTaxedPrice($taxedPrice) {
        if ($taxedPrice === null) { $this->taxedPrice = null; }
        else { $this->taxedPrice = round($taxedPrice, 2); }
    }

    /**
     * Id of taxe on the ticket line
     * @var float
     */
    #[ORM\JoinColumn(name: 'tax_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Tax::class)]
    protected $tax;
    public function getTax() { return $this->tax; }
    public function setTax($tax) { $this->tax = $tax; }

    /**
     * Rate of the tax at the time of the ticket
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $taxRate;
    public function getTaxRate() { return $this->taxRate; }
    public function setTaxRate($taxRate) { $this->taxRate = $taxRate; }

    /**
     * Rate of discount on this ticket line
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $discountRate = 0.0;
    public function getDiscountRate() { return $this->discountRate; }
    public function setDiscountRate($discountRate) { $this->discountRate = $discountRate; }

    /**
     * Total price without taxes with discount rate.
     * It is null when finalTaxedPrice is set.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $finalPrice = null;
    public function getFinalPrice() {
        if ($this->finalPrice === null) { return null; }
        else { return round($this->finalPrice, 2); }
    }
    public function setFinalPrice($finalPrice) {
        if ($finalPrice === null) { $this->finalPrice = null; }
        else { $this->finalPrice = round($finalPrice, 2); }
    }

    /**
     * Total price with taxes and discount rate.
     * It is null when finalPrice is set.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $finalTaxedPrice = null;
    public function getFinalTaxedPrice() {
        if ($this->finalTaxedPrice === null) { return null; }
        else { return round($this->finalTaxedPrice, 2); }
    }
    public function setFinalTaxedPrice($finalTaxedPrice) {
        if ($finalTaxedPrice === null) { $this->finalTaxedPrice = null; }
        else { $this->finalTaxedPrice = round($finalTaxedPrice, 2); }
    }

}
