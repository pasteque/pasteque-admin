<?php



namespace App\Entity;

use Declic3000\Pelican\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Attribute\Ignore;

/**
 * Motgroupe
 */
#[ORM\Table(name: 'motgroupes')]
#[ORM\Entity]
class Motgroupe extends Entity
{
    use TimestampableEntity;
    /**
     * @var integer
     */
    #[ORM\Column(name: 'id_motgroupe', type: 'bigint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $idMotgroupe;

    
    
    #[ORM\OneToMany(targetEntity: \Motgroupe::class, mappedBy: 'parent')]
    protected $children;
    
    
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id_motgroupe')]
    #[ORM\ManyToOne(targetEntity: \Motgroupe::class, inversedBy: 'children')]
    protected $parent;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', length: 80, nullable: false)]
    protected $nom;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nomcourt', type: 'string', length: 12, nullable: true)]
    protected $nomcourt = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'descriptif', type: 'string', length: 255, nullable: true)]
    protected $descriptif;

    /**
     * @var string
     */
    #[ORM\Column(name: 'texte', type: 'text', nullable: true)]
    protected $texte;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'importance', type: 'integer', nullable: false)]
    protected $importance = '1';

    /**
     * @var string
     */
    #[ORM\Column(name: 'objets_en_lien', type: 'text', nullable: false)]
    protected $objetsEnLien;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'systeme', type: 'boolean', nullable: true)]
    protected $systeme = false;

    /**
     * @var boolean
     */
    #[ORM\Column(name: 'actif', type: 'boolean', nullable: false)]
    protected $actif = true;

    /**
     * @var string
     */
    #[ORM\Column(name: 'options', type: 'text', nullable: true)]
    protected $options;
    
    
    #[ORM\OneToMany(targetEntity: \Mot::class, mappedBy: 'motgroupe', cascade: ['all'])]
    #[Ignore]
    protected $mots;

    
    
    /**
     * @return number
     */
    public function getIdMotgroupe()
    {
        return $this->idMotgroupe;
    }

    /**
     * @return mixed
     */
    public function getParent():?Motgroupe
    {
        return $this->parent;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @return string
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * @return number
     */
    public function getImportance()
    {
        return $this->importance;
    }

    /**
     * @return string
     */
    public function getObjetsEnLien()
    {
        return $this->objetsEnLien;
    }

    /**
     * @return boolean
     */
    public function isSysteme()
    {
        return $this->systeme;
    }

    /**
     * @return number
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return mixed
     */
    public function getMots()
    {
        return $this->mots;
    }





    /**
     * @param number $idMotgroupe
     */
    public function setIdMotgroupe($idMotgroupe)
    {
        $this->idMotgroupe = $idMotgroupe;
    }

    /**
     * @param ?Motgroupe $parent
     */
    public function setParent(?Motgroupe $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $nomcourt
     */
    public function setNomcourt($nomcourt)
    {
        $this->nomcourt = $nomcourt;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @param string $texte
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;
    }

    /**
     * @param number $importance
     */
    public function setImportance($importance)
    {
        $this->importance = $importance;
    }

    /**
     * @param string $objetsEnLien
     */
    public function setObjetsEnLien($objetsEnLien)
    {
        $this->objetsEnLien = $objetsEnLien;
    }

    /**
     * @param boolean $systeme
     */
    public function setSysteme($systeme)
    {
        $this->systeme = $systeme;
    }

    /**
     * @param number $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }

    /**
     * @param string $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    public function setMots(mixed $mots)
    {
        $this->mots = $mots;
    }





}

