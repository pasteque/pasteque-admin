<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Class CompositionGroup
 * @package Pasteque
 */
#[ORM\Table(name: 'compositiongroups')]
#[ORM\Entity]
class CompositionGroup extends Entity
{
    protected function getDirectFieldNames() {
        return ['id', 'label', 'dispOrder'];
    }
    protected function getAssociationFields() {
        return [
                [
                 'name' => 'product',
                 'class' => \App\Entity\Product::class,
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'compositionProducts',
                 'class' => \App\Entity\CompositionProduct::class,
                 'array' => true,
                 'embedded' => true,
                 ]
                ];
    }

    public function __construct() {
        $this->compositionProducts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Internal Id.
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'compositionGroups')]
    protected $product;
    public function getProduct() { return $this->product; }
    public function setProduct($product) { $this->product = $product; }

    /**
     * Name of the composition group
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) { $this->label = $label; }

    /**
     * Order of display
     * @var int order
     */
    #[ORM\Column(type: 'integer', name: 'disp_order')]
    protected $dispOrder = 0;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }

    /**
     * Product image if any.
     * @var binary
     */
    #[ORM\Column(type: 'blob', nullable: true)]
    protected $image;
    public function getImage() { return $this->image; }
    public function setImage($image) { $this->image = $image; }

    #[ORM\OneToMany(targetEntity: CompositionProduct::class, mappedBy: 'compositionGroup', cascade: ['persist'], orphanRemoval: true)]
    protected $compositionProducts;
    public function getCompositionProducts() { return $this->compositionProducts; }
    public function setCompositionProducts($compositionProducts) {
        $this->compositionProducts->clear();
        foreach ($compositionProducts as $prd) {
            $this->addCompositionProducts($prd);
        }
    }
    public function clearCompositionProducts() {
        $this->getCompositionProducts()->clear();
    }
    public function addCompositionProducts($compositionProduct) {
        $this->compositionProducts->add($compositionProduct);
        $compositionProduct->setCompositionGroup($this);
    }

}
