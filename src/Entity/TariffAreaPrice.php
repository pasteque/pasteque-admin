<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class TariffAreaPrice
 * @package Pasteque
 */
#[ORM\Table(name: 'tariffareaprices')]
#[ORM\Entity]
class TariffAreaPrice extends Entity // Embedded class
{
    protected function getDirectFieldNames() {
        return ['price'];
    }
    protected function getAssociationFields() {
        return [
                [
                 'name' => 'tariffArea',
                 'class' => \App\Entity\TariffArea::class,
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'product',
                 'class' => \App\Entity\Product::class,
                 ],
                [
                 'name' => 'tax',
                 'class' => \App\Entity\Tax::class,
                 'null' => true // when price is set, see isUsefull()
                 ]
                ];
    }
    public function getId() {
        if ($this->getTariffArea() === null) { return null; }
        return ['tariffArea' => $this->getTariffArea()->getId(), 'product' =>$this->getProduct()->getId()];
    }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'tariffarea_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: TariffArea::class, inversedBy: 'prices')]
    protected $tariffArea;
    public function getTariffArea() { return $this->tariffArea; }
    public function setTariffArea($tariffArea) { $this->tariffArea = $tariffArea; }

    /**
     * @var integer
     */
    #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Product::class)]
    #[ORM\Id]
    protected $product;
    public function getProduct() { return $this->product; }
    public function setProduct($product) { $this->product = $product; }

    /** New sell price without taxes if changed. Null if it is as the original.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $price = null;
    public function getPrice() {
        return ($this->price === null) ? null : round($this->price, 5);
    }
    public function setPrice($price) {
        $this->price = ($price === null) ? null : round($price, 5);
    }

    /**
     * New tax if changed. Null if it is as the original.
     * @var integer
     */
    #[ORM\JoinColumn(name: 'tax_id', referencedColumnName: 'id', nullable: true)]
    #[ORM\ManyToOne(targetEntity: Tax::class)]
    protected $tax;
    public function getTax() { return $this->tax; }
    public function setTax($tax) { $this->tax = $tax; }

    /** Check if the price holds usefull data:
     * either an alternative price, tax or both. */
    public function isUsefull() {
        return ($this->getTax() !== null || $this->getPrice() !== null);
    }

}
