<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StatSales
 */
#[ORM\Table(name: 'stat_sales')]
#[ORM\Index(name: 'ID_PRODUCT', columns: ['product_id', 'date'])]
#[ORM\Index(name: 'DATE', columns: ['date'])]
#[ORM\Entity]
class StatSales extends Entity
{


    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }




    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'bigint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'product_id', type: 'integer', nullable: false)]
    private $productId;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'date', type: 'date', nullable: false)]
    private $date;

    /**
     * @var float
     */
    #[ORM\Column(name: 'quantity', type: 'float', precision: 10, scale: 0, nullable: false)]
    private $quantity;

    /**
     * @var float
     */
    #[ORM\Column(name: 'sales', type: 'float', precision: 10, scale: 0, nullable: false)]
    private $sales;

    /**
     * @var float
     */
    #[ORM\Column(name: 'margin', type: 'float', precision: 10, scale: 0, nullable: false)]
    private $margin;

    /**
     * @var float
     */
    #[ORM\Column(name: 'price_buy', type: 'float', precision: 10, scale: 0, nullable: false)]
    private $priceBuy;

    /**
     * @var float
     */
    #[ORM\Column(name: 'price_sell', type: 'float', precision: 10, scale: 0, nullable: false)]
    private $priceSell;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'tax_id', type: 'integer', nullable: false)]
    private $taxId;


}

