<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Ignore;

/**
 * Supplier
 * @package Pasteque
 */
#[ORM\Table(name: 'suppliers')]
#[ORM\Index(name: 'name', columns: ['name'])]
#[ORM\Entity]
class Supplier extends Entity
{


    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }





    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'bigint', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 120, nullable: true)]
    private $name;

    /**
     * @var string
     */
    #[ORM\Column(name: 'address1', type: 'string', length: 80, nullable: true)]
    private $address1 = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'address2', type: 'string', length: 80, nullable: true)]
    private $address2 = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'town', type: 'string', length: 80, nullable: true)]
    private $town = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'state', type: 'string', length: 80, nullable: true)]
    private $state = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'country', type: 'string', length: 80, nullable: true)]
    private $country = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'zipcode', type: 'string', length: 80, nullable: true)]
    private $zipcode = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'website', type: 'string', length: 64, nullable: true)]
    private $website = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'phone', type: 'string', length: 250, nullable: true)]
    private $phone = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'cellphone', type: 'string', length: 20, nullable: true)]
    private $cellphone = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'fax', type: 'string', length: 20, nullable: true)]
    private $fax = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'email', type: 'string', length: 250, nullable: true)]
    private $email = '';

    /**
     * @var string
     */
    #[ORM\Column(name: 'contact', type: 'text', length: 65535, nullable: true)]
    private $contact;

    /**
     * @var string
     */
    #[ORM\Column(name: 'comment', type: 'text', length: 65535, nullable: true)]
    private $comment;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'is_direct', type: 'boolean', nullable: false)]
    private $is_direct = false;


    /**
     * @var bool
     */
    #[ORM\Column(name: 'active', type: 'boolean', nullable: false)]
    private $active = true;



    /**
     * @var string
     */
    #[ORM\Column(name: 'image', type: 'blob', length: 16777215, nullable: true)]
    private $image;


    /**
     * True if an image can be found for this model.
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $hasImage = false;

    /**
     * Array of tax totals. It holds the final tax base/amount for each
     * tax. That is after all discounts (lines and ticket).
     * @var Product[]
     */
    #[ORM\OneToMany(targetEntity: ProductSupplier::class, mappedBy: 'supplier', cascade: ['persist'], orphanRemoval: true, fetch: 'EXTRA_LAZY')]
    #[Ignore]
    protected $productSuppliers;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name??'';
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    /**
     * @param string $address1
     */
    public function setAddress1(?string $address1): void
    {
        $this->address1 = $address1;
    }

    /**
     * @return string
     */
    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    /**
     * @param string $address2
     */
    public function setAddress2(?string $address2): void
    {
        $this->address2 = $address2;
    }

    /**
     * @return string
     */
    public function getTown(): ?string
    {
        return $this->town??'';
    }

    /**
     * @param string $town
     */
    public function setTown(?string $town): void
    {
        $this->town = $town;
    }

    /**
     * @return string
     */
    public function getState(): ?string
    {
        return $this->state??'';
    }

    /**
     * @param string $state
     */
    public function setState(?string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry(): ?string
    {
        return $this->country??'';
    }

    /**
     * @param string $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getZipcode(): ?string
    {
        return $this->zipcode??'';
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode(?string $zipcode): void
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string
     */
    public function getWebsite(): ?string
    {
        return $this->website??'';
    }

    /**
     * @param string $website
     */
    public function setWebsite(?string $website): void
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone??'';
    }

    /**
     * @param string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getCellphone(): ?string
    {
        return $this->cellphone??'';
    }

    /**
     * @param string $cellphone
     */
    public function setCellphone(?string $cellphone): void
    {
        $this->cellphone = $cellphone;
    }

    /**
     * @return string
     */
    public function getFax(): ?string
    {
        return $this->fax??'';
    }

    /**
     * @param string $fax
     */
    public function setFax(?string $fax): void
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email??'';
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getContact(): ?string
    {
        return $this->contact??'';
    }

    /**
     * @param string $contact
     */
    public function setContact(?string $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }


    public function getHasImage() { return $this->hasImage; }
    public function hasImage() { return $this->getHasImage(); }
    public function setHasImage($hasImage) { $this->hasImage = $hasImage; }

    public function isIsDirect(): bool
    {
        return $this->is_direct;
    }

    public function setIsDirect(bool $is_direct): void
    {
        $this->is_direct = $is_direct;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }




}

