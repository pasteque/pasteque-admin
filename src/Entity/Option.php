<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Option. Non-structured general options.
 * @package Pasteque
 */
#[ORM\Table(name: 'options')]
#[ORM\Entity]
class Option extends Entity
{

    protected function getDirectFieldNames() {
        return [];
    }



    protected function getAssociationFields() {
        return [ ];
    }

    /**
     * @var string
     */
    #[ORM\Id]
    #[ORM\Column(type: Types::STRING)]
    protected $name;
    public function getId() { return $this->name; }
    public function getName() { return $this->name; }
    public function setName($name) { $this->name = $name; }

    /**
     * Is the option a system-option. A system option requires its
     * dedicated API to be updated, if there is any.
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $system = true;
    public function getSystem() { return $this->system; }
    /** Alias for getSystem (the latter is required for Doctrine) */
    public function isSystem() { return $this->getSystem(); }
    public function setSystem($system) { $this->system = $system; }

    /**
     * Content. A single content or generally a JSON string.
     * @var string|null
     */
    #[ORM\Column(type: 'text')]
    protected $content = '';
    public function getContent() { return $this->content; }
    public function setContent($content) { $this->content = $content; }

}
