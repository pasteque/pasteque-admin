<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 *
 * Class Discount
 * @package Pasteque
 */
#[ORM\Table(name: 'discounts')]
#[ORM\Entity]
class Discount extends Entity
{
    protected function getDirectFieldNames() {
        return ['id', 'label', 'startDate', 'endDate', 'rate', 'barcode',
                'barcodeType', 'dispOrder'];
    }
    protected function getAssociationFields() {
        return [];
    }

    /**
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * Start date
     * @var string|null
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    protected $startDate;
    public function getStartDate() { return $this->startDate; }
    public function setStartDate($startDate) { $this->startDate = $startDate; }

    /**
     * End date
     * @var string|null
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    protected $endDate;
    public function getEndDate() { return $this->endDate; }
    public function setEndDate($endDate) { $this->endDate = $endDate; }

    /**
     * Rate of discount
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $rate;
    public function getRate() { return $this->rate; }
    public function setRate($rate) { $this->rate = $rate; }

    /**
     * @var string
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected $barcode;
    public function getBarcode() { return $this->barcode; }
    public function setBarcode($barcode) { $this->barcode = $barcode; }

    /**
     * @var integer
     */
    #[ORM\Column(type: 'integer')]
    protected $barcodeType = 0;
    public function getBarcodeType() { return $this->barcodeType; }
    public function setBarcodeType($type) { $this->barcodeType = $type; }

    /**
     * Order of display
     * @var int order
     */
    #[ORM\Column(type: 'integer', name: 'disp_order')]
    protected $dispOrder = 0;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }




}
