<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use \App\Entity\Category;
use \App\Entity\Tax;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Attribute\Ignore;

/**
 * Class Product
 * @package Pasteque
 */

#[ORM\Entity(repositoryClass: \App\Repository\ProductRepository::class)]
#[ORM\Table(name: 'products')]
#[ORM\Index(name: 'label', columns: ['label'])]
#[ORM\Index(name: 'barcode', columns: ['barcode'], flags: ['fulltext'])]
#[ORM\Index(name: 'search', columns: ['label', 'reference', 'barcode'], flags: ['fulltext'])]
#[ORM\Index(name: 'visible', columns: ['visible'])]
class Product extends Entity
{

    use TimestampableEntity;

    protected function getDirectFieldNames() {
        return ['id', 'reference', 'label', 'barcode', 'priceBuy', 'priceSell',
                'visible', 'scaled', 'scaleType', 'scaleValue', 'dispOrder',
                'discountEnabled', 'discountRate', 'prepay', 'composition',
                'hasImage'];
    }

    public static $allowedFields=['id', 'reference', 'label', 'barcode', 'priceBuy', 'priceSell',
                'visible', 'scaled', 'scaleType', 'scaleValue', 'dispOrder',
                'discountEnabled', 'discountRate', 'prepay', 'composition',
                'hasImage','taxedPrice'];

    protected function getAssociationFields() {
        return [
                [
                 'name' => 'category',
                 'class' => \App\Entity\Category::class
                 ],
                [
                 'name' => 'tax',
                 'class' => \App\Entity\Tax::class
                 ],
                [
                 'name' => 'compositionGroups',
                 'class' => \App\Entity\CompositionGroup::class,
                 'array' => true,
                 'embedded' => true,
                 ]
                ];
    }

    public function __construct() {
        $this->compositionGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * ID of the product
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }
    public function setId(int $id) { $this->id = $id; }
    /**
     * Code of the product, user-friendly ID.
     * It is automatically set from label if not explicitely set.
     * @var string
     */
    #[ORM\Column(type: 'string', unique: true)]
    protected $reference;
    public function getReference() { return $this->reference; }
    public function setReference($reference) { $this->reference = $reference; }

    /**
     * barcode of a product
     * @var string
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected $barcode;
    public function getBarcode() { return $this->barcode; }
    public function setBarcode($barcode) { $this->barcode = $barcode; }

    /**
     * name of a product
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) {
        $this->label = $label;
    }

    /**
     * Buy price without taxes, used for estimated margin computation.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $priceBuy = null;
    public function getPriceBuy() { return round($this->priceBuy, 5); }
    public function setPriceBuy($priceBuy) { $this->priceBuy = round($priceBuy, 5); }

    /**
     * sell price without taxes.
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $priceSell;
    public function getPriceSell() { return round($this->priceSell, 5); }
    public function setPriceSell($price) { $this->priceSell = round($price, 5); }

    /**
     * Is product currently in sale (visible on cash registers) ?
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $visible = true;
    public function getVisible() { return $this->visible; }
    /** Alias for getVisible (the latter is required for Doctrine) */
    public function isVisible() { return $this->getVisible(); }
    public function setVisible($visible) { $this->visible = $visible; }

    /**
     * Is the product sold by scale?
     * isScale can be false with a SCALE_TYPE_WEIGHT (i.e. a box of 200g).
     * When isScale is true, scaleValue is meaningless.
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $scaled = false;
    public function getScaled() { return $this->scaled; }
    /** Alias for getScaled (the latter is required for Doctrine). */
    public function isScaled() { return $this->scaled; }
    public function setScaled($scaled) { $this->scaled = $scaled; }

    /** Constant for scaleType, product is atomical (mapped to 0). */
    const SCALE_TYPE_NONE = 0;
    /** Constant for scaleType, product is referenced by weight (mapped to 1). */
    const SCALE_TYPE_WEIGHT = 1;
    /** Constant for scaleType, product is referenced by volume (mapped to 2). */
    const SCALE_TYPE_VOLUME = 2;
    /**
     * See SCALE_TYPE_* constants.
     * Used to compute reference prices like price per liter or kg.
     * @var int
     */
    #[ORM\Column(type: 'smallint')]
    protected $scaleType = Product::SCALE_TYPE_NONE;
    public function getScaleType() { return $this->scaleType; }
    public function setScaleType($type) {
        if ($type != Product::SCALE_TYPE_NONE
            && $type != Product::SCALE_TYPE_WEIGHT
            && $type != Product::SCALE_TYPE_VOLUME) {
            throw new \InvalidArgumentException('Unknown scaleType');
        }
        $this->scaleType = $type;
    }

    /**
     * The scale value for products referenced by weight or volume and
     * not soled by scale.
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $scaleValue = 1.0;
    public function getScaleValue() { return round($this->scaleValue, 5); }
    public function setScaleValue($value) { $this->scaleValue = round($value, 5); }

    /**
     * ID of the category
     * @var Category
     */
    #[ORM\JoinColumn(name: 'category_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Category::class)]
    protected $category;
    public function getCategory() { return $this->category; }
    public function setCategory(Category $category) { $this->category = $category; }

    /**
     * Order of display inside it's category
     * @var int order
     */
    #[ORM\Column(type: 'integer', name: 'disp_order')]
    protected $dispOrder = 0;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }

    /**
     * ID of a tax
     * @var \App\Entity\Tax
     */
    #[ORM\JoinColumn(name: 'tax_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Tax::class)]
    protected $tax;
    public function getTax() { return $this->tax; }
    public function setTax($tax) { $this->tax = $tax; }

    /**
     * True if an image can be found for this model.
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $hasImage = false;
    public function getHasImage() { return $this->hasImage; }
    public function hasImage() { return $this->getHasImage(); }
    public function setHasImage($hasImage) { $this->hasImage = $hasImage; }

    /**
     * Is discount currently enabled ?
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $discountEnabled = false;
    public function getDiscountEnabled() { return $this->discountEnabled; }
    /** Alias for getDiscountEnabled (the latter is required for Doctrine). */
    public function isDiscountEnabled() { return $this->getDiscountenabled(); }
    public function setDiscountEnabled($discountEnabled) { $this->discountEnabled = $discountEnabled; }

    /**
     * rate of the discount
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $discountRate = 0.0;
    public function getDiscountRate() { return $this->discountRate; }
    public function setDiscountRate($rate) { $this->discountRate = $rate; }

    /**
     * Is product a prepayment refill?
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $prepay = false;
    public function getPrepay() { return $this->prepay; }
    public function isPrepay() { return $this->getPrepay(); }
    public function setPrepay($prepay) { $this->prepay = $prepay; }

    /**
     * Is product a composition?
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected $composition = false;
    public function getComposition() { return $this->composition; }
    public function isComposition() { return $this->getComposition(); }
    public function setComposition($composition) { $this->composition = $composition; }

    #[ORM\OneToMany(targetEntity: CompositionGroup::class, mappedBy: 'product', cascade: ['persist'], orphanRemoval: true)]
    protected $compositionGroups;
    public function getcompositionGroups() { return $this->compositionGroups; }
    public function setCompositionGroups($compositionGroups) {
        $this->compositionGroups->clear();
        foreach ($compositionGroups as $compositionGroup) {
            $this->addCompositionGroup($compositionGroup);
        }
    }


    #[ORM\OneToOne(targetEntity: ProductExtra::class, mappedBy: 'product', orphanRemoval: true, fetch: 'EAGER')]
    protected $productExtra;



    public function clearCompositionGroups() {
        $this->getCompositionGroups()->clear();
    }
    public function addCompositionGroup($compositionGroup) {
        $this->compositionGroups->add($compositionGroup);
        $compositionGroup->setProduct($this);
    }

    /** Set sell price with tax. */
    public function setTaxedPrice($taxedPrice) {
        $tax = $this->getTax();
        $this->setPriceSell($taxedPrice / (1 + $tax->getRate()));
    }

    /** Get sell price with tax.
     * Virtual field passed along toStruct at current time.*/
    public function getTaxedPrice() {
        $tax = $this->getTax();
        $price = $this->getPriceSell();
        return round($price * (1 + $tax->getRate()), 2);
    }

    public function getTaxValue() {
        return round($this->getTaxedPrice() - $this->getPriceSell(), 2);
    }



    function getMargin() {
        if (!empty($this->priceBuy) ) {
            return round($this->priceSell,2) - $this->priceBuy;
        } else {
            return 0;
        }
    }

    function getMarginRate() {
        if (!empty($this->priceBuy)&& round($this->priceSell,2)>0 ) {
            return round(($this->getMargin()/ round($this->priceSell,2))*100,2);
        } else {
            return 0;
        }
    }

    function getTaxedPriceKg() {

        if ($this->getScaleValue()>0 && $this->getScaleType()>0 ){
            if (!empty($this->priceBuy) ) {
                $coeff=1;
                if ($this->getScaleType()==1){
                    $coeff=1000;
                }
                return round($this->getTaxedPrice()*$coeff/$this->getScaleValue(),2);
            } else {
                return 0;
            }
        }
        return false;
    }


    #[ORM\OneToMany(targetEntity: ProductSupplier::class, mappedBy: 'product', cascade: ['persist'], orphanRemoval: true)]
    protected $productSuppliers;
    public function getProductSuppliers() { return $this->productSuppliers; }
    public function setProductSuppliers($productSuppliers) {
        $this->productSuppliers->clear();
        foreach ($productSuppliers as $productSupplier) {
            $this->addProductSupplier($productSupplier);
        }
    }
    public function clearProductSuppliers() {
        $this->getProductSuppliers()->clear();
    }
    public function addProductSupplier($productSupplier) {
        $this->productSuppliers->add($productSupplier);
        $productSupplier->setProduct($this);
    }

    /**
     * @return null|ProductExtra
     */
    public function getProductExtra()
    {
        return $this->productExtra;
    }

    /**
     * @param null|ProductExtra
     */
    public function setProductExtra(?ProductExtra $productExtra): void
    {
        $this->productExtra = $productExtra;
    }



    /**
     * Many Mots have Many Individus.
     */
    #[ORM\JoinTable(name: 'mots_products')]
    #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id', onDelete: 'cascade')]
    #[ORM\InverseJoinColumn(name: 'id_mot', referencedColumnName: 'id_mot', onDelete: 'cascade')]
    #[ORM\ManyToMany(targetEntity: Mot::class)]
    protected $mots;


    /**
     *
     */
    public function getMots()
    {

        return $this->mots;

    }


    /**
     *
     */
    public function removeMots()
    {
        if ($this->mots){
            $this->mots->clear();
        }

    }


    /**
     * @param Mot $mot
     */

    public function removeMot($mot)
    {
        if ($this->mots->contains($mot)) {
            $this->mots->removeElement($mot);
        }
    }


    /**
     * @param Mot $mot
     */
    public function addMot(Mot $mot)
    {
        if (!$this->mots->contains($mot)) {
            $this->mots->add($mot);
        }
    }


    public function toStruct() {
        $struct = parent::toStruct();
        $struct['taxedPrice'] = $this->getTaxedPrice();
        $struct['taxValue'] = $this->getTaxValue();
        if ($this->isPrepay()) {
            // Remove irrelevent fields
            unset($struct['scaleType']);
            unset($struct['scaleValue']);
            unset($struct['compositionGroups']);
            // Embed prepay value to be able to apply discounts on it.
            $struct['prepayValue'] = $this->getPriceSell();
        } elseif ($this->isComposition()) {
            // Disable irrelevent fields
            unset($struct['scaled']);
            unset($struct['scaleType']);
            unset($struct['scaleValue']);
            unset($struct['prepay']);
        } else {
            unset($struct['compositionGroups']);
        }
        return $struct;
    }




    public function toArray():array
    {
        $vars = parent::toArray();
        $vars = array_intersect_key($vars,array_flip($this->getDirectFieldNames()));
        $vars['category_id'] = $this->getCategory()->getId();
        $vars['tax_id'] = $this->getTax()->getId();
        $vars['priceSellvat'] = $this->getTaxedPrice();
        $vars['margin'] = $this->getMargin();
        return $vars;

    }
}
