<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class TariffArea.
 * Outside Doctrine, TariffAreaPrices are included directly in struct
 * and not only the reference by their IDs.
 * That means the TariffAreaPrices class doesn't exists for toStruct
 * and fromStruct (it's just an array of data).
 * I.e. prices will appear like
 * "prices": [{"product": <id>, "tax": <id>, "price": <price>}]
 * @package Pasteque
 */
#[ORM\Table(name: 'tariffareas')]
#[ORM\Entity]
class TariffArea extends Entity
{
    protected function getDirectFieldNames() {
        return ['id', 'reference', 'label', 'dispOrder'];
    }
    protected function getAssociationFields() {
        return [
                [
                 'name' => 'prices',
                 'class' => \App\Entity\TariffAreaPrice::class,
                 'array' => true,
                 'embedded' => true
                 ],
                ];
    }

    public function __construct() {
        $this->prices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * ID of the area
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * Code of the area, user-friendly ID.
     * It is automatically set from label if not explicitely set.
     * @var string
     */
    #[ORM\Column(type: 'string', unique: true)]
    protected $reference;
    public function getReference() { return $this->reference; }
    public function setReference($reference) { $this->reference = $reference; }


    /**
     * name of the area
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) {
        $this->label = $label;
        if ($this->getReference() === null) {
            $this->setReference($label);
        }
    }

    /**
     * Order of display inside it's category
     * @var int order
     */
    #[ORM\Column(type: 'integer', name: 'disp_order')]
    protected $dispOrder = 0;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }

    #[ORM\OneToMany(targetEntity: TariffAreaPrice::class, mappedBy: 'tariffArea', cascade: ['persist'], orphanRemoval: true)]
    protected $prices;
    public function getPrices() { return $this->prices; }
    public function setPrices($prices) {
        $this->prices->clear();
        foreach ($prices as $price) {
            $this->addPrice($price);
        }
    }
    public function clearPrices() {
        $this->getPrices()->clear();
    }
    /** Add a TariffAreaPrice if not useless. */
    public function addPrice($price) {
        if (!$price->isUsefull()) { return; }
        $this->prices->add($price);
        $price->setTariffArea($this);
    }

}
