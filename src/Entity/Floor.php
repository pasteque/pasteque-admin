<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Floor
 * @package Pasteque
 */
#[ORM\Table(name: 'floors')]
#[ORM\Entity]
class Floor extends Entity
{
    protected function getDirectFieldNames() {
        return ['id', 'label', 'dispOrder'];
    }
    protected function getAssociationFields() {
        return [
                [
                 'name' => 'places',
                 'class' => '\Pasteque\Api\Model\Place',
                 'array' => true,
                 'embedded' => true
                 ],
                ];
    }

    public function __construct() {
        $this->places = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * ID of the floor
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * Label of the floor
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) { $this->label = $label; }

    /**
     * Floor image if any.
     * @var binary
     */
    #[ORM\Column(type: 'blob', nullable: true)]
    protected $image;
    public function getImage() { return $this->image; }
    public function setImage($image) { $this->image = $image; }

    /**
     * Order of display
     * @var int order
     */
    #[ORM\Column(type: 'integer', name: 'disp_order')]
    protected $dispOrder = 0;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }

    #[ORM\OneToMany(targetEntity: Place::class, mappedBy: 'floor', cascade: ['persist'], orphanRemoval: true)]
    protected $places;
    public function getPlaces() { return $this->places; }
    public function setPlaces($places) {
        $this->places->clear();
        foreach ($places as $place) {
            $this->addPlace($place);
        }
    }
    public function clearPlaces() {
        $this->getPlaces()->clear();
    }
    public function addPlace($place) {
        $this->places->add($place);
        $place->setFloor($this);
    }

}
