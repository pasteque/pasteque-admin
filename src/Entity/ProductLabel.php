<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
/**
 * @package Pasteque
 */
#[ORM\Table(name: 'products_labels')]
#[ORM\Entity]
class ProductLabel extends Entity
{

    protected function getDirectFieldNames() {
        return ['id', 'name', 'variables'];
    }
    protected function getAssociationFields() {
        return [
            [
                'name' => 'user',
                'class' => \App\Entity\User::class
            ]
        ];
    }

    /**
     * @var integer
     */
    #[ORM\Column(name: 'product_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $productId;

    /**
     * @var integer
     */
    #[ORM\Column(name: 'label_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private $labelId;


}

