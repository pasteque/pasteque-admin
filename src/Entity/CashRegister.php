<?php
//    Pastèque API
//
//    Copyright (C) 2017 Pastèque Contributors
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CashRegister
 * @package Pasteque
 */
#[ORM\Table(name: 'cashregisters')]
#[ORM\Entity]
class CashRegister extends Entity
{

    protected function getDirectFieldNames() {
        return ['id', 'reference', 'label'];
    }


    protected function getAssociationFields() {
        return [];
    }


    /**
     * ID of the cash register
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * Code of the cash register, user-friendly ID.
     * It is automatically set from label if not explicitely set.
     * @var string
     */
    #[ORM\Column(type: 'string', unique: true)]
    protected $reference;
    public function getReference() { return $this->reference; }
    public function setReference($reference) { $this->reference = $reference; }

    /**
     * name of the cash register
     * @var string
     */
    #[ORM\Column(type: 'string')]
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) {
        $this->label = $label;
        if ($this->getReference() === null) {
            $this->setReference($label);
        }
    }






}
