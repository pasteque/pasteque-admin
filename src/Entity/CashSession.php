<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use \App\API\VersionAPI;
use \App\System\DateUtils;
use \App\System\DAO\DAOCondition;
use Doctrine\ORM\Mapping as ORM;

/**
 * A cash session holds Z Ticket data until it's finalized.
 * Once the cash is closed, the final Z ticket is built.
 * Class CashSession
 * @package Pasteque
 */
#[ORM\Table(name: 'sessions')]
#[ORM\UniqueConstraint(name: 'session_index', columns: ['cashregister_id', 'sequence'])]
#[ORM\Entity]
class CashSession extends Entity
{
    /** Close type for a day close. */
    const CLOSE_SIMPLE = 0;
    /** Close type for a period close. */
    const CLOSE_PERIOD = 1;
    /** Close type for a fiscal year close. */
    const CLOSE_FYEAR = 2;

    protected function getDirectFieldNames() {
        return ['sequence', 'continuous', 'openDate', 'closeDate',
                'openCash', 'closeCash', 'expectedCash',
                'ticketCount', 'custCount',
                'cs', 'csPeriod', 'csFYear'];
        // 'closeType' (optional) imported on close but not exported nor stored.
    }
    protected function getAssociationFields() {
        return [
                [
                 'name' => 'cashRegister',
                 'class' => \App\Entity\CashRegister::class
                 ],
                [
                 'name' => 'payments',
                 'class' => \App\Entity\CashSessionPayment::class,
                 'array' => true,
                 'embedded' => true
                 ],
                [
                 'name' => 'taxes',
                 'class' => \App\Entity\CashSessionTax::class,
                 'array' => true,
                 'embedded' => true 
                 ],
                [
                 'name' => 'catSales',
                 'class' => \App\Entity\CashSessionCat::class,
                 'array' => true,
                 'embedded' => true
                 ],
                [
                 'name' => 'catTaxes',
                 'class' => \App\Entity\CashSessionCatTax::class,
                 'array' => true,
                 'embedded' => true
                 ],
                [
                 'name' => 'custBalances',
                 'class' => \App\Entity\CashSessionCustBalance::class,
                 'array' => true,
                 'embedded' => true
                 ]
                ];
    }

    public function __construct() {
        $this->taxes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->catSales = new \Doctrine\Common\Collections\ArrayCollection();
        $this->catTaxes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->custBalances = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Internal Id of the session. Required to link taxes and payments.
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * Id of a cash register
     * @var int
     */
    #[ORM\JoinColumn(name: 'cashregister_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: CashRegister::class)]
    protected $cashRegister;
    public function getCashRegister() { return $this->cashRegister; }
    public function setCashRegister($cashRegister) {
        $this->cashRegister = $cashRegister;
    }

    /**
     * Number of the session's cash register
     * @var int
     */
    #[ORM\Column(type: 'integer')]
    protected $sequence;
    public function getSequence() { return $this->sequence; }
    public function setSequence($sequence) { $this->sequence = $sequence; }

    /**
     * This is a client-side flag to checks when the cash is opened
     * if the previous cash is still in local cache.
     * It is not when the cache was deleted or when switching machine.
     * This should not happens frequently and is used to check for the
     * "disconnect/delete cache/restart" trick to delete the first tickets
     * silently.
     */
    #[ORM\Column(type: 'boolean')]
    protected $continuous = false;
    public function getContinuous() { return $this->continuous; }
    public function isContinuous() { return $this->getContinuous(); }
    public function setContinuous($continuous) {
        $this->continuous = $continuous;
    }

    /**
     * Open date (as a datetime) of session's cash register opening.
     * Read-only. Will throw an exception if trying to override it.
     * @var string|null
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    protected $openDate = null;
    public function getOpenDate() { return $this->openDate; }
    /** @throws \UnexpectedValueException When trying to override it. */
    public function setOpenDate($openDate) {
        if ($this->openDate === null) {
            $this->openDate = $openDate;
        } else if (!($this->openDate->getTimestamp()=== $openDate->getTimestamp())) {
            throw new \UnexpectedValueException('Open date is read only');
        }
    }

    /**
     * Close date (as a datetime) of session's cash register closure
     * Read-only. Will throw an exception if trying to override it.
     * @var string|null
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    protected $closeDate = null;
    public function getCloseDate() { return $this->closeDate; }
    /** @throws \UnexpectedValueException When trying to override it. */
    public function setCloseDate($closeDate) {
        if ($this->closeDate === null) {
            $this->closeDate = $closeDate;
        } else if ($this->closeDate->getTimestamp()!=  $closeDate->getTimestamp()) {
            throw new \UnexpectedValueException('Close date is read only');
        }
    }

    /**
     * Amount of cash at session's cash register opening
     * Read-only. Will throw an exception if trying to override it.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $openCash = null;
    public function getOpenCash() {
        if ($this->openCash === null) { return null; }
        else { return round($this->openCash, 5); }
    }
    /** @throws \UnexpectedValueException When trying to override it. */
    public function setOpenCash($openCash) {
        if ($this->openCash === null) {
            $this->openCash = ($openCash === null) ? null : round($openCash, 5);
        } else if (round($this->openCash, 5) != round($openCash, 5)) {
            throw new \UnexpectedValueException('Open cash is read only');
        }
    }

    /**
     * Amount of cash at session's cash register closing
     * Read-only. Will throw an exception if trying to override it.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $closeCash = null;
    public function getCloseCash() {
        if ($this->closeCash === null) { return null; }
        else { return round($this->closeCash, 5); }
    }
    /** @throws \UnexpectedValueException When trying to override it. */
    public function setCloseCash($closeCash) {
        if ($this->closeCash === null) {
            $this->closeCash = ($closeCash === null) ? null : round($closeCash, 5);
        } else if (round($this->closeCash, 5) != round($closeCash, 5)) {
            throw new \UnexpectedValueException('Close cash is read only');
        }
    }

    /**
     * Diffence's amount of cash at session's cash register closure.
     * Stored in database only for performance.
     * This field must computed automatically and stored on session close.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $expectedCash = null;
    public function getExpectedCash() {
        if ($this->expectedCash === null) { return null; }
        else { return round($this->expectedCash, 5); }
    }
    public function setExpectedCash($expectedCash) {
        $this->expectedCash = ($expectedCash === null) ? null : round($expectedCash, 5);
    }

    /**
     * Number of the tickets in the session. Updated only for closed cashes.
     * @var int
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected $ticketCount = null;
    public function getTicketCount() { return $this->ticketCount; }
    public function setTicketCount($ticketCount) {
        if ($this->ticketCount === null) { $this->ticketCount = $ticketCount; }
        else if ($this->ticketCount != $ticketCount) {
            throw new \UnexpectedValueException('Ticket count is read only');
        }
    }
    /** Private method to remove ticketCount from structs if it is set when not closed. */
    protected function resetTicketCount() { $this->ticketCount = null; }

    /**
     * Number of customers in the session. Updated only for closed cashes.
     * @var int
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected $custCount = null;
    public function getCustCount() { return $this->custCount; }
    /** @throws \UnexpectedValueException When trying to override it. */
    public function setCustCount($custCount) {
        if ($this->custCount === null) { $this->custCount = $custCount; }
        else if ($this->custCount != $custCount) {
            throw new \UnexpectedValueException('Customer count is read only');
        }
    }
    /** Private method to remove custCount from structs if it is set when not closed. */
    protected function resetCustCount() { $this->custCount = null; }

    /**
     * Consolidated sales. Read only and only set on close.
     * Read-only. Will throw an exception if trying to override it.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $cs = null;
    public function getCs() {
        if ($this->cs === null) { return null; }
        else { return round($this->cs, 5); }
    }
    /** @throws \UnexpectedValueException When trying to override it. */
    public function setCs($cs) {
        if ($this->cs === null) {
            $this->cs = ($cs === null) ? null : round($cs, 5);
        } else if (round($this->cs, 5) != round($cs, 5)) {
            throw new \UnexpectedValueException('Consolidated sales is read only');
        }
    }
    /** Private method to remove CS from structs if it is set when not closed. */
    protected function resetCS() { $this->cs = null; }

    /**
     * Consolidated sales total by period. It is automatically computed
     * on close.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: false)]
    protected $csPeriod = 0.0;
    public function getCsPeriod() {
        return round($this->csPeriod, 5);
    }
    public function setCsPeriod($csPeriod) {
            $this->csPeriod = round($csPeriod, 5);
    }

    /**
     * Consolidated sales total by fiscal year. It is automatically computed
     * on close.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: false)]
    protected $csFYear = 0.0;
    public function getCsFYear() {
        return round($this->csFYear, 5);
    }
    public function setCsFYear($csFYear) {
            $this->csFYear = round($csFYear, 5);
    }

    /**
     * Array of tax totals. It holds the final tax base/amount for each tax.
     * @var \App\CashSessionTax[]
     */
    #[ORM\OneToMany(targetEntity: CashSessionTax::class, mappedBy: 'cashSession', cascade: ['persist'], orphanRemoval: true)]
    protected $taxes;
    public function getTaxes() { return $this->taxes; }
    public function setTaxes($taxes) {
        $this->taxes->clear();
        foreach ($taxes as $tax) {
            $this->addTax($tax);
        }
    }
    public function clearTaxes() {
        $this->getTaxes()->clear();
    }
    public function addTax($tax) {
        $this->taxes->add($tax);
        $tax->setCashSession($this);
    }

    /**
     * Array of total amount of payments by mode.
     * @var \App\CashSessionPayment[]
     */
    #[ORM\OneToMany(targetEntity: CashSessionPayment::class, mappedBy: 'cashSession', cascade: ['persist'], orphanRemoval: true)]
    protected $payments;
    public function getPayments() { return $this->payments; }
    public function setPayments($payments) {
        $this->payments->clear();
        foreach ($payments as $payment) {
            $this->addPayment($payment);
        }
    }
    public function clearPayments() {
        $this->getPayments()->clear();
    }
    public function addPayment($payment) {
        $this->payments->add($payment);
        $payment->setCashSession($this);
    }

    /**
     * Array of total amount of cs by category.
     * @var \App\CashSessionCat[]
     */
    #[ORM\OneToMany(targetEntity: CashSessionCat::class, mappedBy: 'cashSession', cascade: ['persist'], orphanRemoval: true)]
    protected $catSales;
    public function getCatSales() { return $this->catSales; }
    public function setCatSales($catSales) {
        $this->catSales->clear();
        foreach ($catSales as $cat) {
            $this->addCatSales($cat);
        }
    }
    public function clearCatSales() {
        $this->getCatSales()->clear();
    }
    public function addCatSales($cat) {
        $this->catSales->add($cat);
        $cat->setCashSession($this);
    }


    protected $catTaxes;



    /**
     * Array of total balance change by customer.
     * @var \App\CashSessionCat[]
     */
    #[ORM\OneToMany(targetEntity: CashSessionCustBalance::class, mappedBy: 'cashSession', cascade: ['persist'], orphanRemoval: true)]
    protected $custBalances;
    public function getCustBalances() { return $this->custBalances; }
    public function setCustBalances($custBalances) {
        $this->custBalances->clear();
        foreach ($custBalances as $custBalance) {
            $this->addCustBalances($custBalance);
        }
    }
    public function clearCustBalances() {
        $this->getCustBalances()->clear();
    }
    public function addCustBalances($custBalance) {
        $this->custBalances->add($custBalance);
        $custBalance->setCashSession($this);
    }

    /** Operation flag for closing. See constants.
     * It is not exported and used only when registering a closed session. */
    private $closeType = CashSession::CLOSE_SIMPLE;
    public function getCloseType() { return $this->closeType; }
    /** Setter used for testing. $closeType is already set within fromStruct. */
    public function setCloseType($closeType) { $this->closeType = $closeType; }

    /**
     * isClosed: return true if closeDate is not null
     * @return bool
     */
    public function isClosed() { return $this->closeDate != null; }

    /**
     * isOpened: return true if openDate is not null
     * @return bool
     */
    public function isOpened() { return $this->openDate != null; }





    public function toStruct() {
        $struct = parent::toStruct();
        $struct['openDate'] = $this->getOpenDate()->getTimestamp();
        $struct['closeDate'] = $this->getCloseDate()->getTimestamp();
        return $struct;
    }


}
