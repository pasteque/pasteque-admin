<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use \App\API\VersionAPI;
use \App\System\DateUtils;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Ticket
 * @package Pasteque
 */
#[ORM\Table(name: 'tickets')]
#[ORM\UniqueConstraint(name: 'ticket_index', columns: ['cashregister_id', 'number'])]
#[ORM\Entity]
class Ticket extends Entity
{
    public function getDirectFieldNames() {
        return ['sequence', 'number', 'date', 'custCount',
                'price', 'taxedPrice',
                'discountRate', 'finalPrice', 'finalTaxedPrice',
                'custBalance'];
    }
    public function getAssociationFields() {
        return [
                [
                 'name' => 'cashRegister',
                 'class' => \App\Entity\CashRegister::class
                 ],
                [
                 'name' => 'user',
                 'class' => \App\Entity\User::class
                 ],
                [
                 'name' => 'lines',
                 'class' => \App\Entity\TicketLine::class,
                 'array' => true,
                 'embedded' => true
                 ],
                [
                 'name' => 'taxes',
                 'class' => \App\Entity\TicketTax::class,
                 'array' => true,
                 'embedded' => true
                 ],
                [
                 'name' => 'payments',
                 'class' => \App\Entity\TicketPayment::class,
                 'array' => true,
                 'embedded' => true
                 ],
                [
                 'name' =>'customer',
                 'class' => \App\Entity\Customer::class,
                 'null' => true
                 ],
                [
                 'name' => 'tariffArea',
                 'class' => \App\Entity\TariffArea::class,
                 'null' => true
                 ],
                [
                 'name' => 'discountProfile',
                 'class' => \App\Entity\DiscountProfile::class,
                 'null' => true
                 ]
                ];
    }

    public function __construct() {
        $this->lines = new \Doctrine\Common\Collections\ArrayCollection();
        $this->taxes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Internal Id of the ticket. Required to link lines and payments.
     * @var integer
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;
    public function getId() { return $this->id; }

    /**
     * Id of a cash register
     * @var int
     */
    #[ORM\JoinColumn(name: 'cashregister_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: CashRegister::class)]
    protected $cashRegister;
    public function getCashRegister() { return $this->cashRegister; }
    public function setCashRegister($cashRegister) {
        $this->cashRegister = $cashRegister;
    }

    /**
     * Number of the session's cash register
     * @var int
     */
    #[ORM\Column(type: 'integer')]
    protected $sequence;
    public function getSequence() { return $this->sequence; }
    public function setSequence($sequence) { $this->sequence = $sequence; }

    /**
     * Number of the ticket inside the session.
     * @var int
     */
    #[ORM\Column(type: 'integer')]
    protected $number;
    public function getNumber() { return $this->number; }
    public function setNumber($number) { $this->number = $number; }

    /**
     * Operator
     * @var string
     */
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class)]
    protected $user;
    public function getUser() { return $this->user; }
    public function setUser($user) { $this->user = $user; }

    /**
     * Payment date, as timestamp.
     * @var date
     */
    #[ORM\Column(type: 'datetime')]
    protected $date;
    public function getDate() { return $this->date; }
    public function setDate($date) { $this->date = $date; }

    /**
     * Array of line's ticket
     * @var \Pasteque\Ticket[]
     */
    #[ORM\OneToMany(targetEntity: TicketLine::class, mappedBy: 'ticket', cascade: ['persist'], orphanRemoval: true)]
    protected $lines;
    public function getLines() { return $this->lines; }
    public function setLines($lines) {
        $this->lines->clear();
        foreach ($lines as $price) {
            $this->addLine($price);
        }
    }
    public function clearLines() {
        $this->getLines()->clear();
    }
    public function addLine($line) {
        $this->lines->add($line);
        $line->setTicket($this);
    }

    /**
     * Array of tax totals. It holds the final tax base/amount for each
     * tax. That is after all discounts (lines and ticket).
     * @var \Pasteque\Ticket[]
     */
    #[ORM\OneToMany(targetEntity: TicketTax::class, mappedBy: 'ticket', cascade: ['persist'], orphanRemoval: true)]
    protected $taxes;
    public function getTaxes() { return $this->taxes; }
    public function setTaxes($taxes) {
        $this->taxes->clear();
        foreach ($taxes as $tax) {
            $this->addTax($tax);
        }
    }
    public function clearTaxes() {
        $this->getTaxes()->clear();
    }
    public function addTax($tax) {
        $this->taxes->add($tax);
        $tax->setTicket($this);
    }

    /**
     * Array of payment
     * @var \Pasteque\Payment[]
     */
    #[ORM\OneToMany(targetEntity: TicketPayment::class, mappedBy: 'ticket', cascade: ['persist'], orphanRemoval: true)]
    protected $payments;
public function getPayments() { return $this->payments; }
    public function setPayments($payments) {
        $this->payments->clear();
        foreach ($payments as $payment) {
            $this->addPayment($payment);
        }
    }
    public function clearPayments() {
        $this->getPayments()->clear();
    }
    public function addPayment($payment) {
        $this->payments->add($payment);
        $payment->setTicket($this);
    }

    /**
     * Id of the customer
     * @var string
     */
    #[ORM\JoinColumn(name: 'customer_id', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: Customer::class)]
    protected $customer;
    public function getCustomer() { return $this->customer; }
    public function setCustomer($customer) { $this->customer = $customer; }

    /**
     * Number of customers in the ticket.
     * @var int
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected $custCount;
    public function getCustCount() { return $this->custCount; }
    public function setCustCount($custCount) { $this->custCount = $custCount; }

    /**
     *
     * @var string
     */
    #[ORM\JoinColumn(name: 'tariffarea_id', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: TariffArea::class)]
    protected $tariffArea;
    public function getTariffArea() { return $this->tariffArea; }
    public function setTariffArea($tariffArea) {
        $this->tariffArea = $tariffArea;
    }

    /**
     * Rate of discount.
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $discountRate = 0.0;
    public function getDiscountRate() { return $this->discountRate; }
    public function setDiscountRate($discountRate) {
        $this->discountRate = $discountRate;
    }

    /**
     * Informative discount profile. The actual discount is set in discountRate.
     * @var string
     */
    #[ORM\JoinColumn(name: 'discountprofile_id', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: DiscountProfile::class)]
    protected $discountProfile;
    public function getDiscountProfile() { return $this->discountProfile; }
    public function setDiscountProfile($discountProfile) {
        $this->discountProfile = $discountProfile;
    }

    /**
     * Price without taxes nor ticket discount.
     * It is null when taxedPrice is set.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $price = null;
    public function getPrice() {
        if ($this->price === null) { return null; }
        else { return round($this->price, 2); }
    }
    public function setPrice($price) {
        if ($price === null) { $this->price = null; }
        else { $this->price = round($price, 2); }
    }

    /**
     * Price without taxes nor ticket discount.
     * It is null when price is set.
     * @var float
     */
    #[ORM\Column(type: 'float', nullable: true)]
    protected $taxedPrice = null;
    public function getTaxedPrice() {
        if ($this->taxedPrice === null) { return null; }
        else { return round($this->taxedPrice, 2); }
    }
    public function setTaxedPrice($taxedPrice) {
        if ($taxedPrice === null) { $this->taxedPrice = null; }
        else { $this->taxedPrice = round($taxedPrice, 2); }
    }

    /**
     * Total price without taxes with discount rate.
     * It is never null.
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $finalPrice;
    public function getFinalPrice() {
        return round($this->finalPrice, 2);
    }
    public function setFinalPrice($finalPrice) {
        $this->finalPrice = round($finalPrice, 2);
    }

    /**
     * Total price with taxes and discount rate.
     * It is never null.
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $finalTaxedPrice;
    public function getFinalTaxedPrice() {
        return round($this->finalTaxedPrice, 2);
    }
    public function setFinalTaxedPrice($finalTaxedPrice) {
        $this->finalTaxedPrice = round($finalTaxedPrice, 2);
    }

    /**
     * Changes in the customer's balance.
     * @var float
     */
    #[ORM\Column(type: 'float')]
    protected $custBalance = 0.0;
    public function getCustBalance() {
        return round($this->custBalance, 2);
    }
    public function setCustBalance($custBalance) {
        $this->custBalance = round($custBalance, 2);
    }

    public function toStruct() {
        $struct = parent::toStruct();
        $struct['date'] = $this->getDate()->getTimestamp();
        return $struct;
    }

 
}
