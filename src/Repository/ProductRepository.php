<?php

namespace App\Repository;

use Doctrine\DBAL\Types\Types;

/**
 * ProductRepository
 *
 */
class ProductRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByBarcodeAndVisible(?string $barcode,?bool $visible,?bool $scaled=null )
    {
        $qb = $this->createQueryBuilder('a');

        if ($barcode){
            $qb ->where('a.barcode < :barcode')
                ->setParameter('barcode', (integer)$barcode, Types::INTEGER);
        }
        if( $visible == 1 ){
            $qb ->andWhere('a.visible = :visible')
                ->setParameter('visible', $visible);
        }
        if( $scaled == 1 ){
            $qb ->andWhere('a.scaled = :scaled')
                ->setParameter('scaled', $scaled);
        }
        $qb ->orderBy('a.label','ASC');
        return $qb
            ->getQuery()
            ->getResult();
    }



}
