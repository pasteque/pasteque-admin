<?php

namespace App\Command;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class StatCommand extends Command
{
    protected static $defaultName = 'app:stat';
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }


    protected function configure()
    {
        $this->setDescription('Generate Stat');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $db=$this->em->getConnection();
        $db->executeQuery('DELETE FROM stat_sales WHERE date >\'2018-04-02\'');
        $select = "select product_id,DATE_FORMAT(date,'%Y-%m-%d') as anneemoisjour,  round(sum(tl.quantity),2) as quantite,round(sum(tl.final_taxed_price),2),0 as margin,0 as price_buy,taxed_unit_price as price_sell,tax_id from ticketlines tl,tickets t where tl.ticket_id = t.id GROUP BY product_id,anneemoisjour,margin, price_buy, price_sell,tax_id ORDER BY t.date";
        $insert='INSERT INTO stat_sales (product_id,date,quantity,sales,margin,price_buy,price_sell,tax_id) '.$select;
        $db->executeQuery($insert);
        $io->success('Générations des stat OK');
        return 0;
    }




}
