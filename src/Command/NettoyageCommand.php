<?php

namespace App\Command;

use App\Action\ProductAction;
use App\Entity\Product;
use App\Entity\ProductSupplier;
use App\Entity\Supplier;
use App\Service\ClientAPI;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class NettoyageCommand extends Command
{
    protected static $defaultName = 'app:nettoyage';
    protected $pa;
    protected $em;
    protected $sac;

    public function __construct(EntityManagerInterface $em, Sac $sac)
    {
        parent::__construct();
        $this->em = $em;
        $this->sac = $sac;
        $sac->initSystemeCache('file','pasteque-admin-console');
        $sac->initSac(true);

    }


    protected function configure()
    {
        $this->setDescription('Importation des fournisseurs et des liens produits')
            ->addOption('truncate','t');

    }


    static function trimarray($n){
        return trim(trim(utf8_encode((string) $n),'"'));
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $db = $this->em->getConnection();
        $clientAPI = new ClientAPI(new RequestStack(), $this->sac);

        $requete = "select id from products p where  (select count(*) from ticketlines tl where p.id=tl.product_id)=0";
        $tab_id= $db->fetchFirstColumn($requete);
        $tab_product=$this->em->getRepository(Product::class)->findBy(['id'=>$tab_id]);
        foreach($tab_product as $product){
            [$reponse, $statut, $err] = $clientAPI->appelAPI('/api/product/'.$product->getId(), [], [], 'DELETE');
            if ($statut == 200) {
                $this->em->remove($product);
                $this->em->flush();
            }
        }

        $io->success('Nettoyage OK');
        return 0;
    }


}
