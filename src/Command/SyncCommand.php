<?php

namespace App\Command;

use App\Service\ClientAPI;
use App\Service\Synchro;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class SyncCommand extends Command
{
    protected static $defaultName = 'app:sync';
    protected $sac;
    protected $em;

    public function __construct(Sac $sac,EntityManagerInterface $em)
    {
        parent::__construct($this::$defaultName);

        $this->sac = $sac;
        $sac->initSystemeCache('file','pasteque-admin-console');
        $sac->initSac(true);
        $this->em = $em;
    }


    protected function configure()
    {
        $this
            ->setDescription('Synchronize all data from api')
            ->addOption('truncate','t');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $clientApi = new ClientAPI(new RequestStack(),$this->sac);
        $synchro = new Synchro($this->em,$this->sac,$clientApi);

        if ($input->hasOption('truncate')  && $input->getOption('truncate')) {
            $synchro->reinit_table();
        }
        $synchro->synchronisation();
        $this->sac->clear();
        $this->sac->initSac(true);

        $synchro->synchronisation_session();

        $this->sac->clear();
        $this->sac->initSac(true);

        $synchro->synchro_ticket();
        $io->success('synchro OK');
        return 0;
    }
}
