<?php

namespace App\Command;

use App\Entity\Product;
use App\Service\ClientAPI;
use  Declic3000\Pelican\Service\Requete;

use App\Service\Synchro;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class SynciCommand extends Command
{
    protected static $defaultName = 'app:sync_image';
    protected $sac;
    protected $em;

    public function __construct(Sac $sac,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->sac = $sac;
        $this->em = $em;
        $this->sac->initSystemeCache('file','pasteque-admin-console');
        $this->sac->initSac(true);

    }


    protected function configure()
    {
        $this->setDescription('Synchronize all image from api');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->sac->initSac(true);
        $clientApi = new ClientAPI(new RequestStack(),$this->sac);
        $synchro = new Synchro($this->em,$this->sac,$clientApi);
        $synchro->synchro_images();
        $synchro->synchro_images("category");
        $io->success('Synchro Image OK');
        return 0;
    }

}
