<?php

namespace App\Command;

use App\Entity\Product;
use App\Entity\ProductSupplier;
use App\Entity\Supplier;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportCommand extends Command
{
    protected static $defaultName = 'app:import';
    protected $sac;
    protected $em;

    public function __construct(EntityManagerInterface $em, Sac $sac)
    {
        parent::__construct();
        $this->em = $em;
        $this->sac = $sac;
        $sac->initSystemeCache('file','pasteque-admin-console');
        $sac->initSac(true);
    }


    protected function configure()
    {
        $this->setDescription('Importation des fournisseurs et des liens produits')
            ->addOption('truncate','t');

    }


    static function trimarray($n){
        return trim(trim(utf8_encode((string) $n),'"'));
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $db = $this->em->getConnection();

        if ($input->hasOption('truncate')  && $input->getOption('truncate')) {
            $db->executeQuery('SET FOREIGN_KEY_CHECKS=0;');
            $db->executeQuery('TRUNCATE TABLE products_suppliers');
            $db->executeQuery('TRUNCATE TABLE suppliers');
            $db->executeQuery('SET FOREIGN_KEY_CHECKS=1;');
        }



        $fichier = $this->sac->get('dir.root') . '/import/FOURNISS.csv';
        $pointeur_fichier = fopen($fichier, "r");
        $nb = 0;
        $tab_nom_cat = [];
        if ($pointeur_fichier <> 0) {
            fgets($pointeur_fichier, 4096);
            while (!feof($pointeur_fichier)) {
                $ligne = fgets($pointeur_fichier, 4096);
                $tab = explode(";", $ligne);
                $tab = array_map($this->trimarray(...), $tab);
                $tab = array_map("addslashes", $tab);

                if (isset($tab[2]) && !empty($tab[2])) {
                    $supplier = new Supplier();
                    $supplier->setName($tab[2]);
                    $supplier->setAddress1($tab[3]);
                    $supplier->setAddress2($tab[4]);
                    $supplier->setTown($tab[7]);
                    $supplier->setCountry('FR');
                    $supplier->setZipcode($tab[5]);
                    $supplier->setPhone($tab[8]);
                    $supplier->setFax($tab[9]);
                    $supplier->setComment($tab[10]);
                    $this->em->persist($supplier);
                    $this->em->flush();
                    $tab_supplier[$tab[1]] = $supplier;
                }
            }
        }

        $io->success('Importation des fournisseur OK');

        $fichier = $this->sac->get('dir.root') . '/import/PRODUIT.csv';
        $pointeur_fichier = fopen($fichier, "r");
        if ($pointeur_fichier <> 0) {
            fgets($pointeur_fichier, 4096);
            while (!feof($pointeur_fichier)) {
                $ligne = fgets($pointeur_fichier, 4096);
                $tab = explode(";", $ligne);
                $tab = array_map($this->trimarray(...), $tab);
                $tab = array_map("addslashes", $tab);

                if (isset($tab[1])){

                    $psupplier = new ProductSupplier();
                    $barcode = (int)$tab[1];
                    $product = $this->em->getRepository(Product::class)->findOneBy(['barcode' => $barcode]);

                    if (!empty($product) && isset($tab_supplier[$tab[7]])) {
                        $psupplier->setProduct($product);
                        $psupplier->setSupplier($tab_supplier[$tab[7]]);
                        $psupplier->setReference($tab[2]);
                        $this->em->persist($psupplier);
                    }
                }

            }
            $this->em->flush();
        }
        $io->success('Importation des ref fournisseurs OK');
        return 0;
    }


}
