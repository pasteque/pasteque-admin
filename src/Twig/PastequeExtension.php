<?php
namespace App\Twig;

use App\Component\Menu;
use  Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use  Declic3000\Pelican\Service\Suc;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Service\Gendarme;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


class PastequeExtension extends AbstractExtension
{

    protected $sac;

    protected $router;

    protected $translator;

    protected $twig;

    protected $suc;



    function __construct(Sac $sac, Suc $suc, UrlGeneratorInterface $router, TranslatorInterface $translator, Environment $twig)
    {
        $this->sac = $sac;
        $this->router = $router;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->suc = $suc;
    }


    public function getFilters(): array
    {
        return parent::getFilters()+[

                new TwigFilter('measureUnit', fn($value) => measureUnit($value))

        ];
    }



    public function getFunctions(): array
    {
        return [


            new TwigFunction('is_string', fn($input) => is_string($input)),

            new TwigFunction('is_float', fn($input) => is_float($input)),

                new TwigFunction('table_cherche', function ($tab,$cle,$valeur,$champs='amount') {
                $tab_temp = [];

                foreach($tab as $t){
                    $getchamp = 'get'.ucfirst($champs);
                    $tab_temp[$t->getId()[$cle]]=$t->$getchamp();
                }
                return $tab_temp[$valeur] ?? 0;
            }),


            new TwigFunction('menu_general', function () {
                $menu = new Menu($this->sac, new EventDispatcher());
                return $menu->get();
            }),

            new TwigFunction('contenance_affichage', function ($contenance, $code_unite) {
                $unite = measureUnit($code_unite);
                if($unite=='gramme'){
                    if ($contenance>=1000){
                        $contenance = $contenance/1000;
                        $unite = 'kilogramme'.(($contenance>=2)?'s':'');
                    }
                    else{
                        $unite .= ($contenance>=2)?'s':'';
                    }
                }else if($unite=='litre'){
                    if ($contenance<1){
                        $contenance = $contenance*100;
                        $unite = 'centilitre'.(($contenance>=2)?'s':'');
                    }  else{
                        $unite .= ($contenance>=2)?'s':'';
                    }
                }
                return $contenance.' '.$unite;
            }),


              new TwigFunction('location', fn($code=null) => getLocation($code))


        ];
    }








}



