<?php
namespace App\EntityExtension;


use App\Entity\Image;
use App\Entity\Supplier;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;



class SupplierExt extends EntityExtension
{

    protected $supplier;


    function __construct(Supplier $supplier, Sac $sac,  EntityManager $em)
    {
        $this->supplier = $supplier;
        parent::__construct($sac, $em);
    }


    public function verifHasImage(){
        $has_image=$this->em->getRepository(Image::class)->findOneBy(['modelId'=>$this->supplier->getPrimaryKey(),'model'=>'supplier']);
        $this->supplier->setHasImage(!empty($has_image));
        $this->em->persist($this->supplier);
        $this->em->flush();
    }



    public function getDocument(){

        return $this->em->getRepository(Image::class)->findOneBy(['model_id'=>$this->supplier->getPrimaryKey(),'model'=>'supplier']);

    }



}

