<?php
namespace App\EntityExtension;


use App\Entity\Etape;
use App\Entity\Image;
use App\Entity\Supplier;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;



class EtapeExt extends EntityExtension
{

    protected $etape;


    function __construct(Etape $etape, Sac $sac,  EntityManager $em)
    {
        $this->etape = $etape;
        parent::__construct($sac, $em);
    }


    public function getStat(){

        $db=$this->em->getConnection();
        $tab=[];
        $sql='SELECT count(etp.product_id) from etapes_products etp  where etape_id='.intval($this->etape->getId()).' and etat=0';
        $tab['a_faire']= $db->fetchFirstColumn($sql);
        $sql='SELECT count(etp.product_id) from etapes_products etp where etape_id='.intval($this->etape->getId()).' and etat=1';
        $tab['fait'] = $db->fetchFirstColumn($sql);
        return $tab;
    }

}

