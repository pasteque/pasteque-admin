<?php
namespace App\EntityExtension;


use App\Entity\Image;
use App\Entity\Product;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\ChargeurDb;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;



class ProductExt extends EntityExtension
{

    function __construct(protected Product $product, Sac $sac,  EntityManager $em)
    {
        parent::__construct($sac, $em);
    }


    public function fromArray(array $userInput)
    {

        $this->product->fromArray($userInput);

        $chargeur = new Chargeur($this->em);
        if (isset($userInput['category_id'])){
            $this->product->setCategory( $chargeur->charger_objet('category',$userInput['category_id']));
        }
        if (isset($userInput['tax_id'])){
            $this->product->setTax( $chargeur->charger_objet('tax',$userInput['tax_id']));
        }
        if (isset($userInput['priceSellvat'])) {
            $this->product->setTaxedPrice($userInput['priceSellvat']);
        }
    }



    public function verifHasImage(){
        $has_image=$this->em->getRepository(Image::class)->findOneBy(['modelId'=>$this->product->getPrimaryKey(),'model'=>'product']);
        $this->product->setHasImage(!empty($has_image));
        $this->em->persist($this->product);
        $this->em->flush();
    }



    public function getDocument(){
        return $this->em->getRepository(Image::class)->findOneBy(['model_id'=>$this->product->getPrimaryKey(),'model'=>'product']);
    }

    public function getStat(){
        $tab_stat=[];
        $sql = "select DATE_FORMAT(date, '%Y-%m') as anneemois, round(sum(quantity),2) as quantite from stat_sales where product_id =".$this->product->getId()." GROUP BY anneemois ORDER BY anneemois";
        $db= $this->em->getConnection();
        $stmt = $db->executeQuery($sql);
        $tab_stat=[];
        while($row = $stmt->fetchAssociative()) {
            $tab_stat[$row['anneemois']]=$row['quantite'];
        }
        return $tab_stat;
    }



    public function getMots()
    {
        $tab_id_mot = $this->getIdMots();
        $tab_mot = $this->sac->tab('mot');
        $tab_systeme = array_keys(table_filtrer_valeur($tab_mot, 'systeme', true));
        if(!empty($tab_systeme)){
            $tab_id_mot = array_diff($tab_id_mot, $tab_systeme);
        }
        return table_simplifier(array_intersect_key($tab_mot, array_flip($tab_id_mot)));
    }




    public function getIdMots()
    {
        $tab_mot = $this->product->getMots();
        $tab = [];
        foreach ($tab_mot as $mot) {
            $tab[] = $mot->getPrimaryKey();
        }
        return $tab;
    }




}

