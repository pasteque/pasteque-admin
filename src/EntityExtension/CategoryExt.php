<?php
namespace App\EntityExtension;


use App\Entity\Image;
use App\Entity\Category;
use Declic3000\Pelican\EntityExtension\EntityExtension;
use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;



class CategoryExt extends EntityExtension
{

    protected $category;


    function __construct(Category $category, Sac $sac,  EntityManager $em)
    {
        $this->category = $category;
        parent::__construct($sac, $em);
    }


    public function verifHasImage(){
        $has_image=$this->em->getRepository(Image::class)->findOneBy(['modelId'=>$this->category->getPrimaryKey(),'model'=>'category']);
        $this->category->setHasImage(!empty($has_image));
        $this->em->persist($this->category);
        $this->em->flush();
    }



    public function getDocument(){

        return $this->em->getRepository(Image::class)->findOneBy(['model_id'=>$this->category->getPrimaryKey(),'model'=>'category']);

    }

    public function fromArray(array $userInput)
    {

        $chargeur= new Chargeur($this->em);
        if (isset($userInput['parent'])){
            $this->category->setParent( $chargeur->charger_objet('category',$userInput['parent']));
        }

    }




    public function getStat(){
        $db= $this->em->getConnection();
        $sql = "select DATE_FORMAT(date, '%Y-%m') as anneemois, round(sum(quantity),2) as quantite from stat_sales where product_id IN (select id from products where category_id=".$this->category->getId().") GROUP BY anneemois ORDER BY anneemois";
        $stmt = $db->executeQuery($sql);
        $tab_stat=[];
        while($row = $stmt->fetchAssociative()) {

            $tab_stat[$row['anneemois']]=$row['quantite'];
        }
        return $tab_stat;
    }

}

