<?php
// src/Event/AfterSendMailEvent.php
namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class ConfigModifEvent extends Event
{
    public function __construct(private $returnValue)
    {
    }

    public function get()
    {
        return $this->returnValue;
    }

    public function set($returnValue)
    {
        $this->returnValue = $returnValue;
    }
}