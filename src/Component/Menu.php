<?php
namespace App\Component;

use App\Event\MenuModifEvent;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Menu
{

    protected $sac;
    protected $dispatcher;


    function __construct(Sac $sac, EventDispatcherInterface $eventDispatcher)
    {
        $this->sac = $sac;
        $this->dispatcher =$eventDispatcher;
    }


    function get()
    {
        $menu = [

            'catalog'=>
                ['label'=> 'Catalog', 'permission'=>'all', 'menu'=>['product_index'=>'Products', 'category_arbo'=>'Categories', 'tax_index'=>'Taxes', 'supplier_index'=>'Suppliers']],
            'costumer'=>
                ['label'=> 'Costumer', 'permission'=>'all', 'menu'=>['customer_index'=>'customer', 'discount_index'=>'discount', 'discountcampaigns'=>'discount_campaigns']],
            'sales'=>
                ['label'=> 'sales', 'permission'=>'all', 'menu'=>['ticketz'=>'ticketz', 'sales'=>'Sales by product']],
            'commande'=>
                ['label'=> 'Commande adh', 'permission'=>'all', 'menu'=>[
                    'commande_index'=>'Prise de commande',
                    'commande_compilation'=>'Compilation commande'
                ]],

            'EticEtiquette'=>
                ['label'=> 'Etic Etiquette', 'permission'=>'all', 'menu'=>['etic'=>'Etic Etape']],

            'administration'=>
                ['label'=> 'Administration', 'permission'=>'all', 'menu'=>['restaurantmap'=>'Restaurant map', 'cashregister_index'=>'cashregister', 'currency_index'=>'currency', 'paymentmode_index'=>'Payment modes', 'mot_index'=>'mots', 'resource_index'=>'Resources', 'user_index'=>'Users', 'role_index'=>'Roles']],

            'tools'=>
                ['label'=> 'Tools', 'permission'=>'all', 'menu'=>['tools'=>'Mémoire', 'inspector'=>'Inspector']],
            /*
                'cashes'=>
                    array(
                        'label'=> 'administration',
                        'permission'=>'all',
                        'menu'=>array(
                            'cashes'=>'cashes',
                            'ztickets'=>'Z tickets',
                        )
                    ),
            */
            '?'=>
                ['label'=> '?', 'permission'=>'all', 'menu'=>['http://www.pasteque.org'=>'Documentation', 'credits'=>'Crédits']]
        ];
  





        //supprimer les entrées des modules non-activés
    
        $tab_module = $this->sac->conf('module');
        $tab_module_inactif = [];
        if (is_array($tab_module)){
            foreach($tab_module as $module => $actif) {
                if ($actif === false) {
                    $tab_module_inactif[$module] = $module;
                }
            }
        }
        if($tab_module)
            $menu = $this->supprime_module_non_actif($menu,$tab_module_inactif);
        


        return $menu;
    }


    function supprime_module_non_actif($menu,$tab_prestation_inactive)
    {
        $entree = array_keys($menu);
        if (!is_array($menu)) {
            $entree[] = $menu;
        }
        foreach ($entree as $valeur1) {
            $supprime = (array_search($valeur1, $tab_prestation_inactive));
            if ($supprime) {
                unset($menu[$valeur1]);
                unset($tab_prestation_inactive[$supprime]);
            } else {
                if(isset($menu[$valeur1]['menu'])){
                foreach ($menu[$valeur1]['menu'] as  $cle2 => $valeur2) {
                    $supprime = (array_search($cle2, $tab_prestation_inactive));
                    //enlever prefixe sr_ et suffixe liste
                    if(!$supprime)
                        $supprime = (array_search(substr((string) $cle2,3,-6), $tab_prestation_inactive));
                    //enlever  suffixe liste
                    if(!$supprime)
                        $supprime = (array_search(substr((string) $cle2,0,-6), $tab_prestation_inactive));
                    if ($supprime) {
                        unset($tab_prestation_inactive[$supprime]);
                        unset($menu[$valeur1]['menu'][$cle2]);
                    }
                }
                }
            }

        }



        $event = new MenuModifEvent($menu);
        $this->dispatcher->dispatch($event, 'menu.modif');

        return $event->getMenu();
}
}
