<?php

namespace App\Component\Filtre;


use Declic3000\Pelican\Component\Filtre\Filtre;

class SupplierFiltre extends Filtre
{



    function filtre_ajouter_active($valeur)
    {
        $checked = ($this->requete->ouArgs('active', $this->valeur_par_defaut)!==null);
        $options = [['on' => $checked, 'valeur' => '1', 'libelle' => 'Masquer fournisseurs historiques']];
        return [
            'id' => 'active',
            'nom' => 'active',
            'type' => 'checkbox',
            'options' => $options];
    }
    function filtre_ajouter_isDirect($valeur)
    {
        $checked = ($this->requete->ouArgs('isDirect', $this->valeur_par_defaut)!==null);
        $options = [['on' => $checked, 'valeur' => '1', 'libelle' => 'Masquer les grossistes']];
        return [
            'id' => 'isDirect',
            'nom' => 'isDirect',
            'type' => 'checkbox',
            'options' => $options];
    }

}