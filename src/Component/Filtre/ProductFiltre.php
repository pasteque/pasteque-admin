<?php

namespace App\Component\Filtre;


use App\Entity\Product;
use Declic3000\Pelican\Component\Filtre\Filtre;

class ProductFiltre extends Filtre
{


    function filtre_ajouter_categorie($valeur)
    {

        $tab_hierarchie = $this->sac->tab('category_tree');
        $tab_cat = $this->sac->tab('category');
        $tab_cat = arbre_linearise_niveau($tab_hierarchie, $tab_cat);
        return $this->filtre_prepare_donnee_hierarchie('category', $tab_cat, $valeur);
    }

    function filtre_ajouter_location($valeur)
    {
        $tab_location = getLocation();
        foreach ($tab_location as $key => &$location)
            $location = ['valeur' => $key, 'libelle' => $location];
        return ['nom' => 'location', 'class' => 'select2_simple', 'options' => $tab_location];
    }


    function filtre_ajouter_visible($valeur)
    {
        $checked = ($this->requete->ouArgs('visible', $this->valeur_par_defaut)!==null);
        $options = [['on' => $checked, 'valeur' => '1', 'libelle' => 'Masquer les produits inactifs']];
        return [
            'id' => 'visible',
            'nom' => 'visible',
            'type' => 'checkbox',
            'options' => $options];
    }


}