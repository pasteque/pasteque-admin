<?php

namespace App\Component\Table;

use Declic3000\Pelican\Component\Table\Table;


class SupplierTable extends Table
{

    protected $objet = 'supplier';

    public const FILTRES = [
        'premier' => [
            'search' => ['ajouter_recherche'],
            'isDirect' => ['ajouter_isDirect'],
            'active' => ['ajouter_active']
        ]
    ];


    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'name' => ['responsivePriority' => 1, 'class' => 'min100'],
        'town' => [],
        'country' => [],
        'isDirect' => ['title' => 'Producteur en direct'],
        'active' => ['title' => 'Actif'],
        'action' => ["orderable" => false,'width'=>'110']
    ];








}
