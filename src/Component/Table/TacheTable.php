<?php

namespace App\Component\Table;

use Declic3000\Pelican\Component\Table\Table;


class TacheTable extends Table
{

    protected $objet = 'tache';

    public const COLONNES = [
        'idTache' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'descriptif' => ['responsivePriority' => 2, 'class' => 'min100'],
        'fonction' => ['responsivePriority' => 2, 'class' => 'min100'],
        'dateExecution' => ['responsivePriority' => 2, 'class' => 'min100'],
        'priorite' => ['responsivePriority' => 2, 'class' => 'min100'],
    	'statut' => ['responsivePriority' => 2, 'class' => 'min100'],
        'action' => ["orderable" => false, 'responsivePriority' => 2, 'class' => 'min100']
    ];





}
