<?php

namespace App\Component\Table;
use Declic3000\Pelican\Component\Table\Table;


class MotTable extends Table
{

    protected $objet = 'mot';

    public const COLONNES = [
        'idMot' => ['title' => 'id','class'=>'min-mobile-l'],
        'createdAt' => ['title' => 'cree_le',"type" => 'date-eu'],
        'nom' => ['responsivePriority'=>1,'class'=>'min150'],
        'descriptif' => ['responsivePriority'=>10,'class'=>'min200'],
        'action' => ["orderable" => false,'responsivePriority'=>2,'class'=>'min100']
        ];


    
    
    function liste_autocomplete($limit = 20): array
    {
        $nom_sql= $this->sac->descr($this->objet . '.nom_sql');
        $nom_colonne_code = $this->sac->descr($this->objet . '.cle_sql');
        $sous_requete = $this->getSelectionObjet(['systeme'=>'false'], $nom_colonne_code . ' as id, '.$nom_sql.'.nom as nom');
        $tab = $this->db->fetchAllAssociative($sous_requete . ' LIMIT 0,' . $limit);
        $tab_data = [];
        foreach ($tab as $el) {
            $tab_data[] = [
                'id' => $el['id'],
                'text' => trim((string) $el['nom']),


            ];
        }
        return $tab_data;
    }




}
