<?php

namespace App\Component\Table;


use Declic3000\Pelican\Component\Table\Table;



class PaymentmodeTable extends Table
{

    protected $objet = 'paymentmode';

    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'reference' => ['title' => 'reference'],
        'label' => ['responsivePriority' => 2, 'class' => 'min100'],
        'backLabel' => ['title' => 'backlabel','responsivePriority' => 3, "orderable" => false],
        'type' => ['title' => 'type','responsivePriority' => 4, 'class' => 'min100', "orderable" => false],
    	'dispOrder' => ['responsivePriority' => 2, 'class' => 'min100', "orderable" => false],
        'action' => ["orderable" => false,'width'=>'110px']
    ];




}
