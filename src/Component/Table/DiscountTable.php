<?php

namespace App\Component\Table;

use Declic3000\Pelican\Component\Table\Table;


class DiscountTable extends Table
{

    protected $objet = 'discount';

    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'startDate' => [],
        'endDate' => [],
        'rate' => [],
        'permission' => [],
        'action' => ["orderable" => false,'width'=>'110']
    ];








}
