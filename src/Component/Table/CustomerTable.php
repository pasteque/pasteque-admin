<?php

namespace App\Component\Table;

use Declic3000\Pelican\Component\Table\Table;


class CustomerTable extends Table
{

    protected $objet = 'customer';

    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'reference' => ['title' => 'reference'],
        'label' => ['responsivePriority' => 2, 'class' => 'min100'],
        'action' => ["orderable" => false,'width'=>'110']
    ];








}
