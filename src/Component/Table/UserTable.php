<?php

namespace App\Component\Table;

use Declic3000\Pelican\Component\Table\Table;


class UserTable extends Table
{

    protected $objet = 'user';

    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'image' => [],
        'name' => [],
        'role.name' => [],
        'active' => [],
        'action' => ["orderable" => false,'width'=>'110']
    ];








}
