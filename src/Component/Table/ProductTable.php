<?php

namespace App\Component\Table;


use Declic3000\Pelican\Component\Table\Table;


class ProductTable extends Table
{

    protected $objet = 'product';

    public const FILTRES = [
        'premier' => [
            'search' => ['ajouter_recherche'],
            'category' => ['ajouter_categorie'],
            'location' => ['ajouter_location'],
            'visible' => ['ajouter_visible']
        ]
    ];



    public const COLONNES = [
        'id' => ['title' => 'id', 'class' => 'min-mobile-l'],
        'barcode' => ['responsivePriority' => 1,'title' => 'barcode'],
        'reference' => ['title' => 'reference'],
        'label' => [ 'class' => 'min100'],
        'category.label' => ['title' => 'Category','responsivePriority' => 3, "orderable" => false],
        'tax.label' => ['title' => 'Tax','responsivePriority' => 4, 'class' => 'min100', "orderable" => false],
        'priceBuy' => ['responsivePriority' => 3, 'class' => 'min100', "orderable" => false],
        'priceSellvat' => [ 'class' => 'min100', "orderable" => false],
        'productExtra.location' => ['title' => 'location','traitement'=>'transformeLocation','responsivePriority' => 5, 'class' => 'min100', "orderable" => false],
        'updatedAt' => ['responsivePriority' => 3,'title' => 'Updated at',"type" => 'datetime-eu',],
        'productExtra.labelToPrint' => ['title' => 'Label to print','traitement'=>'activerDesactiverBoutonLabel', "orderable" => false],
        'visible' => ['title' => 'Actif','traitement'=>'activerDesactiverVisible', "orderable" => false],
        'action' => ['responsivePriority' => 1,"orderable" => false,'width'=>'110px']
    ];




    function filtre_prepare_donnee_hierarchie($nom, $tab_valeur, $valeur)
    {


        $filtre = [
            'id'=> $nom,
            'nom' => $nom,
            'type' => 'select2_hierarchie',
            'class' => 'select2_hierarchie'
        ];
        if ($tab_valeur) {
            foreach ($tab_valeur as $k => $val) {
                $temp = ['libelle' => $val['label'], 'valeur' => $k, 'class'=>'niveau'.$val['niveau'], 'on' => ($valeur == $val)];
                $filtre['options'][] =$temp;
            }
        }
        return $filtre;
    }


    function linearise($tree,$tab,$niveau=0){
        $result =  [];
        if (is_array($tree)){
            foreach($tree as $id=>$elt){
                $tmp = $tab[$id];
                $tmp['niveau'] = $niveau;
                $result[] = $tmp;
                $result = array_merge($result,$this->linearise($elt,$tab,$niveau+1));
            }
        }
        $res=[];
        foreach($result as $r){
            $res[$r['id']] = $r;
        }
        return $res;
    }


}
