<?php

namespace App\Action;


use App\Entity\User;


class UserAction extends Action
{


    function formSave($form, User $objet_data, $modification)
    {

        $data_form = $form->getData();


        if (!$modification) {
            [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/user', $objet_data->toStruct(), [], 'POST');
        } else {
            // TODO : Passer en méthode PATCH
            $ob_tab = $objet_data->toStruct();
            $ob_tab['id'] = $objet_data->getId();
            [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/user', $ob_tab, [], 'POST');

        }
        if ($statut == 200) {
            $this->em->persist($objet_data);
            $this->em->flush();
            $id = $objet_data->getId();
        }

        return ['id' => $objet_data->getPrimaryKey()];
    }


}