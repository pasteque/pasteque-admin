<?php

namespace App\Action;

use App\Service\ClientAPI;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class Action extends \Declic3000\Pelican\Action\Action
{

    protected $clientAPI;

    public function __construct(Requete $requete, EntityManagerInterface $em, Sac $sac, Suc $suc, LogMachine $log)
    {
        parent::__construct($requete,$em,$sac,$suc,$log);
        $this->clientAPI = new ClientAPI($requete->getRequestStack(), $sac);
    }



}