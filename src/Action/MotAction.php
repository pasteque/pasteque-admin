<?php

namespace App\Action;


use App\Entity\Mot;
use Declic3000\Pelican\Action\Action;


class MotAction extends Action
{

    function form_save_after_flush(Mot $objet_data, $modification, $form){

        $this->sac->clear();
        $this->sac->initSac(true);
    }

    function deleteCplt($objet_data){
        $this->sac->clear();
        $this->sac->initSac(true);
    }

}