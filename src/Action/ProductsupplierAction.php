<?php

namespace App\Action;


use App\Entity\Bloc;
use App\Entity\Motgroupe;
use App\Entity\Product;
use App\Entity\ProductSupplier;
use App\Entity\Supplier;


class ProductsupplierAction extends Action
{

    function chargementDataCplt($modification,ProductSupplier $ps, $builder)
    {
        if ($ps->getSupplier()){
            $supplier_id = $ps->getSupplier()->getId();
            $builder->get('supplier_id')->setData($supplier_id);
        }
    }


    function form_save_cplt(ProductSupplier $objet_data, $modification, $form)
    {
        if (!$modification) {
            $id_product = $this->requete->get('product_id');
            $product = $this->em->getRepository(Product::class)->find($id_product);
            $objet_data->setProduct($product);
            }
        else{
            $product = $objet_data->getProduct();
        }
            $supplier_id =  $form->get('supplier_id')->getData();
            $supplier = $this->em->getRepository(Supplier::class)->find($supplier_id);
            $objet_data->setSupplier($supplier);

            $favori =  $form->get('favori')->getData();
            if($favori){
                $tab_ps = $this->em->getRepository(ProductSupplier::class)->findBy(['product'=>$product]);
                foreach ($tab_ps as $ps){
                    $ps->setFavori($supplier_id === $ps->getSupplier()->getId());
                    $this->em->persist($ps);
                }
            }


    }


    function form_save_after_flush(ProductSupplier $objet_data, $modification, $form){

        $this->sac->clear();
        $this->sac->initSac(true);
    }
}