<?php

namespace App\Action;


use App\Entity\Bloc;
use App\Entity\Product;
use App\Entity\ProductExtra;


class ProductAction extends Action
{
    function chargementDataCplt($modification, Product $objet_data, $builder)
    {

        if ($modification) {
            $builder->get('category_id')->setData($objet_data->getCategory()->getId());
            $product_extra = $objet_data->getProductExtra();
            if ($product_extra) {
                $builder->get('product_cplt')->get('location')->setData($product_extra->getLocation());
            }
            $mots = $objet_data->getMots();
                $tab_id_mot=[];
            foreach ($mots as $mot) {
                $tab_id_mot[] = $mot->getIdMot();
            }
            $builder->get('mots')->setData($tab_id_mot);



        }
    }


    function formSave($form, Product $objet_data, $modification)
    {
        $scaleType = $form->get('scaleType')->getData();
        $scaleValue = $form->get('scaleValue')->getData();
        $tab_mot = $form->get('mots')->getData();
        $objet_data->removeMots();
        foreach($tab_mot as $id_mot){
            $mot = $this->chargeur->charger_objet('mot',$id_mot);
            $objet_data->addMot($mot);
        }
        $objet_data->setReference($form->get('barcode')->getData());
        $visible = $form->get('visible')->getData();
        $objet_data->setScaleType($scaleType);
        $objet_data->setScaleValue($scaleValue);
        $objet_data->setVisible($visible);
        $id_category = $form->get("category_id")->getData();
        if ($id_category) {
            $category = $this->chargeur->charger_objet('category', $id_category);
            $objet_data->setCategory($category);
        }

        if (!$modification) {
            $ok = $this->creation_api($objet_data);
        } else {
            $ok = $this->modification_api($objet_data);

        }
        if ($ok) {
            $this->em->persist($objet_data);
            $this->em->flush();
            $location = $form->get('product_cplt')->get('location')->getData();
            $product_extra = $objet_data->getProductExtra();
            if(!$product_extra){
                $product_extra=new ProductExtra();
                $product_extra->setProduct($objet_data);
            }
            $product_extra->setLocation((((int)$location)===0)?null:(int)$location);
            $this->em->persist($product_extra);
            $this->em->flush();

        }
        return ['id' => $objet_data->getPrimaryKey()];
    }


    function creation_api(Product $produit){
        [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/product', $produit->toStruct(), [], 'POST');
        return [($statut == 200),$reponse->id];
    }

    function modification_api(Product $produit){
        $ob_tab = $produit->toStruct();
        $ob_tab['id'] = $produit->getId();
        [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/product', $ob_tab, [], 'POST');
        return $statut == 200;
    }

    function labelToPrint(Product $produit,?bool $valeur=null)
    {

        $product_extra = $produit->getProductExtra();
        if (!$product_extra) {
            $product_extra = new ProductExtra();
            $product_extra->setProduct($produit);
            $valeur ??= true;
            $product_extra->setLabelToPrint($valeur);
        } else {
            $valeur ??= !$product_extra->getLabelToPrint();
            $product_extra->setLabelToPrint($valeur);
        }
        $this->em->persist($product_extra);
        $this->em->flush();
        return true;
    }


    function supprimer(Product $produit){

        [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/product/'.$produit->getId(), [], [], 'DELETE');
        if ($statut == 200) {
            $this->em->remove($produit);
            $this->em->flush();
        }

    }



    function changePrix(Product $produit,$prix_achat,$marge=null){


        $taux_tva = 1 + $produit->getTax()->getRate();
        if($marge===null){
            $marge = 1-($produit->getMarginRate()/100);
        }
        else{
            $marge = 1-($marge/100);
        }
        $nouveau_prix_de_vente = ($prix_achat / $marge) * $taux_tva;
        $nouveau_prix_de_vente = round($nouveau_prix_de_vente / 0.05) * 0.05;
        $nouveau_prix_de_vente = $nouveau_prix_de_vente / $taux_tva;
        $produit->setPriceBuy($prix_achat);
        $produit->setPriceSell($nouveau_prix_de_vente);
        if ($this->modification_api($produit)) {
            $this->em->persist($produit);
        }

    }




}