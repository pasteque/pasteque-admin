<?php

namespace App\Action;

use App\Entity\CashRegister;


class CashregisterAction extends Action
{


    function formSave($form, CashRegister $objet_data, $modification)
    {
        if (!$modification) {
            [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/cashregister', $objet_data->toArray(), [], 'POST');
        } else {
            // TODO : Passer en méthode PATCH
            //unset($pr['id']);
            $objet_data_array = $objet_data->toArray();
            $objet_data_array['id'] = $objet_data->getId();
            [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/cashregister', $objet_data_array, [], 'POST');

        }
        if ($statut == 200) {
            $this->em->persist($objet_data);
            $this->em->flush();
            $this->sac->clear();
            $this->sac->initSac(true);
        }
        return ['id' => $objet_data->getPrimaryKey()];
    }


}