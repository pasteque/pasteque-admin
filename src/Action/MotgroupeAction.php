<?php

namespace App\Action;


use App\Entity\Mot;
use App\Entity\Motgroupe;
use Declic3000\Pelican\Action\Action;


class MotgroupeAction extends Action
{

    function form_save_after_flush(Motgroupe $objet_data, $modification, $form){

        $this->sac->clear();
        $this->sac->initSac(true);
    }

    function deleteCplt($objet_data){
        $this->sac->clear();
        $this->sac->initSac(true);
    }



}