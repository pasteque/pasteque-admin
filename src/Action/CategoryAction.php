<?php

namespace App\Action;


use App\Entity\Bloc;
use App\Entity\Category;
use App\Entity\Product;


class CategoryAction extends Action
{


    function chargementDataCplt($modification, Category $objet_data, $builder)
    {

        if ($modification) {
            if ($objet_data->getParent()){
                $builder->get('parent')->setData($objet_data->getParent()->getId());
            }


        }
    }


    function formSave($form, Category $objet_data, $modification)
    {

        $id_parent = $form->get("parent")->getData();
        if ($id_parent) {
            $parent = $this->chargeur->charger_objet('category', $id_parent);
            $objet_data->setParent($parent);
        }elseif($id_parent==0){
            $objet_data->setParent(null);
        }

        if (!$modification) {
            [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/category', $objet_data->toArray(), [], 'POST');
        } else {
            // TODO : Passer en méthode PATCH
            //unset($pr['id']);
            $cat = $objet_data->toArray();
            $cat['id'] = $objet_data->getId();
            [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/category', $cat, [], 'POST');

        }
        if ($statut == 200) {
            $this->em->persist($objet_data);
            $this->em->flush();
            $this->sac->clear();
            $this->sac->initSac(true);
        }
        return ['id' => $objet_data->getPrimaryKey()];
    }


    function deleteBefore(Category $category){

        [$reponse, $statut, $err] = $this->clientAPI->appelAPI('/api/category/'.$category->getId(), [], [], 'DELETE');
        if ($statut != 200) {
            trigger_error("Suppresion impossible", E_USER_ERROR);
        }


    }


    function deleteCplt(Category $category){
        $this->sac->clear();
        $this->sac->initSac(true);
    }


}