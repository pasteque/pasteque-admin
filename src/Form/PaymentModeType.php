<?php

namespace App\Form;

use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PaymentModeType extends AbstractType
{



    protected $tab_paymentmode;

    function __construct(Sac $sac){
        $this->tab_paymentmode = $sac->tab('floor');
    }



    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_paymentmode = $this->tab_paymentmode;
        $tab_order= ['In first'=>1] ;
        foreach($tab_paymentmode as $paymentmode){
            $tab_order[$paymentmode['label']]=$paymentmode['disp_order'];
        }
        $builder
            ->add('label', TextType::class, [])
            ->add('backlabel', TextType::class, [])
            ->add('disp_order', ChoiceType::class, ['choices'=>$tab_order])
            ->add('visible', CheckboxType::class, []);
               }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'paymentmode',
        ]);
    }
}