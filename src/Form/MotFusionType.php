<?php


namespace App\Form;




use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;


class MotFusionType extends AbstractType
{



    protected $tab_mot;

    public function __construct(Sac $sac)
    {
        $this->tab_mot = array_flip(table_simplifier($sac->tab('mot')));
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id_mot_fusion',ChoiceType::class, [
        'constraints' => new Assert\NotBlank(),
        'choices' =>$this->tab_mot,
        'multiple'=>true,
        'attr' => ['class' => 'select2']])
        ->add('nouveau_nom',TextType::class);

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'mot',
            'name'       => 'mot',
        ]);
    }
}