<?php


namespace App\Form;

use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FloorType extends AbstractType
{

    protected $tab_floor;

    function __construct(Sac $sac){
        $this->tab_floor = $sac->tab('floor');
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_floor = $this->tab_floor;
        $tab_order= ['In first'=>1] ;
        foreach($tab_floor as $floor){
            $tab_order[$floor['label']]=$floor['disp_order'];
        }
        $builder
            ->add('label', TextType::CLASS, ['attr' => ['class' => '']])
            ->add('image', FileType::class, ['attr' => ['class' => 'jq-ufs secondaire']])
            ->add('disp_order', ChoiceType::class, ['choices'=>$tab_order]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'place',
        ]);
    }
}