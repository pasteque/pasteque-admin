<?php


namespace App\Form;



use App\Entity\Motgroupe;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class MotType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', TextType::CLASS,['label' => 'Nom', 'attr' => ['class'=>'']]);
        $builder->add('nomcourt', TextType::CLASS,['label' => 'Nomcourt', 'attr' => ['class'=>'']]);
        $builder->add('motgroupe', EntityType::CLASS,['class'=>Motgroupe::class, 'attr' => ['class'=>'']]);
        $builder->add('descriptif', TextType::CLASS,['label' => 'Descriptif', 'attr' => ['class'=>'']]);
        $builder->add('texte', TextType::CLASS,['label' => 'Texte', 'attr' => ['class'=>'']]);
        $builder->add('importance', TextType::CLASS,['label' => 'Importance', 'attr' => ['class'=>'']]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'mot',
            'name'       => 'mot',
        ]);
    }
}