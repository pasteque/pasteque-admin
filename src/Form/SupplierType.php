<?php

namespace App\Form;

use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SupplierType extends AbstractType
{


    protected $tab_pays;


    function __construct(Sac $sac){
        $this->tab_pays = table_simplifier($sac->tab('pays'));

    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
           ->add('name', TextType::class, ['attr' => ['class' => '']])
            ->add('isDirect', CheckboxType::class, ['required' => false, 'attr' => ['align_with_widget' => true, 'class' => 'bs_switch']])
            ->add('address1', TextType::class, ['label' => 'address1'])
            ->add('address2', TextType::class, ['label' => 'address2'])
            ->add('zipcode', TextType::class, ['label' => 'zip code'])
            ->add('town', TextType::class, [])
            ->add('state', TextType::class, [])
            ->add('website', TextType::class, [])
            ->add('country', ChoiceType::class, ['choices' => array_flip($this->tab_pays)])
            ->add('phone', TextType::class, ['attr' => ['class' => 'phone']])
            ->add('cellphone', TextType::class, ['attr' => ['class' => 'phone']])
            ->add('fax', TextType::class, ['attr' => ['class' => 'phone']])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'adresse@email.fr']])
            ->add('contact', TextareaType::class, ['attr'=>['class'=>'secondaire']])
            ->add('comment', TextareaType::class, ['attr'=>['class'=>'secondaire']])
            ->add('image', FileType::class, ['label' => 'Photo', 'attr' => ['class' => 'jq-ufs secondaire']])

        ->add('active', CheckboxType::class, ['required' => false, 'attr' => ['align_with_widget' => true, 'class' => 'bs_switch']]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'supplier',
        ]);
    }
}