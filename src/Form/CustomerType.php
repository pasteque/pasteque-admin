<?php

namespace App\Form;

use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CustomerType extends AbstractType
{

    protected $tab_categories;
    protected $tab_hierarchie;
    protected $tab_pays;


    function __construct(Sac $sac){
        $this->tab_hierarchie = $sac->tab('category_tree');
        $this->tab_categories = $sac->tab('category');
        $this->tab_pays = table_simplifier($sac->tab('pays'));

    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('firstname', TextType::class, ['attr' => ['class' => '']])
            ->add('firstname', TextType::class, ['label' => 'nom_famille', 'attr' => ['class' => '']])
            ->add('lastname', TextType::class, ['attr' => ['class' => '']])
            ->add('addr1', TextType::class, ['label' => 'address'])
            ->add('addr2', TextType::class, ['label' => 'address'])
            ->add('zipcode', TextType::class, ['label' => 'zip code'])
            ->add('city', TextType::class, [])
            ->add('region', TextType::class, [])
            ->add('country', ChoiceType::class, ['choices' => array_flip($this->tab_pays)])
            ->add('phone1', TextType::class, ['attr' => ['class' => 'phone']])
            ->add('phone2', TextType::class, ['attr' => ['class' => 'phone']])
            ->add('fax', TextType::class, ['attr' => ['class' => 'phone']])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'adresse@email.fr']])
            ->add('note', TextareaType::class, ['attr'=>['class'=>'secondaire']])
            ->add('image', FileType::class, ['label' => 'Photo', 'attr' => ['class' => 'jq-ufs secondaire']]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'customer',
        ]);
    }
}