<?php


namespace App\Form;

use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CategoryType extends AbstractType
{


    protected $tab_categories;
    protected $tab_hierarchie;
    protected $tab_tax;


    function __construct(Sac $sac){
        $this->tab_hierarchie = $sac->tab('category_tree');
        $this->tab_categories = $sac->tab('category');
        $this->tab_tax = table_simplifier($sac->tab('tax'));

    }




    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_categories = arbre_linearise_niveau($this->tab_hierarchie,$this->tab_categories);

        $tab_categories_class = table_colonne_cle_valeur($tab_categories,'label','niveau');
        foreach($tab_categories_class as &$val){
            $val=['class'=>'niveau'.$val];
        }
        $tab_categories = array_flip(table_simplifier($this->tab_categories,'label'));
        $tab_categories=['Racine'=>'0']+$tab_categories;
        $builder
            ->add('parent', ChoiceType::CLASS, ['choice_attr' => $tab_categories_class, 'mapped'=>false, 'choices' => $tab_categories, 'expanded' => false, 'multiple' => false, 'required'=>false, 'attr' => ['placeholder'=>'Pas de catégorie parente' ,'class' => 'select2_hierarchie']])
            ->add('reference', TextType::CLASS, ['attr' => ['class' => '']])
            ->add('label', TextType::CLASS, ['attr' => ['class' => '']])
            ->add('dispOrder', NumberType::CLASS, ['attr' => ['class' => '']]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

            'name' => 'category',
        ]);
    }
}