<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class ProductfromType extends AbstractType
{


    public function __construct(UrlGeneratorInterface $router)
    {
        $this->url_autocomplete = $router->generate('product_index',['action'=>'autocomplete']);

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('product_id', HiddenType::class)
            ->add('from_product_id', HiddenType::class,['required' => true])
            ->add('from_product', TextType::class, [
                'label' => 'Product',
                'required' => true,
                'attr' => [
                    'class' => 'autocomplete_auto',
                    'data-affichage' => 'product',
                    'data-composite' => 'text',
                    'data-cible' => 'Productfrom_from_product_id',
                    'data-url' => $this->url_autocomplete,
                    'data-message' => 'Aucun produit trouvé',
                    'placeholder' => 'Rechercher et choisir un produits'
                ]]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            //   'data_class' => 'zone',
            'name' => 'product_scale',
        ]);
    }
}