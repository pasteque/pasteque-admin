<?php

namespace App\Form;

use App\Form\Type\MotsType;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class MotProductType extends AbstractType
{

    
    protected $tab_mots;
    
    public function __construct(Sac $sac, Suc $suc)
    {
    
        $tab_mots = $sac->tab('mot_arbre.product');
        $tab_mot_groupe = $sac->tab('motgroupe');
        $tab_mot_groupe_exclu = table_simplifier(table_filtrer_valeur($tab_mot_groupe,'systeme',true),'nom');
        $tab_mot_groupe_exclu += table_simplifier(table_filtrer_valeur($tab_mot_groupe,'actif',false),'nom');
       // $tab_mot_groupe_exclu += table_filtrer_valeur($tab_mot_groupe,'nomcourt','tag');
        $this->tab_mots =array_diff_key($tab_mots,array_flip($tab_mot_groupe_exclu));
        $tab_mot_suc = $suc->choice('mot');
        if ($tab_mot_suc){
            foreach($this->tab_mots as $gp=>&$tab_mot){
                    $tab_mot = array_intersect($tab_mot,$tab_mot_suc);
            }
        }


    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('mots',ChoiceType::class, ['mapped'=>false,'choices' => $this->tab_mots, 'multiple' => true]);
        $builder->add('mots_tag', MotType::class,['mapped'=>false]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'mot_rouleau',
        ]);
    }
}