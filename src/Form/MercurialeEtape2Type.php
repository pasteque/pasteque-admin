<?php

namespace App\Form;

use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MercurialeEtape2Type extends AbstractType
{

    protected $tab_supplier;

    function __construct(Sac $sac){
        $this->tab_supplier = array_flip(table_simplifier($sac->tab('supplier'),'name'));
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        foreach ($options['changements_class'] as &$truc){
            $truc=['class'=>$truc];
        }
        $builder->add('choix', ChoiceType::CLASS, [
            'label' => 'Cochez les modifications à faire',

            'choices' => $options['changements'],
            'choice_attr' => $options['changements_class'],
            'expanded' => true, 'multiple' => true,
            'attr' => [],
            'row_attr' => ['class' => 'tout_cocher']
        ]);
        $builder->add('json', HiddenType::class);
        $builder->add('supplier_id', HiddenType::class);
        $builder->add('category_id', HiddenType::class);
        $builder->add('options', HiddenType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'mercuriale2',
            'changements'=>[],
            'changements_class'=>[]
        ]);
    }

}