<?php


namespace App\Form;




use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class MotgroupeType extends AbstractType
{




    protected $tab_mot_groupe;

    public function __construct(Sac $sac)
    {

        $this->tab_mot_groupe = table_simplifier($sac->tab('motgroupe'));


    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {


        $builder->add('parent_id', ChoiceType::class, [
            'label' => 'groupeparent',
            'mapped'=>false,
            'choices' => array_flip(array_merge([0 => 'aucun'], $this->tab_mot_groupe)), 'attr' => []]
        )
            ->add('nom', TextType::class, ['attr' => ['class' => 'span2', 'placeholder' => 'Nom du goupe de mot'], 'extra_fields_message' => 'mot_sans_espace'])
            ->add('nomcourt', TextType::class, ['constraints' => new Assert\NotBlank(), 'attr' => ['class' => 'span2', 'placeholder' => 'Nom court du goupe de mot'], 'extra_fields_message' => 'Ncourt'])
            ->add('descriptif', TextType::class, ['label' => 'descriptif', 'attr' => ['required' => false, 'class' => 'span2', 'placeholder' => 'Description du groupe de mots'], 'extra_fields_message' => 'nom'])
            ->add('texte', TextareaType::class,
                ['label' => 'explication', 'attr' => ['required' => false, 'placeholder' => '']])
            ->add('objets_en_lien', TextType::class, ['label' => 'objets_en_lien', 'attr' => ['placeholder' => 'si plusieurs séparé par ;']])
            ->add('systeme', CheckboxType::class, ['required' => false, 'attr' => ['align_with_widget' => true, 'class' => 'bs_switch']])
            ->add('nom_option1', TextType::class, [
                //   'constraints' => new Assert\NotBlank(),
                'mapped'=>false,
                'attr' => ['class' => 'span2', 'placeholder' => 'titre option 1 '],
                'extra_fields_message' => 'Titre',
            ])
            ->add('optgroup_option1', ChoiceType::class, ['label' => 'Groupe indépendant', 'mapped'=>false, 'expanded' => true, 'label_attr' => ['class' => 'radio-inline'], 'choices' => ['oui' => 1, 'non' => 0], 'attr' => ['inline' => true]])
            ->add('classement_option1', ChoiceType::class, ['label' => 'Classement', 'mapped'=>false, 'expanded' => true, 'label_attr' => ['class' => 'radio-inline'], 'choices' => ['Nom' => "nom", 'Non court' => 'nomcourt', 'Identifiant' => 'id_mot'], 'attr' => ['inline' => true]])
            ->add('indice_option1', IntegerType::class, ['label' => 'indice_option1', 'mapped'=>false])
            ->add('operateur', ChoiceType::class, ['label' => 'Operateur', 'mapped'=>false, 'expanded' => true, 'label_attr' => ['class' => 'radio-inline'], 'choices' => ['Et' => 'AND', 'Ou' => 'OR'], 'attr' => ['inline' => true]])
            ->add('actif', ChoiceType::class, ['label' => 'Actif', 'expanded' => true, 'label_attr' => ['class' => 'radio-inline'], 'choices' => ['oui' => 1, 'non' => 0], 'attr' => ['inline' => true]]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            //   'data_class' => 'motgroupe',
            'name' => 'motgroupe',
        ]);
    }
}