<?php

namespace App\Form;

use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MercurialeEtape1Type extends AbstractType
{


    protected $tab_categories;
    protected $tab_hierarchie;
    protected $tab_supplier;


    function __construct(Sac $sac){
        $this->tab_supplier = array_flip(table_simplifier($sac->tab('supplier'),'name'));
        $this->tab_hierarchie = $sac->tab('category_tree');
        $this->tab_categories = $sac->tab('category');
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $options_form=[
            'Ajouter les nouveaux produits  '=> 'options_ajout',
            'Modifier du nom des produits   '=> 'options_modification_nom',
            'Modifier du prix dres produits   '=> 'options_modification_prix',
            'Modifier les caractéristique des produits '=> 'options_modification_attr',
            'Création de la référence fournisseur si inexistante '=> 'options_ajout_reference',
            'Modification de la référence fournisseur existantes'=> 'options_modification_reference',
        ];

        $tab_categories = arbre_linearise_niveau($this->tab_hierarchie, $this->tab_categories);
        $tab_categories_class = table_colonne_cle_valeur($tab_categories, 'label', 'niveau');
        foreach ($tab_categories_class as &$val) {
            $val = ['class' => 'niveau' . $val];
        }

        $tab_categories = array_flip(table_simplifier($tab_categories, 'label'));

        $builder
            ->add('category_id', ChoiceType::CLASS, ['label' => 'Categorie par défaut', 'choice_attr' => $tab_categories_class, 'choices' => $tab_categories, 'expanded' => false, 'multiple' => false, 'attr' => ['class' => 'select2_hierarchie']])
            ->add('supplier_id', ChoiceType::CLASS, ['label' => 'Supplier','choices' => $this->tab_supplier, 'expanded' => false, 'multiple' => false, 'attr' => []])
            ->add('options', ChoiceType::CLASS, ['label' => 'Options','choices' => $options_form, 'expanded' => true, 'multiple' => true, 'attr' => []])
            ->add('json', HiddenType::class,[]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'mercuriale1'
        ]);
    }



}