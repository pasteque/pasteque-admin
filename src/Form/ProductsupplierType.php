<?php


namespace App\Form;

use App\Entity\ProductExtra;
use App\Entity\Tax;
use Declic3000\Pelican\Service\Sac;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ProductsupplierType extends AbstractType
{


    protected $tab_supplier;


    function __construct(Sac $sac){
        $this->tab_supplier = array_flip(table_simplifier($sac->tab('supplier'),'name'));
    }



    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('supplier_id', ChoiceType::CLASS, ['label' => 'Supplier','mapped'=>false,'choices' => $this->tab_supplier, 'expanded' => false, 'multiple' => false, 'attr' => []])
            ->add('reference',TextType::class,['label' => 'Référence du fournisseur'])
            ->add('colisage',NumberType::class,['label' => 'Colisage'])
            ->add('priceBuy',MoneyType::class,['label' => 'Prix d\'achat du colis'])
            ->add('favori',CheckboxType::class,['label' => 'Fournisseur favori pour ce produit']);
    }





    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            //   'data_class' => 'zone',
            'name' => 'zone',
        ]);
    }
}