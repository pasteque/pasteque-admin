<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class LabelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $tab_bloc = ['simple label'=>'simple', 'Prix au kilo'=>'au_kilo'];
			$builder->add('depart', TextType::CLASS,['label' => 'position_depart', 'attr' => ['data-rows'=>$options['rows'],'data-cols'=>$options['cols'],'class'=>'position_table']]);
			$builder->add('modele', ChoiceType::CLASS,['label' => 'modele','choices'=>$tab_bloc, 'attr' => ['class'=>'']]);
	}
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'label',
            'cols'       => 3,
            'rows'       => 8
        ]);
    }
}