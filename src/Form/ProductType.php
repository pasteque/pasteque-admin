<?php


namespace App\Form;

use App\Entity\ProductExtra;
use App\Entity\Tax;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ProductType extends AbstractType
{

    protected $tab_categories;
    protected $tab_hierarchie;
    protected $tab_tax;
    protected $tab_mots;


    function __construct(Sac $sac,Suc $suc){
        $this->tab_hierarchie = $sac->tab('category_tree');
        $this->tab_categories = $sac->tab('category');
        $this->tab_tax = table_simplifier($sac->tab('tax'));
        $tab_mots = $sac->tab('mot_arbre.product');
        $tab_mot_groupe = $sac->tab('motgroupe');
        $tab_mot_groupe_exclu = table_simplifier(table_filtrer_valeur($tab_mot_groupe,'systeme',true),'nom');
        $tab_mot_groupe_exclu += table_simplifier(table_filtrer_valeur($tab_mot_groupe,'actif',false),'nom');
        // $tab_mot_groupe_exclu += table_filtrer_valeur($tab_mot_groupe,'nomcourt','tag');
        $this->tab_mots =array_diff_key($tab_mots,array_flip($tab_mot_groupe_exclu));
        $tab_mot_suc = $suc->choice('mot');
        if ($tab_mot_suc){
            foreach($this->tab_mots as $gp=>&$tab_mot){
                $tab_mot = array_intersect($tab_mot,$tab_mot_suc);
            }
        }
    }



    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tab_measureUnit = array_flip(measureUnit());
        $tab_categories = arbre_linearise_niveau($this->tab_hierarchie, $this->tab_categories);
        $tab_categories_class = table_colonne_cle_valeur($tab_categories, 'label', 'niveau');
        foreach ($tab_categories_class as &$val) {
            $val = ['class' => 'niveau' . $val];
        }

        $tab_categories = array_flip(table_simplifier($tab_categories, 'label'));
        $tab_tax = array_flip($this->tab_tax);
        $builder
            ->add('category_id', ChoiceType::CLASS, ['label' => 'Category', 'mapped'=>false, 'choice_attr' => $tab_categories_class, 'choices' => $tab_categories, 'expanded' => false, 'multiple' => false, 'attr' => ['class' => 'select2_hierarchie']])
            ->add('barcode', TextType::CLASS, ['attr' => ['class' => '']])
            ->add('reference', TextType::CLASS, ['disabled'=>true, 'attr' => ['class' => '']])
            ->add('label', TextType::CLASS, ['attr' => ['class' => '']])
            ->add('priceBuy', MoneyType::CLASS, ['label' => 'Buy price', 'attr' => ['class' => '']])
            ->add('priceSell', MoneyType::CLASS, ['label' => 'Sell price', 'attr' => ['class' => '']])
            ->add('priceSellvat', MoneyType::CLASS, ['label' => 'Sell price taxes', 'attr' => ['class' => ''], 'mapped'=>false])
            ->add('realsell', HiddenType::CLASS, ['attr' => ['class' => ''], 'mapped'=>false])
            ->add('tax', EntityType::CLASS, ['class' => Tax::class, 'expanded' => false, 'multiple' => false])
            ->add('discountEnabled', CheckboxType::class, ['required' => false, 'attr' => ['align_with_widget' => true, 'class' => 'bs_switch']])
            ->add('discountRate', NumberType::CLASS, ['attr' => ['class' => '']])
            ->add('scaled', CheckboxType::class, ['required' => false, 'attr' => ['align_with_widget' => true, 'class' => 'bs_switch']])
            ->add('scaleType', ChoiceType::CLASS, ['label' => 'Measure Unit', 'choices'=>$tab_measureUnit, 'attr' => ['class' => '']])
            ->add('scaleValue', NumberType::CLASS, ['label' => 'Contenance', 'attr' => ['class' => '']])

            ->add('visible', CheckboxType::class, ['required' => false, 'attr' => ['align_with_widget' => true, 'class' => 'bs_switch']])
            ->add('mots',ChoiceType::class, ['mapped'=>false,'choices' => $this->tab_mots, 'multiple' => true])
           ->add('product_cplt', ProductCpltType::class, ['mapped' => false]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            //   'data_class' => 'zone',
            'name' => 'zone',
        ]);
    }
}