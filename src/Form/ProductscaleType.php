<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ProductscaleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tab_scale_type = array_flip(measureUnit());

        $builder->add('product_id', HiddenType::CLASS)
                ->add('scaleTypeBool', CheckboxType::CLASS, [ 'label' => 'Le poids au kilo ou litre  s\'applique  sur ce produits','attr'=>['checked'=>'checked']])
                ->add('scaleType', ChoiceType::CLASS, [ 'choices' => $tab_scale_type, 'expanded' => false, 'multiple' => false])
                ->add('scaleValue', NumberType::CLASS,['label'=>'Contenu (poids net égouté)','required'=>false]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            //   'data_class' => 'zone',
            'name' => 'product_scale',
        ]);
    }
}