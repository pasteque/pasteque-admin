<?php


function getLienProfilGroupeObjet($id = null){
    $tab = [
        'ROLE_ADMIN' => ['admin', 'geo', 'doc', 'com'],
        'ROLE_USER' => ['generique']
    ];
    return getValeur($tab, $id);
}

function measureUnit($code = null){

    $tab = [
        '0' => '',
        '1' => 'gramme',
        '2' => 'litre',
    ];

    return getValeur($tab, $code);

}


function getObjetLog($id = null)
{

    $tab = [];
    $tab = $tab + ['ACT' => 'activite',
            'PRO' => 'product',
            'CAT' => 'category',
            'CAS' => 'cashregister',
            'CUS' => 'customer',
            'IMA' => 'image'
        ];
    return getValeur($tab, $id);
}

function getLocation($code = null)
{
    $tab = [
        '1' => 'derriere la caisse',
        '2' => 'petite étagère dans l\'entrée',
        '3' => 'étagère à légume',
        '4' => 'frigo',
        '5' => 'Ilot vrac 1',
        '6' => 'Ilot vrac 2 : le marché du vrac',
        '7' => 'Rayonnage A',
        '8' => 'Rayonnage B',
        '9' => 'Rayonnage C',
        '10' => 'Rayonnage D',
        '11' => 'Rayonnage E',
        '12' => 'Rayonnage F',
        '13' => 'Rayonnage G',
        '14' => 'Rayonnage H',
        '15' => 'Rayonnage I',
        '16' => 'Rayonnage J',
        '17' => 'Etagère épice',
        '18' => 'Accrocher aux grilles',
    ];

    return getValeur($tab, $code);
}


function class_css_action($code, $substition = 'color-info')
{
    $tab = [
        'CRE' => 'color-success',
        'NEW' => 'color-success',
        'MOD' => 'color-warning',
        'DEL' => 'color-danger',
        'SUP' => 'color-danger'
    ];
    return getValeur($tab, $code, null, $substition);
}


function class_css_objet($code, $substition)
{
    $tab = [
        'IND' => 'fa-user',
        'MEM' => 'fa-users',
        'SR1' => 'fa-euro-sign',
        'PAI' => 'fa-euro-sign',
        'SR2' => 'fa-euro-sign',
        'SR3' => 'fa-euro-sign',
        'SR4' => 'fa-euro-sign',
        'SR5' => 'fa-euro-sign',
        'DOC' => 'fa-file',
        'VOT' => 'ico ico-vote'
    ];
    return getValeur($tab, $code, null, $substition);
}


function getStatutTache($id = null)
{
    $tab = ['0' => 'A faire', '1' => 'En cours', '2' => 'Suspendu', '3' => 'En erreur', '4' => 'Terminée'];
    return getValeur($tab, $id);
}


function getProfil($id = null)
{
    $tab = getProfilSansNiveau() + getProfilNiveau();
    return getValeur($tab, $id);
}


function getProfilSansNiveau($id = null)
{
    $tab = ['X' => 'Configuration', 'A' => 'Administrateur'];
    return getValeur($tab, $id);
}

function getProfilNiveau($id = null)
{
    $tab = ['G' => 'Gestion associative', 'L' => 'Communication', 'C' => 'Comptabilité'];
    return getValeur($tab, $id);
}

function getNiveau($id = null)
{
    $tab = ['1' => 'Voir', '2' => 'Voir, créer et modifier', '3' => 'Voir, créer, modifier et supprimer'];
    return getValeur($tab, $id);
}


function getPreselection($id = null)
{
    $tab = ['membre_echu' => 'membre_echu', 'membre_ardent_echu' => 'membre_ardent_echu'];
    return getValeur($tab, $id);
}

