(function() {
window["templates"] = window["templates"] || {};

window["templates"]["bouton.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<a class="btn btn-{{couleur}} btn-sm force-tooltip {{class}}" {{protection}}href="{{url}}" {{#title}}title="{{title}}"{{/title}} data-placement="bottom" id="lien_{{id}}" {{{attr}}}>\n    <i class="{{icon}}"></i>\n</a>\n\n';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["bouton_action.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="btn-group" role="group">\n    {{#bt_action}}\n        {{{bt_action}}}\n    {{/bt_action}}\n</div>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["modal_bs4.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal" tabindex="-1" role="dialog" id="{{id}}">\n    <div class="modal-dialog {{class_cplt}}">\n        <div class="modal-content">\n            <div class="modal-header">\n                {{#titre}}\n                <h5 class="modal-title">{{titre}}</h5>\n                {{/titre}}\n                {{#close}}\n                <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n                    <span aria-hidden="true">&times;</span>\n                </button>\n                {{/close}}\n            </div>\n            <div class="modal-body">\n                {{{body}}}\n            </div>\n            {{#footer}}\n            <div class="modal-footer">\n                {{{footer}}}\n            </div>\n            {{/footer}}\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["modal_bs5.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal fade" tabindex="-1" role="dialog" id="{{id}}">\n    <div class="modal-dialog {{class_cplt}}">\n        <div class="modal-content">\n            <div class="modal-header">\n                {{#titre}}\n                <h5 class="modal-title">{{titre}}</h5>\n                {{/titre}}\n                {{#close}}\n                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>\n                {{/close}}\n            </div>\n            <div class="modal-body">\n                {{{body}}}\n            </div>\n            {{#footer}}\n            <div class="modal-footer">\n                {{{footer}}}\n            </div>\n            {{/footer}}\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["notification_progress.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="notification_progress" class="{{ class }}">\n    <div class="message">\n    {{#icon}} <i class="fa fa-icon"></i>   {{/icon}} {{ message }}\n        <div class="progress progress-bar-striped active">\n            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">\n                60%\n            </div>\n        </div>\n    </div>\n\n    <div class="action">\n        <a class="btn btn-danger btn-sm force-tooltip "  title="supprimer" data-placement="bottom" id="lien_supprimer">\n        <i class="fa fa-trash-alt"></i>\n    </a>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["notification_simple.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="notification_simple" id="notification{{id_notification}}" class="{{ class }}">\n    <div class="message">\n    {{#icon}} <i class="fa fa-icon"></i>   {{/icon}} <h4>{{ nom }}</h4><p>{{message}}</p>\n    </div>\n    <div class="action">\n        <a class="btn btn-danger btn-sm force-tooltip "  title="supprimer" data-placement="bottom" id="lien_supprimer">\n        <i class="fa fa-trash-alt"></i>\n    </a>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["telephones.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '  {{#mobile}}\n   <i class="fa fa-mobile-alt" aria-hidden="true"></i> {{mobile}}\n {{/mobile}}\n {{#telephone}}\n <i class="fa fa-phone" aria-hidden="true"></i> {{telephone}}\n {{/telephone}}\n {{#telephone_pro}}\n  <i class="fa fa-phone" aria-hidden="true"></i> {{telephone_pro}}\n {{/telephone_pro}}\n\n \n ';

}
return __p
}})();
(function() {
window["templates"] = window["templates"] || {};

window["templates"]["trier_par_dropdown.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<span class="etat"><span class="valeur"></span></span>\n<div class="btn-group">\n    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"\n            aria-expanded="false" data-name="{{name}}" data-selecteur="{{selecteur}}">\n        Ajouter un critère <i class="fa fa-sort"></i>\n    </button>\n    <div class="dropdown-menu dropdown-menu-right trier_par_selection"></div>\n</div>';

}
return __p
}})();