let data_notifications =[];

$.fn.extend({
    'initialisation_js': initialisation_js
});

$(document).ready(function(){
    initialisation_champs_autocomplete_recherche($('#barre_navigation'));
    initialisation_notification(data_notifications);

    $('body').initialisation_js();
});


function initialisation_js(){
    $(this).initialisation_depliable();
    $(this).initialisation_composant_forms();
    $(this).initialisation_modal();
    $(this).initialisation_ajax();
    $(this).initialisation_champs_autocomplete_automatique();
    $(this).initialisation_trier_par('div.trier_par_selection');
}


