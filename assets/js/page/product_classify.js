$.fn.extend({
    'product_classify': product_classify,
});


function product_classify(url_action, url_action_modifiy) {

    var id_category_en_cours = 0;

    $("#categories_list li div",this).click(function () {
        id_category_en_cours = $(this).parent().attr("id").substring(10);
        $.ajax({
            type: 'GET', // Le type de ma requete
            url: url_action, // L'url vers laquelle la requete sera envoyee
            data: {
                category: id_category_en_cours
            },
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#products_list div.list').remove();
                $('#categories_list li div').removeClass('on');
                $('#categories_list li#licategory'+id_category_en_cours+'> div').addClass('on');
                $('#products_list').append("<div class=\"list\"><ul></ul></div>");
                for (var i = 0; i < data.length; i++) {
                    $('#products_list div.list ul').append('<li id="product' + data[i].id + '">' + data[i].code + ' - ' + data[i].label + '</li>');
                }

              //  $( "#categories_list li div.on" ).droppable( "destroy" );
                $("#categories_list li div").droppable({
                        'accept': "#products_list li",
                        'classes': {
                            "ui-droppable-active": "ui-state-active",
                            "ui-droppable-hover": "ui-state-hover"
                        },

                        'drop': function (event, ui) {

                            tab_products = new Array();
                            data = $('#draggingContainer ul li');
                            for (var i = 0; i < data.length; i++)
                                tab_products.push($(data[i]).attr("id").substring(7));
                            if (data.length > 0)
                                modify_category(url_action_modifiy, tab_products, $(this).parent().attr("id").substring(10));
                        }
                    }
                );


                $('#products_list div.list ul li').click(function (e) {

                    if (!e.ctrlKey) {
                        // Ctrl not pressed, clear previous selections
                        $('#products_list div.list ul li').removeClass("selected");
                    }
                    $(this).addClass("selected");


                    $('#products_list li.selected').draggable({
                        'cursor': "move",
                        'tolerance' :"pointer",
                        'cursorAt': { left: -5,top:$(this).height()/2 },
                        'helper': function () {
                            var selected = $('#products_list li.selected');
                            if (selected.length === 0) {
                                selected = $(this);
                            }
                            var container = $('<div/>').attr('id', 'draggingContainer');
                            container.append('<ul></ul>');
                            $('ul',container).append(selected.clone());
                            return container;
                        },
                        start: function(event, ui){
                            $(this).draggable('instance').offset.click = {
                                left: -5,
                                top: Math.floor(ui.helper.height() / 2)
                            };
                        }
                    });


                });

            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    });

}


function modify_category(url_action_modifiy, tab_products, category) {

    $.ajax({
        type: 'GET', // Le type de ma requete
        url: url_action_modifiy, // L'url vers laquelle la requete sera envoyee
        data: {
            tab_products: JSON.stringify(tab_products),
            category: category
        },
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            for (var i = 0; i < tab_products.length; i++) {
                $('#products_list li#product' + tab_products[i]).remove();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}