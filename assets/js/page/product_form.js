$.fn.extend({
    'initialisation_product_form': initialisation_product_form,
});


function v2p(numberp){
    if (numberp != undefined) {
        return financial(numberp.replace(',', '.'));
    }
}


function initialisation_product_form(){

    var round_price=0.05;
    var tax_rates = getTax();

    $(this).initialisation_champs_autocomplete_automatique();
    $("#productform_priceBuy",this).val(v2p($("#productform_priceBuy",this).val()));
    $("#productform_realsell",this).val(v2p($("#productform_realsell",this).val()));
    $("#productform_priceSell",this).val(v2p($("#productform_priceSell",this).val()));

    $('input, select',this).keypress(function(event) { return event.keyCode != 13; });

    $('#productform_barcode').change(function() {
        $('#productform_reference').val($(this).val());
    });

    $("input#productform_priceSellvat").TouchSpin({

        min: -999999999999999999.99,
        max: 999999999999999999.99,
        step: round_price,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        decimal:2,
        forcestepdivisibility:'none',
        verticalbuttons: true,
        verticalupclass: 'glyphicon glyphicon-plus',
        verticaldownclass: 'glyphicon glyphicon-minus'
    });

    $("#productform_priceBuy").parent('div').parent('div').parent('div')
        .after(
            '<div id="li_formedit-margin" class="form-group row">' +
            '   <div class="col-sm-3">' +
            '       <label for="formedit-margin" class="control-label">Taux de marque</label>' +
            '   </div>' +
            '   <div class="col-sm-9">' +
            '       <div class="input-group">' +
            '           <input type="text" placeholder="" class="form-control " value="" name="margin" id="productform_margin">' +
            '           <input type="text" placeholder="" class="form-control " value="" name="margin" id="productform_marginRate">' +
            '           <div class="input-group-append"><span class="input-group-text">%</span></div>' +
            '       </div>' +
            '   </div>' +
            '</div>')





    $("#productform_margin").change(function() {
        var val = $(this).val().replace(",", ".");
        $(this).val(val);
        var rate = ((val / 100)).toFixed(4);
        $("#productform_marginRate").val(rate);
        updateSell(tax_rates,round_price);
        updateMargin();
    });


    $("#productform_marginRate").hide();
    $("#productform_marginRate").change(function() {
        var val = $(this).val().replace(",", ".");
        $(this).val(val);
        var margin = ((val) * 100).toFixed(2);
        $("#productform_margin").val(margin);
        updateSell(tax_rates,round_price);
        updateMargin();
    });

    $("#productform_priceSellvat").change(function() {
        var val = $(this).val().replace(",", ".");
        $(this).val(val);
        updateSellPrice(tax_rates);
    });
    $("#productform_tax").change(function() {
        var round_price='0.05';
        var way_calculation_price ='price';
        if (way_calculation_price =="margin")
            updateMargin();
        else{
            updateSell(tax_rates,round_price);
            updateMargin()
        }
    });
    $("#productform_priceSell").change(function() {
        var round_price='0.05';
        var val = $(this).val().replace(",", ".");
        $("#productform_realsell").val(val);
        $(this).val(val);
        updateSellVatPrice(tax_rates,round_price);
    });
    $("#productform_priceBuy").change(function() {
        var round_price='0.05';
        var way_calculation_price ='price';
        var val = $(this).val().replace(",", ".");
        $(this).val(val);
        if (way_calculation_price ==="margin"){
            updateMargin();
        }
        else{
            updateSell(tax_rates,round_price);
            updateMargin();
        }
    });

    updateMargin(tax_rates);
    updateSell(tax_rates,round_price);
    updateBarcode();

    $("#productform_barcode").change(updateBarcode);



}





roundPrice = function(number,round_price)
{
    if (round_price > 0.01)
        number=((number/(round_price*100)).toFixed(2)*(round_price*100)).toFixed(2)
    return number
};


bloque_touche_entree=function (event){
    if(event.keyCode == 13)
        event.stop();
};

updateSell = function(tax_rates,round_price) {
    var buy = $("#productform_priceBuy").val();
    var rate = $("#productform_marginRate").val();
    var sell = buy  / (1 - rate);
    if (tax_rates[$("#productform_tax").val()]!=undefined){
        var ratevat = parseFloat(tax_rates[$("#productform_tax").val()]['rate']);
    }
    var sellvat = sell * ( ratevat+1);
    sellvat = roundPrice(sellvat.toFixed(2),round_price);
    sell = sellvat /( ratevat+1);
    $("#productform_priceSellvat").val(sellvat);
    $("#productform_realsell").val(sell);
    $("#productform_priceSell").val(sell.toFixed(2));
};


updateSellPrice = function(tax_rates) {
    var sellvat = $("#productform_priceSellvat").val();
    var rate = parseFloat(tax_rates[$("#productform_tax").val()]['rate']);
    var sell = sellvat / (rate+1);
    $("#productform_realsell").val(sell);
    $("#productform_priceSell").val(sell.toFixed(2));
    updateMargin();
};

updateSellVatPrice = function(tax_rates,round_price) {
    // Update sellvat price
    var sell = $("#productform_realsell").val();
    var tax_id = $("#productform_tax").val()
    var rate = parseFloat(tax_rates[tax_id]['rate']);
    var sellvat = roundPrice((sell * (rate+1)),round_price);
    // Round to 2 decimals and refresh sell price to avoid unrounded payments
    //sellvat = sellvat.toFixed(2);
    $("#productform_priceSellvat").val(sellvat);
    updateMargin();
};
updateMargin = function() {

    var sell = $("#productform_priceSell").val();
    var buy = $("#productform_priceBuy").val();
    var ratio = (sell-buy) / sell;
    var margin = (ratio * 100).toFixed(2);
    var rate = (ratio).toFixed(4);
    $("#productform_margin").val(margin );
    $("#productform_marginRate").val(rate);
};


clearImage = function() {
    $("#img").hide();
    $("#clear").hide();
    $("#restore").show();
    $("#clearImage").val(1);
};
restoreImage = function() {
    $("#img").show();
    $("#clear").show();
    $("#restore").hide();
    $("#clearImage").val(0);
};

updateBarcode = function() {
    var barcode = $("#productform_barcode").val();
    if(barcode>999999999999)
        var src = "?{url_action_param}=img&w=barcode&code=" + barcode;
    $("#productform_barcodeImg").attr("src", src);
}


generateBarcode = function() {
    var first = Math.floor(Math.random() * 9) + 1;
    var code = new Array();
    code.push(first);
    for (var i = 0; i < 11; i++) {
        var num = Math.floor(Math.random() * 10);
        code.push(num);
    }
    var checksum = 0;
    for (var i = 0; i < code.length; i++) {
        var weight = 1;
        if (i % 2 == 1) {
            weight = 3;
        }
        checksum = checksum + weight * code[i];
    }
    checksum = checksum % 10;
    if (checksum != 0) {
        checksum = 10 - checksum;
    }
    code.push(checksum);
    var barcode = code.join("");
    $("#productform_barcode").val(barcode);
    updateBarcode();
};




function autocomplete_reponse_product(data){
    return '<p>'+data.id+' <strong>' + data.text + '</strong></p>';
}
