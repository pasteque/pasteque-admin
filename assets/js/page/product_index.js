function activerDesactiverBoutonLabel(valeur, data, options) {

    let url_label = options['url_label'];
    url_label = url_label.replace('/0/','/'+data[0]+'/');
    let classname='btn-secondary';
    if (valeur=='true'){
        classname='btn-primary';
    }
        return '<a href="'+url_label+'" class="btn btn-sm btn-ajax-switch '+classname+'" title="Ajouter/retirer à la liste des étiquettes à imprimer">' +
            '<i class="fa fa-tags"></i>' +
            '</a>';


}

function transformeLocation(id, data, options) {
    return getLocation(id);
}

function activerDesactiverVisible(valeur, data, options) {

    let url_visible = options['url_visible'];
    url_visible = url_visible.replace('/0/','/'+data[0]+'/');
    let classname='btn-secondary';
    if (valeur=='true'){
        classname='btn-primary';
    }
    return '<a href="'+url_visible+'" class="btn btn-sm btn-ajax-switch '+classname+'" title="Activer/désactiver un produit">' +
        '<i class="fa fa-thumbs-up"></i>' +
        '</a>';


}
