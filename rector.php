<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use \Rector\Php80\Rector\Class_\AnnotationToAttributeRector;
use \Rector\Php80\ValueObject\AnnotationToAttribute;
use \Rector\Set\ValueObject\SetList;



return RectorConfig::configure()
    ->withPaths([
        __DIR__ . '/lib',
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ])
    // uncomment to reach your current PHP version
    ->withPhpSets(php81: true)
    ->withSets([
        \Rector\Doctrine\Set\DoctrineSetList::ANNOTATIONS_TO_ATTRIBUTES,
        \Rector\Symfony\Set\SymfonySetList::ANNOTATIONS_TO_ATTRIBUTES,
        \Rector\Symfony\Set\SensiolabsSetList::ANNOTATIONS_TO_ATTRIBUTES
        ]
    )

    ->withConfiguredRule(
        AnnotationToAttributeRector::class,
        [new AnnotationToAttribute('Symfony\Routing\Annotation\Route')]
    )
    //->withSets([SetList::DEAD_CODE])
    ->withTypeCoverageLevel(0);
